<?php

class TeacherDataAction extends BaseAction
{
    protected $_teacher = null;
    
    function _initialize() {
        parent::_initialize();
        $this->_teacher = new TeacherModel();
    }

    public function teacher_get_teacher_course() {
        $ret = array();

        do
        {
            $account_data = new AccountDataAction();
            $username = $account_data->get_my_username();
            
            $course_model = new CourseModel();
            $course_data = $course_model->get_course_by_teacher_id($username);
            if(FALSE === $course_data)
            {
                break;
            }
            
            foreach ($course_data as $key => $value) 
            {
                $course_data[$key]['link_time'] = CourseTimeModel::db_to_string($value['link_time']);
            }

            DirectionDataAction::get_name_in_array($course_data, array('*', 'co_direction'), 'co_direction');

            $ret['data'] = $course_data;
        } while (0);

        return $ret;
    }

    public function teacher_get_teacher_info() {
        $ret = array();
        
        $account_data = new AccountDataAction();
        $username = $account_data->get_my_username();
        
        $ret['data'] = $this->_teacher->t_find($username);
        
        return $ret; 
    }

    public function teacher_get_course_data($co_id) {
        $ret = array();
        
        $course_data = new CourseDataAction();
        $ret['data'] = $course_data->get_course_data($co_id);
        
        return $ret;
    }
    
    public function teacher_get_course_student($co_id, $page, $page_size) {
        $ret = array();
        
        $course_data = new CourseDataAction();
        $ret['count'] = $course_data->get_course_student_count($co_id, $page, $page_size);
        $ret['page'] = $page ? $page : 1;

        $course_student_ids = $course_data->get_course_student_id($co_id, $page, $page_size);

        $student_data = new StudentDataAction();
        $ret['data'] = $student_data->get_students($course_student_ids);

        $course = $course_data->get_course_data($co_id);
        $ret['co_name'] = $course['co_name'];
        
        return $ret;
    }
    

    public function teacher_modify_course_data_a(){
        $ret_data = '';
        $ret_info = '修改失败';
        $ret_status = 0;
        
        do{
            if(FALSE === ($co_id = safe_post_b('co_id'))){
                $ret_info = '缺少参数';
                break;
            }
            if(FALSE === ($co_introduction = safe_post_b('co_introduction'))){
                $ret_info = '缺少参数';
                break;
            }

            $course_data = new CourseDataAction();
            $ret_data = $course_data->update_course_data($co_id, FALSE, FALSE, FALSE, FALSE, FALSE, $co_introduction, '');
            
            if(FALSE === $ret_data){
                $ret_info = '修改失败';
                break;
            }
            
            $ret_data = TRUE;
            $ret_info = '修改成功';
            $ret_status = 1;
        }while(0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function teacher_update_teacher_data_a(){
        $ret_data = '';
        $ret_info = ' 修改失败';
        $ret_status = 0;

        $account_data = new AccountDataAction();
        $username = $account_data->get_my_username();

        if(FALSE !== safe_post_b('t_phone')){
            $t_phone = safe_post_b('t_phone');
        }
        if(FALSE !== safe_post_b('t_email')){
            $t_email = safe_post_b('t_email');
        }
        if(FALSE !== safe_post_b('t_introduction')){
            $t_introduction = safe_post_b('t_introduction');
        }

        $ret_data = $this->update_teacher_info($username, null, null, $t_phone, $t_email, $t_introduction);
        
        if($ret_data === TRUE){
            $ret_info = ' 修改成功';
            $ret_status = 1;
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }

    public function search_teacher_info($key, $page, $page_size, $count){
        $ret = $this->_teacher->t_search($key, $page, $page_size, $count);
        return $ret;
    }
    
    public function find_teacher_info($t_id) {
        return $this->_teacher->t_find($t_id);
    }
    
    public function update_teacher_info($id, $name, $sex, $phone, $email, $introduction) {
        return $this->_teacher->t_update($id, $name, $sex, $phone, $email, $introduction);
    }
    
    public function delete_teacher_info($t_id) {
        
    }
    
    public function remove_teacher_info($t_ids) {
        $ret = FALSE;
        
        do
        {
            $result = $this->_teacher->t_remove($t_ids);
            if ($result === FALSE)
            {
                break;
            }
            
            $rbac = new RbacModel();
            $result = $rbac->remove_users($t_ids);
            if ($result === FALSE)
            {
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        return $ret;
    }
    
    public function create_teacher_info() {
        
    }
    
    public function upload_teacher_info($path) {
        $result = array(FALSE, '导入出错');
        
        do{
            vendor('Excel.reader');
            $excel_reader = new Spreadsheet_Excel_Reader;
            $excel_reader->setOutputEncoding('UTF-8');
            $excel_reader->read($path);

            //判断Excel文件是否符合标准
            if($excel_reader->sheets[0]['numCols'] < 6){
                $result[1] = '格式错误';
                break;
            }

            $line_unset = array();  //记录被删除掉的行

            //逐行检测Excel数据
            foreach ($excel_reader->sheets[0]['cells'] as $line_num => $line) {
                //去除表格头
                if($line_num == 1){
                    unset($excel_reader->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //表格前两项为必填信息
                if(count($line) < 2){
                    unset($excel_reader->sheets[0]['cells'][$line_num]);
                    $line_unset[] = $line_num;
                    continue;
                }
            }

            //存入数据库teacher表
            $result[0] = $this->_teacher->t_upload($excel_reader->sheets[0]['cells']);
            
            if($result[0] === FALSE){
                $result[1] = '导入数据库失败';
                break;
            }
            
            //存入Rbac表
            $rabc_model = new RbacModel();
            $result[0] = $rabc_model->add_user($excel_reader->sheets[0]['cells'], C('ROLE_TEACHER'));
            
            if($result[0] == FALSE){
                $result[1] = '导入数据库失败';
                break;
            }

            if(count($line_unset) > 0){
                $result[0] = FALSE;
                $result[1] = '以下行信息缺失：' . json_encode($line_unset);
                break;
            }
            
            $result[0] = TRUE;
            $result[1] = '导入成功';
        }while (0);
        
        return $result;
    }

    public static function get_name_in_array(&$arr, $id_key, $name_key, $unset = false)
    {
        if (count($id_key) == 1) {
            $key = reset($id_key);
            if (isset($arr[$key])) {
                $teacher = new TeacherModel();
                $arr[$name_key] = $teacher->t_get_name($arr[$key]);
                if ($unset) {
                    unset($arr[$key]);
                }
            }
            return;
        }

        if (reset($id_key) === '*') {
            array_shift($id_key);
            foreach ($arr as &$arr_next) {
                if (is_array($arr_next)) {
                        self::get_name_in_array($arr_next, $id_key, $name_key, $unset);
                }
            }
        } else {
            $key = reset($id_key);
            if (isset($arr[$key])) {
                array_shift($id_key);
                self::get_name_in_array($arr[$key], $id_key, $name_key, $unset);
            }
        }
    }

    
}