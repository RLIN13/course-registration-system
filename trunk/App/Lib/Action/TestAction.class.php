<?php

class TestAction extends Action{
    //put your code here
    function init_db()
    {
        if (!APP_DEBUG) {
            return;
        }
        
        $test = new TestModel();
        $test->disable_foreign_key();
        
        $test->init_class();
        $test->init_course();
        $test->init_course_time();
        $test->init_course_student();
        $test->init_direction();
        
        $test->init_rbac();
        $test->init_student();
        $test->init_teacher();
        
        $test->enable_foreign_key();
    }
    
    public function test() {
        $selecting_direction = new SelectingDirectionModel();
        $selecting_direction->filtrate_by_lab(1, 2011);
    }
}
