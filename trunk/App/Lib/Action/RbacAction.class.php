<?php

class RbacAction extends Action
{
    public function index()
    {
        trace_user( __CLASS__, __FUNCTION__, "in");
        if (!APP_DEBUG) {
            return;
        }

        $data = $this->fetch_module();

        $rbac = new RbacModel();
        $access_path = $rbac->get_access_path();
        
        foreach ($access_path as $path) {
            if (isset($data[$path['app_name']]) &&
                isset($data[$path['app_name']][$path['action_name']]) &&
                isset($data[$path['app_name']][$path['action_name']][$path['method_name']])
            ) {
                $data[$path['app_name']][$path['action_name']][$path['method_name']][$path['role_id'] - 1] = 1;
            }
        }

        $this->assign('data', json_encode($data));
        $this->display();
    }

    public function update()
    {
        trace_user(__CLASS__, __FUNCTION__, "in");
        if (!APP_DEBUG) {
            return;
        }
        
        $data = $_POST['data'];
        $rbac = new RbacModel();
        $r = $rbac->update_access($data);
        $this->ajaxReturn($r ? 1 : 0);
    }

    public function fetch_module()
    {
        $app = $this->get_app_name();
        $modules = $this->get_module();
        foreach ($modules as $module) {
            $functions = $this->get_function($module);
            foreach ($functions as $function) {
                $data[$app][$module][$function] = array(0, 0, 0);
            }
        }
        return $data;
    }

    protected function get_app_name()
    {
        return APP_NAME;
    }

    protected function get_module()
    {
        $group_path = LIB_PATH . 'Action/';
        if (!is_dir($group_path)) {
            return null;
        }
        
        $group_path .= '/*.class.php';
        $ary_files = glob($group_path);
        foreach ($ary_files as $file) {
            if (is_dir($file)) {
                continue;
            } 
            else {
                $file_tmp = basename($file, 'Action.class.php');
                if (false === strchr(C('NOT_AUTH_MODULE'), $file_tmp)) {
                    $files[] = $file_tmp;
                }
            }
        }
        return $files;
    }

    protected function get_function($module)
    {
        if (empty($module)) {
            return null;
        }
        
        $action = A($module);

        $data_module = substr($module, -4) == 'Data';

        $functions = get_class_methods($action);
        $inherents_functions = array(
            '_initialize', '__construct', 'getActionName', 'isAjax', 'display', 'show', 'fetch',
            'buildHtml', 'assign', '__set', 'get', '__get', '__isset',
            '__call', 'error', 'success', 'ajaxReturn', 'redirect', '__destruct', 'theme'
        );
        foreach ($functions as $func) {
            if (in_array($func, $inherents_functions)) {
                continue;
            }

            if ($data_module && substr($func, -2) != '_a') {
                continue;
            }

            $customer_functions[] = $func;
        }
        return $customer_functions;
    }
}
