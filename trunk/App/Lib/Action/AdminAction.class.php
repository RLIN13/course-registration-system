<?php

class AdminAction extends BaseAction
{
    private $_admin_data = null;
    
    function _initialize()
    {
        parent::_initialize();
        
        $this->_admin_data = new AdminDataAction();
        
        $acc = new AccountDataAction();
        $name = $acc->get_my_name();
        array_urlencode($name);
        $this->assign('name', urldecode(json_encode($name)));
    }

    public function index()
    {
        $this->redirect('./Admin/admin_course_manage');
    }


    public function course_student()
    {
        $id = safe_get_b('id');       

        $course_data = new CourseDataAction();

        $data = array();
        $data['data'] = $course_data->read_course_student($id);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/course_student');
        $content .= $this->fetch('./Admin/footer');
        
        $this->show($content);
    }


    public function class_manage()
    {
        $grade = safe_get_b('grade');

        $class = new ClassDataAction();

        $data = array();
        $data['data'] = $class->get($grade);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/class_manage');
        $content .= $this->fetch('./Admin/footer');
        $this->show($content);
    }

    public function add_class()
    {
        $class = new ClassDataAction();
        $data = array();
        $data['class_list'] = $class->get_grade_class_map();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/add_class');
        $this->show($content);
    }

    public function update_class()
    {
        $id = safe_get_b('id');

        $class = new ClassDataAction();
        $data = array();
        $data['class_list'] = $class->get_grade_class_map();
        $data['data'] = $class->get($id);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/update_class');
        $this->show($content);
    }
    
    public function admin_course_manage() {
        $key = safe_get_b('keyword');
        $grade = safe_get_b('grade');
        $semester = safe_get_b('semester');
        $page = safe_get_b('page');
        $page_size = safe_get_b('page_size');

        $data = $this->_admin_data->admin_get_course_data($key, $grade, $semester, $page, $page_size);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_course_manage');
        $content .= $this->fetch('./Admin/footer');
        $this->show($content);
    }
    
    public function admin_single_course_manage() {
        $co_id = safe_get_b('co_id');
        
        $data = $this->_admin_data->admin_get_single_course_data($co_id);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/admin_single_course_manage');

        $this->show($content);
     }
     
    public function admin_select_time_manage() {
        $data = $this->_admin_data->admin_get_select_time_data();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_select_time_manage');
        $content .= $this->fetch('./Admin/footer');
        
        $this->show($content);
    }
    
    public function admin_special_student_manage()
    {
        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_special_student_manage');
        $content .= $this->fetch('./Admin/footer');
        $this->show($content);
    }

    public function admin_select_setting_manage()
    {
        $data = $this->_admin_data->admin_get_select_setting_data();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_select_setting_manage');
        $content .= $this->fetch('./Admin/footer');
        $this->show($content);
    }
    
    public function admin_direction_manage()
    {
        $data = $this->_admin_data->admin_get_direction_data();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_direction_manage');
        $content .= $this->fetch('./Admin/footer');
        
        $this->show($content);
    }
    

    public function admin_credit_manage()
    {
        $keyword        = safe_get_b('keyword');               
        $grade          = safe_get_b('grade');                  
        $select_credit  = safe_get_b('select_credit');          
        $credit_bottom  = safe_get_b('credit_bottom');          
        $credit_top     = safe_get_b('credit_top');             
        $page           = safe_get_b('page');                   
        $page_size      = safe_get_b('page_size');              

        $data = $this->_admin_data->admin_get_credit_data($keyword, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_credit_manage');
        $content .= $this->fetch('./Admin/footer');
        
        $this->show($content);
    }
    
    public function admin_teacher_manage(){
        $keyword = safe_get_b('keyword');
        $page = safe_get_b('page');
        $page_size = safe_get_b('page_size');
        
        $data = $this->_admin_data->admin_get_teacher_data($keyword, $page, $page_size);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_teacher_manage');
        $content .= $this->fetch('./Admin/footer');
        
        $this->show($content);
    }
    
    public function admin_student_manage(){
        $keyword     = safe_get_b('keyword');                   
        $page        = safe_get_b('page');                      
        $page_size   = safe_get_b('page_size');                 
        $grade       = safe_get_b('grade');                     
        $class       = safe_get_b('class');                     
                
        $data = $this->_admin_data->admin_get_student_data($keyword, $grade, $class, $page, $page_size);
        
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_student_manage');
        $content .= $this->fetch('./Admin/footer');
        
        $this->show($content);
    }
    
    public function admin_account_manage() {
        $content = $this->fetch('./Admin/header');
        $content .= $this->fetch('./Admin/admin_account_manage');
        $content .= $this->fetch('./Admin/footer');

        $this->show($content);
    }

    public function admin_single_student_manage(){
        $id = safe_get_b("id");
        
        $data = $this->_admin_data->admin_get_single_student_data($id);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/admin_single_student_manage');
        $this->show($content);
    }

    public function admin_single_teacher_manage(){
        $t_id = safe_get_b('t_id');
        
        $data = $this->_admin_data->admin_get_single_teacher_data($t_id);
        
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Admin/admin_single_teacher_manage');
        $this->show($content);
    }
}
