<?php

class CourseDataAction extends BaseAction
{
    protected $_course = null;
    protected $_course_student = null;
    protected $_direction = null;
    protected $_leading_course = null;
    protected $_course_time = null;

    function _initialize() {
        parent::_initialize();
        
        $this->_course = new CourseModel();
        $this->_course_student = new CourseStudentModel();
        $this->_direction = new DirectionModel();
        $this->_leading_course = new LeadingCourseModel();
        $this->_course_time = new CourseTimeModel();
    }
    
    public function create_course_student_data($co_id, $s_id, $co_num) {
        return $this->_course_student->cs_create($co_id, $s_id, $co_num);
    }
    
    public function delete_coutse_student_data($co_id, $s_id) {
        return $this->_course_student->cs_delete($s_id, $co_id);
    }
    
    public function get_course_select_num($co_id) {
        $ret[] = $this->_course->co_get_selected_num($co_id);
        $ret[] = $this->_course->co_get_max_num($co_id);
        
        return $ret;
    }
    
    public function increase_selected_num($co_id) {
        return $this->_course->co_increase_selected_num($co_id);
    }
    
    public function decrease_selected_num($co_id) {
        return $this->_course->co_decrease_selected_num($co_id);
    }
    
    public function get_grade_list() {
        return $this->_course->get_grade_list();
    }

    public function get_semester_list() {
        return $this->_course->get_semester_list();
    }
    
    public function check_leading_course($s_id, $co_num) {
        $leading_course_nums = $this->_leading_course->lc_get_leading_course_nums($co_num);
        if ($leading_course_nums === FALSE)
        {
            return TRUE;
        }
        
        foreach ($leading_course_nums as $co_num) 
        {
            if ($this->check_had_selected_course($s_id, $co_num) === FALSE)
            {
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    public function check_had_selected_course($s_id, $co_num) {
        $cs_nums = $this->_course_student->cs_get_course_numbers($s_id);
        
        return in_array($co_num, $cs_nums);
    }

    public function check_course_time_conflict($s_id, $co_id) {
        $select_course_times = $this->_course_time->ct_get($co_id);
        $selected_course_ids = $this->_course_student->cs_get_course_ids($s_id);
        
        foreach ($selected_course_ids as $selected_co_id)
        {
            $selected_course_times = $this->_course_time->ct_get($selected_co_id);
            if ($selected_course_times === FALSE)
            {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                continue;
            }
            
            if ($select_course_times['0']['ct_day'] === $selected_course_times[0]['ct_day'] &&
                    $select_course_times['0']['ct_time'] | $selected_course_times[0]['ct_time'])
            {
                return TRUE;
            }
        }
        
        return FALSE;
    }

    public function upload_course_data($path) {
        $ret = array(TRUE, '导入出错');

        do 
        {
            vendor('Excel.reader');
            $data = new Spreadsheet_Excel_Reader();
            $data->setOutputEncoding('UTF-8');
            $data->read($path);
            error_reporting(E_ALL ^ E_NOTICE);

            if ($data->sheets[0]['numCols'] != 9) 
            {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                $ret[1] = '上传文件格式错误';
                break;
            }

            $parsed_course_data = array();
            $parsed_leading_course_data = array();
            $failed_line_num = array();
            foreach ($data->sheets[0]['cells'] as $line_num => $line) 
            {
                if ($line_num == 1) 
                {
                    continue;
                }

                $parse_ret = $this->parse_course($line);
                if (false === $parse_ret) 
                {
                    trace_user(__CLASS__, __FUNCTION__, __LINE__);
                    $failed_line_num[] = $line_num;
                    continue;
                }
                $parsed_course_data[] = $parse_ret;

                //解析前导课
                if (isset($line[6])) 
                {
                    $parsed_leading_course_data[$parse_ret['id'][2]] = $this->parse_leading_course($line[6]);
                }

            }

            $ret[0] =$this->_course->co_upload($parsed_course_data);

            if (false === $ret[0]) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                $ret[1] = '导入失败';
                break;
            }

            $leading_course = new LeadingCourseModel();
            $ret[0] = $leading_course->batch_add($parsed_leading_course_data);

            if (false === $ret[0]) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                $ret[1] = '导入失败';
                break;
            }

            if (count($failed_line_num) > 0) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                $ret[0] = false;
                $ret[1] = '以下行数导入失败：' . json_encode($failed_line_num);
                break;
            }

            $ret[1] = '导入成功';
        } while (0);

        return $ret;
    }
    
    protected function parse_course($line) {
        $ret = false;

        do {

            if (!isset($line[1]) || 
                !isset($line[2]) || 
                !isset($line[3]) || !is_numeric($line[3]) || 

                !isset($line[5]) || !is_numeric($line[5]) ||

                !isset($line[7]) || !is_numeric($line[7]) || 
                !isset($line[8]) || 
                !isset($line[9])
            ) 
            {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }

            $course_data = array();

            $parse_ret = CourseDataAction::parse_id($line[1]);
            if (false === $parse_ret) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }
            $course_data['id'] = $parse_ret;

            $parse_ret = CourseDataAction::parse_week($line[8]);
            if (false === $parse_ret) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }
            $course_data['week'] = $parse_ret;
            
            $parse_ret = CourseTimeModel::string_to_time_array($line[9]);
            if (false === $parse_ret) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }

            $course_data['time'] = $parse_ret;
            $course_data['leading_course'] = isset($line[6]);
            $course_data['name'] = $line[2];
            $course_data['grade'] = $line[3];

            if (!isset($line[4]) || $line[4] === '')
            {
                $course_data['direction'] = '';
            }
            else 
            {
                $course_data['direction'] = $this->_direction->get_id_by_name($line[4]);
            }
            $course_data['max_student_num'] = $line[5];
            $course_data['credit'] = $line[7];

            $ret = $course_data;
        } while (0);

        return $ret;
    }
    
    protected static function parse_week($week) {
        $ret = false;
        $week_pattern = '/^(\d{1,})-(\d{1,})/';
        $matchs = array();
        if (preg_match($week_pattern, $week, $matchs))
        {
            $ret = $matchs;
        }
        
        return $ret;
    }

    protected static function parse_id($id) {
        $ret = false;
        $id_pattern = '/^(\d{4}-\d{4}-\d)-(\d{6,})-([a-zA-Z]\d{4,})-(\d)/';
        $matches = array();
        if (preg_match($id_pattern, $id, $matches)) {
            $ret = $matches;
        }

        return $ret;
    }

    protected static function parse_time($time) {
        $ret = false;
        do {
            $time_pattern = '/周(.+?)第([\d,]+?)节/';
            $matches = array();
            if (!preg_match_all($time_pattern, $time, $matches)) {
                break;
            }

            $ret = array();

            foreach ($matches[1] as $key => $day) {
                switch ($day) {
                    case '一':
                        $day = 1;
                        break;
                    case '二':
                        $day = 2;
                        break;
                    case '三':
                        $day = 3;
                        break;
                    case '四':
                        $day = 4;
                        break;
                    case '五':
                        $day = 5;
                        break;
                    case '六':
                        $day = 6;
                        break;
                    default:
                        $day = 7;
                }
                $ret[$day] = 0;

                $class = $matches[2][$key];
                $class = explode(',', $class);
                foreach ($class as $num) {
                    if (!is_numeric($num)) {
                        $ret = false;
                        break;
                    }
                    $ret[$day] |= (1 << ((int)$num - 1));
                }

                if (false === $ret) {
                    break;
                }
            }

        } while (0);

        return $ret;
    }

    protected static function parse_leading_course($leading_course){
        $ret = false;

        do {
            if (empty($leading_course)) {
                break;
            }

            $leading_course = explode(',', $leading_course);

            $ret = array();
            foreach ($leading_course as $leading_id) {
                if (!is_numeric($leading_id)) {
                    $ret = false;
                    break;
                }

                $ret[] = $leading_id;
            }

        } while (0);

        return $ret;
    }

    public function get_course_student_count($co_id, $page, $page_size) {
        $ret = $this->_course_student->get_course_student_id($co_id, $page, $page_size, TRUE);
        return $ret;
    }

    public function get_course_student_id($co_id, $page, $page_size) {
        $ret = $this->_course_student->get_course_student_id($co_id, $page, $page_size, FALSE);
        return $ret;
    }
    

    public function read_course_student($id)
    {
        $ret = $this->_course->read_course_student($id);
        return $ret;
    }

    public function get_course_num($course_id)
    {
        $course_id = trim($course_id);
        $course_number = substr($course_id, 12, 6);
        return $course_number;
    }

    public function get_selectable_course_data() {
        $ret = $this->_course->co_get_selectable_course();

        if (false !== $ret) 
        {
            TeacherDataAction::get_name_in_array($ret, array('*', 'co_teacher'), 'co_teacher_name');
            DirectionDataAction::get_name_in_array($ret, array('*', 'co_direction'), 'co_direction_name');
            CourseTimeModel::db_to_string_in_array($ret, array('*', 'link_time'), 'link_time');
        }

        return $ret;
    }

    public function read_all_course_info($page,$page_size,$count){

        $ret = $this->_course->read_grab_course($page, $page_size, $grade, $count);

        if (false !== $ret) {
            TeacherDataAction::get_name_in_array($ret, array('*', 'co_teacher'), 'co_teacher_name');
            DirectionDataAction::get_name_in_array($ret, array('*', 'co_direction'), 'co_direction_name');
            CourseTimeModel::db_to_string_in_array($ret, array('*', 'link_time'), 'link_time');
        }

        return $ret;
    }


    public function get_course_schedule($stu_id, $semester, $page, $page_size, $count = false){
        $course = new CourseModel();
        $ret = $course->get_course($stu_id, $semester, $page, $page_size, $count);
        if ($ret && !$count) {
            TeacherDataAction::get_name_in_array($ret, array('*', 'co_teacher'), 'co_teacher_name');
            DirectionDataAction::get_name_in_array($ret, array('*', 'co_direction'), 'co_direction');
            CourseTimeModel::db_to_string_in_array($ret, array('*', 'link_time'), 'link_time_string');
            CourseTimeModel::db_to_time_array_in_array($ret, array('*', 'link_time'), 'link_time');
        }
        return $ret;
    }
    
    public function get_selected_course($s_id) {
        $selected_co_ids = $this->_course_student->cs_get_course_ids($s_id);
        if ($selected_co_ids === FALSE)
        {
            return FALSE;
        }
        
        $ret = array();
        foreach ($selected_co_ids as $co_id) {
            $co_data = $this->_course->co_get($co_id);
            if ($co_data === FALSE)
            {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                continue;
            }
            $ret[] = $co_data[0];
        }

        return $ret;
    }

    public function get_student_select_course($stu_id)
    {
        $course = new CourseModel();
        $ret = $course->get_student_select_course($stu_id);
        if (!empty($ret)) {
            TeacherDataAction::get_name_in_array($ret, array('*', 'co_teacher'), 'co_teacher_name');
            DirectionDataAction::get_name_in_array($ret, array('*', 'co_direction'), 'co_direction');
            CourseTimeModel::db_to_string_in_array($ret, array('*', 'link_time'), 'link_time_string');
            CourseTimeModel::db_to_time_array_in_array($ret, array('*', 'link_time'), 'link_time');
        }
        return $ret;
    }
    

    public function search_course_data($keyword, $grade, $semester, $page, $page_size, $count)
    {
        $ret = FALSE;

        do{
            $result = $this->_course->co_search($keyword, $grade, $semester, $page, $page_size, $count);

            if (FALSE === $result) {
                break;
            }

            DirectionDataAction::get_name_in_array($result, array('*', 'co_direction'), 'co_direction');
            CourseTimeModel::db_to_string_in_array($result, array('*', 'link_time'), 'time', TRUE);
            
            $ret = $result;
        }while(0);

        return $ret;
    }
    
    public function update_course_data($id, $name, $grade, $direction, $max_num, $leading_course, $introduction, $credit) {
        $ret = $this->_course->co_update($id, $name, $grade, $direction, $max_num, $leading_course, $introduction, $credit);
        return $ret;
    }

    public function get_course_data($co_id) {
        $ret = $this->_course->co_find($co_id, true);

        if (FALSE !== $ret) 
        {
            TeacherDataAction::get_name_in_array($ret, array('co_teacher'), 'co_teacher');
            DirectionDataAction::get_name_in_array($ret, array('co_direction'), 'co_direction');
            CourseTimeModel::db_to_string_in_array($ret, array('link_time'), 'link_time');
        }
        
        return $ret;
    }
    

}
