<?php


class SelectDataAction extends BaseAction
{
    public function after_selecting_direction_second_a()
    {   
        $ret_data = '';
        $ret_info = '绑定失败';
        $ret_status = 0;
        do {
            $ret = $this->after_selecting_direction_second();

            if (false === $ret[0]) {
                $ret_data = FALSE;
                $ret_info = $ret[1];
                break;
            }

            $ret_data = true;
            $ret_info = '选择成功';
            $ret_status = 1;
        } while (0);
        $this->ajaxReturn($ret_data,$ret_info,$ret_status);
    }

    public function after_selecting_direction_second()
    {
        $ret = array(false, array());
        do{
            $stu = new StudentModel();
            $stu_record = $stu->s_get_id_did();
            if(FALSE === $stu_record)
            {   $data = array();
                $data['info']= '绑定失败，原因：没有学生';
                array_push($ret[1], $data);
                break;
            }
            foreach($stu_record as $s_id => $s_did)
            {
                $student_id = $s_id;
                $direction_id = $s_did;
                if($direction_id ===null)
                {
                   $data = array();
                   $data['info'] = '绑定失败，原因:该学生没选方向';
                   $data['student_id'] = $student_id;
                   array_push($ret[1], $data);
                   continue;
                }
                $course = new CourseModel();
                $course_record = $course->get_course_id_number($direction_id);
                if(FALSE === $course_record)
                {
                    $data = array();
                    $data['info'] = '绑定失败，原因:该方向没有课程';
                    $data['student_id'] = $student_id;
                    $data['direction_id'] = $direction_id;
                    array_push($ret[1], $data);
                    continue;
                }
                 foreach ($course_record as $c_id => $c_number)
                 {
                     $course_id = $c_id;
                     $course_number = $c_number;
                     $course_ret = $course->co_increase_selected_num($course_id);
                     if(FALSE === $course_ret)
                     {
                         $data = array();
                         $data['info'] = '绑定失败，原因:该课程已满员';
                         $data['student_id'] = $student_id;
                         $data['course_id'] = $course_id;
                         array_push($ret[1], $data);
                         continue;
                     }
                     $course_stu = new CourseStudentModel();
                     $course_stu_ret = $course_stu->cs_create($course_id, $student_id, $course_number);
                     if($course_stu_ret ===1)
                     {
                         continue;
                     }
                     $course->co_decrease_selected_num($course_id);
                 }
            }
        }while(0);
        if (empty($ret[1]))
        {
             $ret[0] = TRUE;
             $ret[1] = '绑定成功';
        }
        return $ret;
    }

    public function after_selecting_course_a()
    {
        $ret_data = '';
        $ret_info = '设置失败';
        $ret_status = 0;

        do {
            $ret = $this->after_selecting_course();

            if (false === $ret) {
                break;
            }

            $ret_data = true;
            $ret_info = '设置成功';
            $ret_status = 1;
        } while (0);

        $this->ajaxReturn($ret_data,$ret_info,$ret_status);
    }

    public function after_selecting_course(){
        $course = new CourseModel();
        $ret = $course->set_all_selected();
        return $ret;
    }
}
