<?php

class SettingDataAction extends BaseAction {
    protected $_setting = null;
    protected $_select_grade = null;

    function _initialize()
    {
        parent::_initialize();
        $this->_setting = new SettingModel();
        $this->_select_grade = new SelectGradeModel();
    }

    public function is_select_first_time() {
        $times = $this->_setting->get_select_first_times();
        
        $start_time = strtotime($times[0]['se_direction_one_start_time']);
        $end_time = strtotime($times[0]['se_direction_one_end_time']);
        $current_time = strtotime("now");
        
        if ($current_time >= $start_time && $current_time <= $end_time)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function is_select_second_time() {
        $times = $this->_setting->get_select_second_times();
        
        $start_time = strtotime($times[0]['se_direction_two_start_time']);
        $end_time = strtotime($times[0]['se_direction_two_end_time']);
        $current_time = strtotime("now");
        
        if ($current_time >= $start_time && $current_time <= $end_time)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function is_select_third_time() {
        $times = $this->_setting->get_select_third_times();
        
        $start_time = strtotime($times[0]['se_course_start_time']);
        $end_time = strtotime($times[0]['se_course_end_time']);
        $current_time = strtotime("now");
        
        if ($current_time >= $start_time && $current_time <= $end_time)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function is_select_direction_grade($student_grade) {
        $select_grade = $this->_setting->get_direction_grade();
        
        if ($student_grade == $select_grade)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function is_select_course_grade($student_grade) {
        $select_grades = $this->_select_grade->get_select_grades();
        
        if (in_array($student_grade, $select_grades))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function get_select_times() {
        return $this->_setting->get_times();
    } 
    
    public function get_select_grades_map() {
        $class_data = new ClassDataAction();
        $grades = $class_data->get_grade_list();
        
        $select_grades = $this->_select_grade->get_select_grades();
        
        $data = array();
        foreach ($grades as $grade) 
        {
            if (in_array($grade, $select_grades))
            {
                $data[] = array($grade, 1);
            }
            else
            {
                $data[] = array($grade, 0);
            }
        }

        return $data;
    }
    
    public function get_special_good_percent() {
        return $this->_setting->get_special_good_percent();
    }
    
    public function get_normal_good_percent() {
        return $this->_setting->get_normal_good_percent();
    }
    
    public function get_direction_grade() {
        return $this->_setting->get_direction_grade();
    }
    
    public function update_setting_data($special_good_percent, $normal_good_percent, $direction_grade, $course_grades) {
        $ret = FALSE;
        
        do
        {
            $result = $this->_setting->se_update(false, false, false, false, false, false, $special_good_percent, $normal_good_percent, $direction_grade);

            if ($result === FALSE) 
            {
                break;
            }

            $result = $this->_select_grade->update_select_grades($course_grades);
            
            if ($result === FALSE) 
            {
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        return $ret;
    }
    
    public function update_time($one_start_time, $one_end_time, $two_start_time, $two_end_time, $three_start_time, $three_end_time) {
        $one_start_time = empty($one_start_time) ? false : str_replace('_',' ',$one_start_time);
        $one_end_time = empty($one_end_time) ? false : str_replace('_',' ',$one_end_time);
        $two_start_time = empty($two_start_time) ? false : str_replace('_',' ',$two_start_time);
        $two_end_time = empty($two_end_time) ? false : str_replace('_',' ',$two_end_time);
        $three_start_time = empty($three_start_time) ? false : str_replace('_',' ',$three_start_time);
        $three_end_time = empty($three_end_time) ? false : str_replace('_',' ',$three_end_time);

        $date_time_arr = array($one_start_time, $one_end_time, $two_start_time, $two_end_time, $three_start_time, $three_end_time);

        $ret = false;
        do
        {
            $ret = true;
            foreach($date_time_arr as $data)
            {
                $ret = $this->check_data_time($data);
                if (!$ret) {
                    break;
                }
            }

            if(false === $ret)
            {
                break;
            }

            $ret = $this->_setting->se_update($one_start_time, $one_end_time, $two_start_time, $two_end_time,
                $three_start_time, $three_end_time);
        }while(0);

        return $ret;
    }

    function check_date($date) {
        $dateArr = explode('-', $date);
        $ret = false;
        if (is_numeric($dateArr[0]) && is_numeric($dateArr[1]) && is_numeric($dateArr[2]))
        {
            $ret = checkdate($dateArr[1],$dateArr[2],$dateArr[0]);

        }
        return $ret;
    }

    function check_time($time) {
        $timeArr = explode(':', $time);
        if (is_numeric($timeArr[0]) && is_numeric($timeArr[1]) && is_numeric($timeArr[2]))
        {
            if (($timeArr[0] >= 0 && $timeArr[0] <= 23) && ($timeArr[1] >= 0 && $timeArr[1] <= 59) && ($timeArr[2] >= 0 && $timeArr[2] <= 59))
            {
                return true;
            }
        }
        return false;
    }

    public function check_data_time($data){
        $ret = false;
        do {
            $dataArr = explode(' ',$data);
            if (!isset($dataArr[0]) || !isset($dataArr[1])) {
                break;
            }

            $ret = $this->check_date($dataArr[0]); 
            if(false === $ret)
            {
                break;
            }

            $ret = $this->check_time($dataArr[1]);  
            if(false === $ret)
            {
                break;
            }
        } while (0);

        return $ret;
    }
}
