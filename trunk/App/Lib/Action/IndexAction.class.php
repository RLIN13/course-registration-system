<?php

class IndexAction extends BaseAction
{
    public function index()
    {
        $rbac = new RbacModel();
        $role = $rbac->get_role($_SESSION[C('USER_AUTH_KEY')]);
        if (C('ROLE_ADMIN') == $role) {
            $this->redirect('Admin/index');
        } elseif (C('ROLE_TEACHER') == $role) {
            $this->redirect('Teacher/index');
        } elseif (C('ROLE_STUDENT') == $role) {
            $this->redirect('Student/index');
        } else {
            $this->redirect('Public/login');
        }
    }
}