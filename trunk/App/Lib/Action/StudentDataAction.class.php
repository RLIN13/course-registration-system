<?php

class StudentDataAction extends BaseAction
{
    protected $_student = null;

    function _initialize()
    {
        parent::_initialize();
        $this->_student = new StudentModel();
    }

    public function student_get_course_data($semester, $page, $page_size) {
        $acc = new AccountDataAction();
        $stu_id = $acc->get_my_username();

        $course_data = new CourseDataAction();

        $data['count'] = $course_data->get_course_schedule($stu_id, $semester, $page, $page_size, true);
        $data['page'] = $page ? $page : 1;
        $data['data'] = $course_data->get_course_schedule($stu_id, $semester, $page, $page_size, false);
        $data['semester'] = $semester;
        $data['semester_list'] = $course_data->get_semester_list();
        
        return $data;
    }
    
    public function student_get_course_schedule($semester) {
        $acc = new AccountDataAction();
        $stu_id = $acc->get_my_username();

        $course_data = new CourseDataAction();
        $data['semester_list'] = $course_data->get_semester_list();

        if (empty($semester)) {
            $semester = $data['semester_list'][0];
        }

        $data['semester'] = $semester;
        $data['data'] = $course_data->get_course_schedule($stu_id, $semester, 0, 1000, false);
        
        return $data;
    }
    
    public function student_get_direction_first_data() {
        $account_data = new AccountDataAction();
        $username = $account_data->get_my_username();
        
        $selecting_direction_data = new SelectingDirectionDataAction();
        $data = $selecting_direction_data->get_direction_first($username);
        
        return $data;
    }
    
    public function student_get_direction_second_data() {
        G('get');
        $account_data = new AccountDataAction();
        $username = $account_data->get_my_username();
        
        $selecting_direction_data = new SelectingDirectionDataAction();
        $data = $selecting_direction_data->get_direction_second($username);
        echo G('get', 'end');
        return $data;
    }
    
    public function student_get_course_third_data() {
        $account_data = new AccountDataAction();
        $username = $account_data->get_my_username();
        
        $selecting_direction_data = new SelectingDirectionDataAction();
        $data = $selecting_direction_data->get_course_third($username);

        return $data;
    }
    
    public function student_get_credit_data() {
        $acc = new AccountDataAction();
        $id = $acc->get_my_username();

        $stu = new StudentDataAction();
        $data = array();
        $data['data'] = $stu->get_credit($id);
        
        return $data;
    }
    
    public function student_get_student_info() {
        $student_data = new StudentDataAction;
        $data=array();
        $data['data']=$student_data->get_single_student();
        
        return $data;
    }
    
    public function student_get_single_course_data($id) {
        $course = new CourseDataAction();

        $data = array();
        $data['data'] = $course->get_course_data($id);
        
        return $data;
    }
    
    public function student_get_single_teacher_info($t_id) {
        $teacher = new TeacherModel();
        $result = $teacher->t_find($t_id);
        if (!empty($result)) {
            $result = $result[0];
        }

        $data = array();
        $data['data'] = $result;
        
        return $data;
    }

    public function student_select_direction_first_a() {
        $ret_data = FALSE;
        $ret_info = '选课失败';
        $ret_status = 0;

        $data = array(); 
        $level = 1;
        $account_data = new AccountDataAction();
        $username = $account_data->get_my_username();
        $grade = $this->get_grade($username);

        while (($direction_id = safe_post_b('direction_id_' . $level)) != -1) {
            $choice = array();
            $choice[] = $direction_id;
            $choice[] = $level;

            $data[] = $choice;
            $level++;
            
            if(4 === $level){
                break;
            }
        }
        
        $selecting_direction_data = new SelectingDirectionDataAction();
        $result = $selecting_direction_data->select_direction_first($username, $grade, $data);
        
        if ($result[0] === FALSE)
        {
            $ret_info = $result[1];
        }
        else
        {
            $ret_data = TRUE;
            $ret_info = $result[1];
            $ret_status = 1;
        }

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function student_select_direction_second_a() {
        $ret_data = FALSE;
        $ret_info = '选择失败';
        $ret_status = 0;

        $d_id = safe_post_b('selected_direction');
        
        $account = new AccountDataAction();
        $username = $account->get_my_username();
        $grade = $this->get_grade($username);
        
        do 
        {
            if (false === $d_id) 
            {
                $ret_info = '选择失败，你没选方向';
                break;
            }

            $selecting_direction_data = new SelectingDirectionDataAction();
            $ret = $selecting_direction_data->select_direction_second($username, $grade, $d_id);

            if (false === $ret[0]) 
            {
                $ret_info = $ret[1];
                break;
            }

            $ret_data = TRUE;
            $ret_info = '选择成功';
            $ret_status = 1;
        } while (0);

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function student_cancle_direction_second_a() {
        $ret_data = '';
        $ret_info = '退选失败';
        $ret_status = 0;
        
        $direction_id = safe_post_b('d_id');
        
        $account = new AccountDataAction();
        $username = $account->get_my_username();
        $grade = $this->get_grade($username);
        
        do {
            $selecting_direction_data = new SelectingDirectionDataAction();
            $ret = $selecting_direction_data->cancle_direction_second($username, $grade, $direction_id);

            if (FALSE === $ret[0]) {
                $ret_data = FALSE;
                $ret_info = $ret[1];
                break;
            }

            $ret_data = TRUE;
            $ret_info = $ret[1];
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data,$ret_info,$ret_status);
    }
    
    public function student_select_course_third_a() {
        $ret_data = FALSE;
        $ret_info = '选择失败';
        $ret_status = 0;
        
        $co_id = safe_post_b('co_id');

        $account = new AccountDataAction();
        $username = $account->get_my_username();
        $grade = $this->get_grade($username);
        
        do 
        {
            if (FALSE === $co_id) 
            {
                $ret_info = '选择失败，你没选任何课程。';
                break;
            }

            $selecting_direction_data = new SelectingDirectionDataAction();
            $ret = $selecting_direction_data->select_course_third($username, $grade, $co_id);

            if (FALSE === $ret[0]) {
                $ret_info = $ret[1];
                break;
            }

            $ret_data = true;
            $ret_info = $ret[1];
            $ret_status = 1;
        } while (0);

        $this->ajaxReturn($ret_data,$ret_info,$ret_status);
    }
    
    public function student_cancle_course_third_a() {
        $ret_data = FALSE;
        $ret_info = '退课失败';
        $ret_status = 0;
        
        $co_id = safe_post_b("co_id");
        
        $account = new AccountDataAction();
        $username = $account->get_my_username();
        $grade = $this->get_grade($username);
        
        do {
            $selecting_direction_data = new SelectingDirectionDataAction();
            $ret = $selecting_direction_data->cancle_course_third($username, $grade, $co_id);

            if (false === $ret[0]) {
                $ret_info = $ret[1];
                break;
            }

            $ret_data = true;
            $ret_info = '退课成功';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data,$ret_info,$ret_status);
    }

    public function student_update_student_info_a() {
        $phone = safe_post_b('phone');
        $email = safe_post_b('email');
        
        $ret_data = '';
        $ret_info = '修改失败';
        $ret_status = 0;
        
        $account = new AccountDataAction();
        $username = $account->get_my_username();
        
        do 
        {
            $result = $this->update_student_info($username, FALSE, FALSE, FALSE, FALSE, $phone, $email, FALSE, FALSE, FALSE);
            
            if ($result = FALSE)
            {
                break;
            }
            
        $ret_info = '修改成功';
        $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data,$ret_info,$ret_status);
    }


    public function search_credit_data($keyword, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size, $count)
    {
        return $this->_student->s_search_credit($keyword, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size, $count);
    }
    
    public function search_student_info($keyword, $grade, $class, $page, $page_size, $count){
        return $this->_student->s_search($keyword, $grade, $class, $page, $page_size, $count);
    }

    public function find_student_info($s_id) {
        $result = $this->_student->s_find($s_id);
        return $result;
    }
    
    public function update_student_info($id, $name, $sex, $grade, $class, $phone, $email, $direction, $lib, $good) {
        $result = $this->_student->s_update($id, $name, $sex, $grade, $class, $phone, $email, $direction, $lib, $good);
        return $result;
    }
    
    public function delete_student_info() {
        
    }
    
    public function remove_student_info($s_ids) {
        $ret = FALSE;
        
        do
        {
            $result = $this->_student->s_remove($s_ids);
            if ($result === FALSE)
            {
                break;
            }
            
            $rbac = new RbacModel();
            $result = $rbac->remove_users($s_ids);
            if ($result === FALSE)
            {
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        return $ret;
    }
    
    public function create_student_info() {
        
    }
    
    public function upload_student_info($path) {
        $result = array(FALSE, '导入出错');
        
        do{
            vendor('Excel.reader');
            $excel_reader = new Spreadsheet_Excel_Reader;
            $excel_reader->setOutputEncoding('UTF-8');
            $excel_reader->read($path);

            //判断Excel文件是否符合标准
            if($excel_reader->sheets[0]['numCols'] < 11){
                $result[1] = '格式错误';
                break;
            }

            $line_unset = array();  //记录被删除掉的行

            //逐行检测Excel数据
            foreach ($excel_reader->sheets[0]['cells'] as $line_num => $line) {
                //去除表格头
                if($line_num == 1){
                    unset($excel_reader->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //表格前两项为必填信息
                if(count($line) < 2){
                    unset($excel_reader->sheets[0]['cells'][$line_num]);
                    $line_unset[] = $line_num;
                    continue;
                }
            }

            //存入数据库teacher表
            $result[0] = $this->_student->s_upload($excel_reader->sheets[0]['cells']);
            
            if($result[0] == FALSE){
                $result[1] = '导入数据库失败';
                break;
            }
            
            //存入Rbac表
            $rabc_model = new RbacModel();
            $result[0] = $rabc_model->add_user($excel_reader->sheets[0]['cells'], C('ROLE_TEACHER'));
            
            if($result[0] == FALSE){
                $result[1] = '导入数据库失败';
                break;
            }

            if(count($line_unset) > 0){
                $result[0] = FALSE;
                $result[1] = '以下行信息缺失：' . json_encode($line_unset);
                break;
            }
            
            $result[1] = '导入成功';
        }while (0);
        
        return $result;
    }

    public function upload_lab_student($path)
    {
        $ret = array(false, '导入实验室学生失败',0);
        $failed_line_num = array();

        do{
            //读取文件
            vendor('Excel.reader');
            $data = new Spreadsheet_Excel_Reader();
            $data->setOutputEncoding('UTF-8');
            $data->read($path);
            error_reporting(E_ALL ^ E_NOTICE);

            //判断格式
            if ($data->sheets[0]['numCols'] != 2 )
            {
                $ret[1] = '上传文件格式错误';

                break;
            }

            //去除不合法数据
            foreach ($data->sheets[0]['cells'] as $line_num => $line)
            {
                if ($line_num == 1) //忽略标题行，行数是从1开始的
                {
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //解析方向=>id
                //每次得到一个s_id 和对应的dir_id
                $parse_ret = $this->parse_direction($line);
    
                if (false === $parse_ret)
                {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //设置s_lib标记，写入数据库，设置成功返回true
                $ret_set_lib = $this->_student->s_set_lib($parse_ret['student_id'], $parse_ret['direction_id']);

                if(empty($ret_set_lib))
                {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }
            }

            if (count($failed_line_num) > 0)
            {
                $ret[0] = false;
                $ret[1] = '导入完成，但以下行数实验室学生导入失败：' . json_encode($failed_line_num .'请核查后重新导入错误行');
                break;
            }

            $ret = array(true, '导入实验室学生完成',1); //执行到这里说明已经完成了更新，并在ssd表内添加了记录

        }while(0);

        return $ret;
    }

    public function upload_special_student($path)
    {
        $ret = array(false, '导入卓越班学生失败', 0);
        $failed_line_num = array();

        do{
            //读取文件
            vendor('Excel.reader');
            $data = new Spreadsheet_Excel_Reader();
            $data->setOutputEncoding('UTF-8');
            $data->read($path);
            error_reporting(E_ALL ^ E_NOTICE);

            //判断格式
            if ($data->sheets[0]['numCols'] != 2)
            {
                $ret[1] = '上传文件格式错误';
                break;
            }

            //去除不合法数据
            foreach ($data->sheets[0]['cells'] as $line_num => $line)
            {
                if ($line_num == 1) //忽略标题行，行数是从1开始的
                {
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //解析方向=>id
                //每次得到一个s_id 和对应的dir_id
                $parse_ret = $this->parse_direction($line);

                if (false === $parse_ret)
                {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //设置卓越班学生的s_good标记，写入数据库，设置成功返回true
                $ret_set_good = $this->_student->s_set_good($parse_ret['student_id'], $parse_ret['direction_id']);

                if(empty($ret_set_good))
                {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }
            }

            if (count($failed_line_num) > 0)
            {
                $ret[0] = false;
                $ret[1] = '导入完成，但以下行数卓越班学生导入失败：' . json_encode($failed_line_num  .'请核查后重新导入错误行');
                break;
            }

            $ret = array(true, '导入卓越班学生完成',1); //执行到这里说明已经完成了更新，并在ssd表内添加了记录

        }while(0);

        return $ret;
    }


    public function upload_normal_student($path)
    {
        $ret = array(false, '导入普通班学生失败', 0);
        $failed_line_num = array();

        do{
            //读取文件
            vendor('Excel.reader');
            $data = new Spreadsheet_Excel_Reader();
            $data->setOutputEncoding('UTF-8');
            $data->read($path);
            error_reporting(E_ALL ^ E_NOTICE);

            //判断格式
            if ($data->sheets[0]['numCols'] != 2)
            {
                $ret[1] = '上传文件格式错误';
                break;
            }

            //去除不合法数据
            foreach ($data->sheets[0]['cells'] as $line_num => $line)
            {
                if ($line_num == 1) //忽略标题行，行数是从1开始的
                {
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //解析方向=>id
                //每次得到一个s_id 和对应的dir_id
                $parse_ret = $this->parse_direction($line);

                if (false === $parse_ret)
                {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                //设置卓越班学生的s_good标记，写入数据库，设置成功返回true
                $ret_set_good = $this->_student->s_set_good($parse_ret['student_id'], $parse_ret['direction_id']);

                if(empty($ret_set_good))
                {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }
            }

            if (count($failed_line_num) > 0)
            {
                $ret[0] = false;
                $ret[1] = '导入完成，但以下行数普通班学生导入失败：' . json_encode($failed_line_num  .'请核查后重新导入错误行');
                break;
            }

            $ret = array(true, '导入普通班学生完成',1); //执行到这里说明已经完成了更新，并在ssd表内添加了记录

        }while(0);

        return $ret;
    }

    public function get_students($s_ids) {
        $ret = FALSE;

        do
        {
            if (!is_array($s_ids))
            {
                break;
            }
            
            $result = array();
            
            foreach ($s_ids as $s_id) 
            {
                $s_id = reset($s_id);
                $data = $this->_student->s_find($s_id);

                if ($data === FALSE)
                {
                    trace_user(__CLASS__, __FUNCTION__, __LINE__);
                    continue;
                }
                array_push($result, $data[0]);
            }
            
            $ret = $result;
        } while(0);
 
        return $ret;
    }
    
    public function get_grade($s_id) {
        return $this->_student->s_get_grade($s_id);
    }
    
    public function get_direction($s_id) {
        return $this->_student->s_get_direction($s_id);
    }
    
    public function set_direction($s_id, $d_id) {
        return $this->_student->s_set_direction($s_id, $d_id);
    }
    
    public function del_direction($s_id) {
        return $this->_student->s_set_direction($s_id, 0);
    }
    
    public function get_direction_id_name($s_id) {
        $d_id = $this->_student->s_get_direction($s_id);

        if ($d_id === FALSE)
        {
            return FALSE;
        }
        
        $direction_data = new DirectionDataAction();
        $d_name = $direction_data->get_direction_name($d_id);
        $ret['d_id'] = $d_id;
        $ret['d_name'] = $d_name;

        return $ret;
    }

    protected function parse_direction($line)
    {
        $ret = false;

        //echo '这里是parse_direction</br>';
        do {
            //去除不合法数据
            if (!isset($line[1]) || //学号
                !isset($line[2]))    //所属方向
            {
                break;
            }

            $special_student_direction_data = array();

            $special_student_direction_data['student_id'] = $line[1];

            if (null === $this->_dir_id_map) {
                $dir = new DirectionModel();
                $this->_dir_id_map = $dir->get_dir_id_map();
            }

            if ('全方向' === $line[2])
            {
                $special_student_direction_data['direction_id'] = -1;
            }
            elseif (!isset($this->_dir_id_map[$line[2]]))
            {
                break;
            }
            else
            {
                $special_student_direction_data['direction_id'] = $this->_dir_id_map[$line[2]];
            }

            $ret = $special_student_direction_data;
        } while (0);
        //dump($ret);
        //返回 $ret['id']=学号 $ret['direction'] = 方向id
        return $ret;
    }

    public function get_grade_list()
    {
        return $this->_student->s_get_grades();
    }

    public function  get_single_student()
    {
        $account=new AccountDataAction();
        $sid=$account->get_my_username();
        $result = $this->find_student_info($sid);
        $dir = new DirectionModel();
        $s_dir = $dir->d_find($result[0]['s_direction']);
        $dir_name = $s_dir[0]['d_name'];
        $result['direction_name'] = $dir_name;
        $class = new ClassModel();
        $s_class = $class ->c_search(null, $result[0]['s_grade'], $result[0]['s_class'], null);
        $class_name = $s_class[0]['c_name'];
        $result['class_name'] = $class_name;
        return $result;
    }

    public function get_credit($id)
    {
        if (empty($id)) {
            return FALSE;
        }

        return $this->_student->s_get_credit($id);
    }
    
    
}