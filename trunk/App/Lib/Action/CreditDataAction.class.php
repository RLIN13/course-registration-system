<?php

class CreditDataAction extends BaseAction
{

    public function upload_score($path)
    {
        $ret = array(false, '导入出错');

        do{
            vendor('Excel.reader');
            $data = new Spreadsheet_Excel_Reader();
            $data->setOutputEncoding('UTF-8');
            $data->read($path);
            error_reporting(E_ALL ^ E_NOTICE);

            if ($data->sheets[0]['numCols'] != 3) {
                $ret[1] = '上传文件格式错误';
                break;
            }

            $failed_line_num = array();

            foreach ($data->sheets[0]['cells'] as $line_num => $line) {
                if ($line_num == 1) { 
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }

                if (count($line) < 3 || !is_numeric($line[3]) || $line[3] < 0) {
                    $failed_line_num[] = $line_num;
                    unset($data->sheets[0]['cells'][$line_num]);
                    continue;
                }
            }

            $course_student = new CourseStudentModel();
            $ret[0] = $course_student->set_pass($data->sheets[0]['cells'], true);

            if (false === $ret[0]) {
                $ret[1] = '导入失败';
                break;
            }

            if (count($failed_line_num) > 0) {
                $ret[0] = false;
                $ret[1] = '以下行数导入失败：' . json_encode($failed_line_num);
                break;
            }

            $ret[1] = '导入成功';
        }while(0);

        return $ret;
    }

    public function upload_score_a()
    {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;

        do {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }

            $url = $url[1];

            $ret = $this->upload_score($url);
            if (!$ret[0]) {
                $ret_info = $ret[1];
                break;
            }

            $this->update_credit();

            $ret_data = '';
            $ret_info = '导入成功';
            $ret_status = 1;

        }while(0);

        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }

    public function update_credit()
    {
        $stu = new StudentModel();
        $stu->update_pass_credit();
        $stu->update_select_credit();
    }

}
