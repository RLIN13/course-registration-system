<?php

class AdminDataAction extends BaseAction
{
    function _initialize() {
        parent::_initialize();
    }

    public function admin_get_course_data($key, $grade, $semester, $page, $page_size) {
        $ret = array();
        
        $course_data = new CourseDataAction();
        $ret['count'] = $course_data->search_course_data($key, $grade, $semester, $page, $page_size, TRUE);
        $ret['page'] = $page ? $page : '1';
        $ret['semester'] = $course_data->get_semester_list();
        $ret['grade'] = $course_data->get_grade_list();
        $ret['data'] = $course_data->search_course_data($key, $grade, $semester, $page, $page_size, FALSE);
        
        return $ret;
    }
    
    public function admin_get_single_course_data($co_id) {
        $ret = array();
        
        $course_data = new CourseDataAction();
        $ret['data'] = $course_data->get_course_data($co_id);
        $ret['grade'] = $course_data->get_grade_list();
        
        $direction_data = new DirectionDataAction();
        $ret['direction'] = $direction_data->get_id_names();
        
        return $ret;
    }
    
    public function admin_get_course_student($co_id, $page, $page_size) {
        $ret = array();
        
        $course_data = new CourseDataAction();
        $ret['count'] = $course_data->get_course_student_count($co_id, $page, $page_size);
        $ret['page'] = $page ? $page : 1;

        $course_student_ids = $course_data->get_course_student_id($co_id, $page, $page_size);

        $student_data = new StudentDataAction();
        $ret['data'] = $student_data->get_students($course_student_ids);

        $course = $course_data->get_course_data($co_id);
        $ret['co_name'] = $course['co_name'];
        
        return $ret;
    }
    
    public function admin_get_select_time_data() {
        $ret = array();
        
        $setting_data = new SettingDataAction();
        $ret['data'] = $setting_data->get_select_times();
        
        return $ret;
    }
    
    public function admin_get_select_setting_data() {
        $ret = array();
        
        $setting_data = new SettingDataAction();
        $ret['grade_list'] = $setting_data->get_select_grades_map();
        $ret['special_good_percent'] = $setting_data->get_special_good_percent();
        $ret['normal_good_percent'] = $setting_data->get_normal_good_percent();
        $ret['direction_grade'] = $setting_data->get_direction_grade();
        
        return $ret;
    }
    
    public function admin_get_direction_data() {
        $ret = array();
        
        $direction_data = new DirectionDataAction();
        $ret['data'] = $direction_data->get_direction_datas();
        
        return $ret;
    }
    
    public function admin_get_credit_data($key, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size) {
        $ret = array();
        
        $student_data = new StudentDataAction();
        $ret['count'] = $student_data->search_credit_data($key, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size, TRUE);
        $ret['page'] = $page ? $page : '1';
        $ret['grade'] = $student_data->get_grade_list();
        $ret['data'] = $student_data->search_credit_data($key, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size, FALSE);
    
        return $ret;
    }
    
    public function admin_get_teacher_data($keyword, $page, $page_size) {
        $ret = array();
        
        $teacher_data = new TeacherDataAction();
        $ret['count'] = $teacher_data->search_teacher_info($keyword, $page, $page_size, TRUE);
        $ret['page'] = $page ? $page : 1;
        $ret['data'] = $teacher_data->search_teacher_info($keyword, $page, $page_size, FALSE);
        
        return $ret;
    }
    
    public function admin_get_single_teacher_data($t_id) {
        $ret = array();
        
        $teaacher_data = new TeacherDataAction();
        $ret['data'] = $teaacher_data->find_teacher_info($t_id);
        
        return $ret;
    }
    
    public function admin_get_student_data($keyword, $grade, $class, $page, $page_size) {
        $ret = array();
        
        $student_data = new StudentDataAction();
        $ret['count'] = $student_data->search_student_info($keyword, $grade, $class, $page, $page_size, TRUE);
        $ret['page'] = $page ? $page : 1;
        $ret['data'] = $student_data->search_student_info($keyword, $grade, $class, $page, $page_size, FALSE);

        $class_data = new ClassDataAction();
        $ret['grade_list']= $class_data->get_grade_class_map();
        
        return $ret;
    }
    
    public function admin_get_single_student_data($s_id) {
        $ret = array();

        $student_data = new StudentDataAction();
        $ret['data'] = $student_data->find_student_info($s_id);
        
        $direction_data = new DirectionDataAction();
        $ret['direction'] = $direction_data->get_id_names();

        $class_data = new ClassDataAction();
        $ret['grade_class']= $class_data->get_grade_class_map();

        return $ret;
    }
    
    public function admin_get_class_data() {
        
    }

    public function admin_upload_course_data_a() {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;
        
        $course_data = new CourseDataAction();
        do
        {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }
            
            $url = $url[1];
            
            $result = $course_data->upload_course_data($url);
            if ($result[0] === FALSE) 
            {
                $ret_info = $result[1];
                break;
            }

            $ret_info = '导入成功';
            $ret_status = 1;
        } while (0);
        
        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_update_course_data_a() {
        $id = safe_post_b('id');                             
        $name = safe_post_b('name');                        
        $grade = safe_post_b('grade');                       
        $direction = safe_post_b('direction');              
        $credit = safe_post_b('credit');                     
        $introduction = safe_post_b('introduction');         
        $max_num = safe_post_b('max_num');                   
        $leading_course = safe_post_b('leading_course');     
        
        $ret_data = '';
        $ret_info = '修改失败!';
        $ret_status = 0;
        
        do 
        {
            $course_data = new CourseDataAction();
            $result = $course_data->update_course_data($id, $name, $grade, $direction, $max_num, $leading_course, $introduction, $credit);

            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '修改成功';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_updata_select_time_a() {
        $one_start_time   = safe_post_b('one_start_time');           
        $one_end_time     = safe_post_b('one_end_time');             
        $two_start_time   = safe_post_b('two_start_time');          
        $two_end_time     = safe_post_b('two_end_time');             
        $three_start_time = safe_post_b('three_start_time');        
        $three_end_time   = safe_post_b('three_end_time');          
        
        $ret_data = '';
        $ret_info = '修改失败!';
        $ret_status = 0;
        
        $setting_data = new SettingDataAction();
        do
        {
            $result = $setting_data->update_time($one_start_time, $one_end_time, $two_start_time, $two_end_time, $three_start_time, $three_end_time);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '修改成功!';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_upload_lab_student_a() {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;
        
        $student_data = new StudentDataAction();
        do
        {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }
            
            $url = $url[1];
            
            $result = $student_data->upload_lab_student($url);
            if ($result[0] === FALSE) 
            {
                $ret_info = $result[1];
                break;
            }

            $ret_info = '导入成功';
            $ret_status = 1;
        } while (0);
        
        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_upload_special_student_a() {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;
        
        $student_data = new StudentDataAction();
        do
        {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }
            
            $url = $url[1];
            
            $result = $student_data->upload_special_student($url);
            if ($result[0] === FALSE) 
            {
                $ret_info = $result[1];
                break;
            }

            $ret_info = '导入成功';
            $ret_status = 1;
        } while (0);
        
        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_upload_normal_student_a() {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;
        
        $student_data = new StudentDataAction();
        do
        {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }
            
            $url = $url[1];
            
            $result = $student_data->upload_normal_student($url);
            if ($result[0] === FALSE) 
            {
                $ret_info = $result[1];
                break;
            }

            $ret_info = '导入成功';
            $ret_status = 1;
        } while (0);
        
        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }

    public function admin_update_select_setting_a() {
        $special_good_percent = safe_post_b('special_good_percent');
        $normal_good_percent  = safe_post_b('normal_good_percent'); 
        $direction_grade = safe_post_b('direction_grade');                               
        $course_grades = safe_post_b('course_grades');
        
        $ret_data = '';
        $ret_info = '修改失败!';
        $ret_status = 0;
        
        $setting_data = new SettingDataAction();
        do
        {
            $result = $setting_data->update_setting_data($special_good_percent, $normal_good_percent, $direction_grade, $course_grades);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '修改成功!';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }

    public function admin_remove_direction_a() {
        $ids = safe_post_b('direction_id');         

        $ret_data = '';
        $ret_info = '删除失败!';
        $ret_status = 0;
        
        $direction_data = new DirectionDataAction();
        do
        {
            $result = $direction_data->remove_direction_data($ids);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '删除成功!';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_create_direction_a() {
        $name = safe_post_b('name');                     
        $num = safe_post_b('num');                       
        $introduction = safe_post_b('introduction');   

        $ret_data = '';
        $ret_info = '添加失败';
        $ret_status = 0;
        
        $direction_data = new DirectionDataAction();
        do
        {
            $result = $direction_data->create_direction_data($name, $num, $introduction);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '添加成功!';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_update_direction_a() {
        
    }
    
    
    
    public function admin_upload_score_a() {
        
    }
    
    
    
    
    
    public function admin_upload_teacher_a() {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;
        trace_user(__CLASS__, __FUNCTION__, __LINE__);
        $teacher_data = new TeacherDataAction();
        do
        {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }
            
            $url = $url[1];
            
            $result = $teacher_data->upload_teacher_info($url);
            if ($result[0] === FALSE) {
                $ret_info = $result[1];
                break;
            }

            $ret_info = '导入成功';
            $ret_status = 1;
        } while (0);
        
        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_remove_teacher_a() {
        $t_ids = safe_post_b('t_id');
        
        $ret_data = '';
        $ret_info = '删除失败';
        $ret_status = 0;
        
        $teacher_data = new TeacherDataAction();
        do
        {
            $t_ids = explode(',', $t_ids);
            $result = $teacher_data->remove_teacher_info($t_ids);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '删除成功!';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_update_teacher_a() {
        $t_id = safe_post_b('t_id');
        $t_name = safe_post_b('t_name');
        $t_sex = safe_post_b('t_sex');
        $t_phone = safe_post_b('t_phone');
        $t_email = safe_post_b('t_email');
        $t_introduction = safe_post_b('t_introduction');
        
        $ret_data = '';
        $ret_info = ' 修改失败';
        $ret_status = 0;
        
        $teacher_data = new TeacherDataAction();
        do
        {
            $result = $teacher_data->update_teacher_info($t_id, $t_name, $t_sex, $t_phone, $t_email, $t_introduction);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = ' 修改成功';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_upload_student_a() {
        $ret_data = '';
        $ret_info = '导入失败';
        $ret_status = 0;
        
        $student_data = new StudentDataAction();
        do
        {
            $url = get_upload_xls();
            if (!$url[0]) {
                $ret_info = $url[1];
                break;
            }
            
            $url = $url[1];
            
            $result = $student_data->upload_student_info($url);
            if ($result[0] === FALSE) {
                $ret_info = $result[1];
                break;
            }

            $ret_info = '导入成功';
            $ret_status = 1;
        } while (0);
        
        if (count($url) == 1 && file_exists($url)) {
            unlink($url);
        }
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_remove_student_a() {
        $s_ids = safe_post_b('ids');
        
        $ret_data = '';
        $ret_info = '删除失败';
        $ret_status = 0;

        $student_data = new StudentDataAction();
        do
        {
            $s_ids = explode(',', $s_ids);
            $result = $student_data->remove_student_info($s_ids);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '删除成功!';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_update_student_a() {
        $id = safe_post_b('s_id');
        $name = safe_post_b('s_name');
        $sex = safe_post_b('s_sex');
        $grade = safe_post_b('s_grade');
        $class = safe_post_b('s_class');
        $phone = safe_post_b('s_phone');
        $email = safe_post_b('s_email');
        $direction = safe_post_b('s_direction');
        $lib = safe_post_b('s_lib');
        $good = safe_post_b('s_good');

        $ret_data = '';
        $ret_info = '更新失败';
        $ret_status = 0;

        $student_data = new StudentDataAction();
        do
        {
            $result = $student_data->update_student_info($id, $name, $sex, $grade, $class, $phone, $email, $direction, $lib, $good);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = ' 修改成功';
            $ret_status = 1;
        } while (0);

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_after_direction_first_a() {
        $ret_data = '';
        $ret_info = '第一轮选课筛选失败';
        $ret_status = 0;
        
        $selecting_direction = new SelectingDirectionDataAction();
        do
        {
            $result = $selecting_direction->after_direction_first();
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '第一轮选课筛选成功';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_after_direction_second_a() {
        $ret_data = '';
        $ret_info = '第二轮选课确认失败';
        $ret_status = 0;
        
        $selecting_direction = new SelectingDirectionDataAction();
        do
        {
            $result = $selecting_direction->after_direction_second();
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '第二轮选课确认成功';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_after_direction_third_a() {
        $ret_data = '';
        $ret_info = '第三轮选课确认失败';
        $ret_status = 0;
        
        $selecting_direction = new SelectingDirectionDataAction();
        do
        {
            $result = $selecting_direction->after_course_third();
            if ($result === FALSE)
            {
                break;
            }
            
            $ret_info = '第三轮选课确认成功';
            $ret_status = 1;
        } while (0);
        
        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
    
    public function admin_reset_account_a() {
        
    }
}
