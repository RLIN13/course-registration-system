<?php

class SelectingDirectionDataAction extends BaseAction {
    private $_selecting_direction = null;
    
    function _initialize(){
        parent::_initialize();
        
        $this->_selecting_direction = new SelectingDirectionModel();
    }
    
    public function get_selected_directions ($s_id) {
        $ret = FALSE;
        
        do
        {
            $result = $this->_selecting_direction->sd_get($s_id);
            if ($result === FALSE)
            {
                break;
            }
            
            foreach ($result as $value) {
                $ret[$value['sd_level']] = $value['sd_direction_id'];
            }
        } while (0);
        
        return $ret;
    }
    
    public function create_selecting_direction($sd_student_id, $sd_direction_id, $sd_level) {
        return $this->_selecting_direction->sd_create($sd_student_id, $sd_direction_id, $sd_level);
    }
    
    public function remove_selecting_direction($sd_student_id) {
        return $this->_selecting_direction->sd_remove($sd_student_id);
    }
    
    public function filter_invalid_data($direction_grade) {
        $selecting_direction = new SelectingDirectionModel();
        $selecting_direction->filter_invalid_grade($direction_grade);
        $selecting_direction->filter_invalid_direction();
        $selecting_direction->filter_invalid_student();
    }
    
    public function get_direction_first($username) {
        if (C('REDIS_ON') === TRUE)
        {
            $data = $this->get_direction_first_r($username);
        }
        else
        {
            $data = $this->get_direction_first_m($username);
        }
        
        return $data;
    }
    
    public function get_direction_first_m($username) {
        $direction_data = new DirectionDataAction();
        $data['data'] = $direction_data->get_direction_datas();
        
        $selecting_direction_data = new SelectingDirectionDataAction();
        $data['selected'] = $selecting_direction_data->get_selected_directions($username);
        
        return $data;
    }
    
    public function get_direction_first_r($username) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        if ($redis->exists('direction_first') === FALSE)
        {
            $redis->init_direction_first();
        }
        
        return $redis->get_direction_first($username);
    }
    
    public function select_direction_first($username, $grade, $choices) {
        if (C('REDIS_ON') === TRUE)
        {
            $ret = $this->select_direction_first_r($username, $grade, $choices);
        }
        else 
        {
            $ret = $this->select_direction_first_m($username, $grade, $choices);
        }
        return $ret;
    }
    
    public function select_direction_first_m($username, $grade, $choices) {
        $ret = array(FALSE, '选课失败');
        
        $this->_selecting_direction->startTrans();
        do
        {
            $setting_data = new SettingDataAction();
            if ($setting_data->is_select_first_time() === FALSE)
            {
                $ret[1] = '现在不是第一轮选课时间';
                break;
            }
            
            if ($setting_data->is_select_direction_grade($grade) === FALSE)
            {
                $ret[1] = '你没有权限选方向';
                break;
            }
            
            $selecting_direction_data = new SelectingDirectionDataAction();
            $result = $selecting_direction_data->remove_selecting_direction($username);
            if ($result === FALSE)
            {
                break;
            }
            
            foreach ($choices as $value) 
            {
                $result = $selecting_direction_data->create_selecting_direction($username, $value[0], $value[1]);
                if ($result === FALSE)
                {
                    break;
                }
            }

            if ($result === FALSE)
            {
                break;
            }
            
            $ret[0] = TRUE;
            $ret[1] = '选课成功';
        } while (0);
        
        if ($ret[0] === FALSE)
        {
            $this->_selecting_direction->rollback();
        }
        else
        {
            $this->_selecting_direction->commit();
        }
        
        return $ret;
    }
    
    public function select_direction_first_r($username, $grade, $choices) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        return $redis->select_direction_first($username, $grade, $choices);
    }
    
    public function after_direction_first() {
        if (C('REDIS_ON') === TRUE)
        {
            $this->restore_direction_first_from_redis();
        }
        
        $direction_data = new DirectionDataAction();
        $direction_data->empty_selected_num();
        
        $setting_data = new SettingDataAction();
        $direction_grade = $setting_data->get_direction_grade();
        
        $this->filter_invalid_data($direction_grade);
        
        //获取各课程特殊学生人数上限
        $good_max_num = $this->get_dir_good_max_num();

        //循环n次处理n个志愿
        for ($level = 1; $level <= 3; ++$level)
        {
            $tmp_ret = $this->filtrate_direction_first_by_level($direction_grade, $level, $good_max_num);
            if (false === $tmp_ret[0]) {
                break;
            }
        }

        if (false !== $tmp_ret[0]) {
            $ret[0] = true;
            $ret[1] = '成功';
        } else {
            $ret[0] = false;
            $ret[1] = $tmp_ret[1];
        }

        return $ret;
    }
    
    public function get_dir_good_max_num() {
        $setting_data = new SettingDataAction();
        $special_percent = $setting_data->get_special_good_percent();
        $normal_percent = $setting_data->get_normal_good_percent();
        if (false === $special_percent || false === $normal_percent) {
            return false;
        }

        $direction_data = new DirectionDataAction();
        $dir_list = $direction_data->get_direction_datas();

        $ret_data = array();
        $ret_data[0] = array(); //卓越班&成绩好
        foreach ($dir_list as $line) {
            $ret_data[0][$line['d_id']] = $line['d_max_num'] * $special_percent / 100;
        }

        $ret_data[1] = array(); //普通班&成绩好
        foreach ($dir_list as $line) {
            $ret_data[1][$line['d_id']] = $line['d_max_num'] * $normal_percent / 100;
        }

        return $ret_data;
    }
    
    public function filtrate_direction_first_by_level($grade, $level, &$good_max_num) {
        $ret = false;

        do {
            //实验室学生，只有在第一志愿时能够全中
            if (1 == $level) {
                $ret = $this->_selecting_direction->filtrate_by_lab($grade, $level);
                if (false === $ret[0]) {
                    break;
                }
            }

            //卓越班&成绩好学生
            $ret = $this->_selecting_direction->filtrate_special_good($grade, $level, $good_max_num[0]);
            if (false === $ret[0]) {
                break;
            }

            //普通班&成绩好学生
            $ret = $this->_selecting_direction->filtrate_normal_good($grade, $level, $good_max_num[1]);
            if (false === $ret[0]) {
                break;
            }

            //其他学生
            $ret = $this->_selecting_direction->filtrate_other($grade, $level);
            if (false === $ret[0]) {
                break;
            }

            $ret = $this->_selecting_direction->clean_wish($level);
            if (false === $ret[0]) {
                break;
            }
        }while(0);

        return $ret;
    }
    
    public function restore_direction_first_from_redis() {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        $redis->resotre_direction_first();
    }
    
    public function get_direction_second($username) {
        if (C('REDIS_ON') === TRUE)
        {
            $data = $this->get_direction_second_r($username);
        }
        else 
        {
            $data = $this->get_direction_second_m($username);
        }
        
        return $data;
    }
    
    public function get_direction_second_m($username) {
        $direction_data = new DirectionDataAction();
        $data['data'] = $direction_data->get_direction_datas();
        
        $student_data = new StudentDataAction();
        $data['selected'] = $student_data->get_direction_id_name($username);
        
        return $data;
    }
    
    public function get_direction_second_r($username) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        if ($redis->exists('direction_second') === FALSE)
        {
            $redis->init_direction_second();
        }
        
        return $redis->get_direction_second($username);
    }
    
    public function select_direction_second($username, $grade, $d_id) {
        G('select');
        if (C('REDIS_ON') === TRUE)
        {
            $ret = $this->select_direction_second_r($username, $grade, $d_id);
        }
        else 
        {
            $ret = $this->select_direction_second_m($username, $grade, $d_id);
        }
        trace_user(__CLASS__, __FUNCTION__, G('select', 'end'));
        return $ret;
    }
    
    public function select_direction_second_m($username, $grade, $d_id) {
        $ret = array(FALSE, '选课失败');

        do 
        {
            $setting_data = new SettingDataAction();
            if ($setting_data->is_select_second_time() === FALSE)
            {
                $ret[1] = '现在不是第二轮选课时间';
                break;
            }
            if ($setting_data->is_select_direction_grade($grade) === FALSE)
            {
                $ret[1] = '你没有权限选方向';
                break;
            }
            
            $student_data = new StudentDataAction();
            $stu_dir_id = $student_data->get_direction($username);
            if (FALSE !== $stu_dir_id) {
                $ret[1] = '已有所属方向';
                break;
            }

            $direction_data = new DirectionDataAction();
            $direction_num = $direction_data->get_direction_select_num($d_id);
            if ($direction_num[0] === FALSE)
            {
                $ret[1] = '没有该方向';
                break;
            }
            if ($direction_num[0] >= $direction_num[1])
            {
                $ret[1] = '该方向人数已满';
                break;
            }

            $add_ret = $direction_data->increase_selected_num($d_id);
            if (FALSE === $add_ret) 
            {
                $ret[1] = '该方向人数已满';
                break;
            }

            $update_ret = $student_data->set_direction($username, $d_id);
            if ($update_ret === FALSE) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                $direction_data->decrease_selected_num($d_id);
                $ret[1] = '选课失败';
                break;
            }

            $ret[0] = TRUE;
            $ret[1] = '选课成功';
        } while (0);

        return $ret;
    }
    
    public function select_direction_second_r($username, $grade, $d_id) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        return $redis->select_direction_second($username, $grade, $d_id);
    }
    
    public function cancle_direction_second($username, $grade, $d_id) {
        if (C('REDIS_ON') === TRUE)
        {
            $ret = $this->cancle_direction_second_r($username, $grade, $d_id);
        }
        else
        {
            $ret = $this->cancle_direction_second_m($username, $grade, $d_id);
        }
        
        return $ret;
    }
    
    public function cancle_direction_second_m($username, $grade, $d_id) {
        $ret = array(false, '退选失败');
        
        do 
        {
            $setting_data = new SettingDataAction();
            if ($setting_data->is_select_second_time() === FALSE)
            {
                $ret[1] = '现在不是第二轮选课时间';
                break;
            }
            if ($setting_data->is_select_direction_grade($grade) === FALSE)
            {
                $ret[1] = '你没有权限选方向';
                break;
            }

            $student_data = new StudentDataAction();
            $student_d_id = $student_data->get_direction($username);
            if ($student_d_id != $d_id) 
            {
                $ret[1] = '没有所属方向';
                break;
            }

            $delete_ret = $student_data->del_direction($username);
            if (FALSE === $delete_ret) {
                $ret[1] = '学生数据更新失败';
                break;
            }

            $direction_data = new DirectionDataAction();
            $decrease_ret = $direction_data->decrease_selected_num($d_id);
            if (FALSE === $decrease_ret) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                $student_data->set_direction($username, $d_id);
                $ret[1] = '学生数据更新失败';
                break;
            }

            $ret[0] = true;
            $ret[1] = '退选成功';

        } while (0);

        return $ret;
    }
    
    public function cancle_direction_second_r($username, $grade, $d_id) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        return $redis->cancle_direction_second($username, $grade, $d_id);
    }
    
    public function after_direction_second() {
        
    }
    
    public function restore_direction_second_from_redis() {
        
    }
    
    public function get_course_third($username) {
        if (C('REDIS_ON') === TRUE)
        {
            $data = $this->get_course_third_r($username);
        }
        else
        {
            $data = $this->get_course_third_m($username);
        }
        
        return $data;
    }
    
    public function get_course_third_m($username) {
        $course_data = new CourseDataAction();
        $data['data']  = $course_data->get_selectable_course_data();

        $data['selected'] = $course_data->get_selected_course($username);
        
        return $data;
    }
    
    public function get_course_third_r($username) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        if ($redis->exists('course_third') === FALSE)
        {
            $redis->init_course_third();
        }
        
        return $redis->get_course_third($username);
    }
    
    public function select_course_third($username, $grade, $co_id) {
        if (C('REDIS_ON') === TRUE)
        {
            $ret = $this->select_course_third_r($username, $grade, $co_id);
        }
        else 
        {
            $ret = $this->select_course_third_m($username, $grade, $co_id);
        }
        
        return $ret;
    }
    
    public function select_course_third_m($username, $grade, $co_id) {
        $ret = array(FALSE, '选择失败，原因：');

        do 
        {
            $setting_data = new SettingDataAction();
            if ($setting_data->is_select_third_time() === FALSE)
            {
                $ret[1] = '现在不是第三轮选课时间';
                break;
            }
            if ($setting_data->is_select_course_grade($grade) === FALSE)
            {
                $ret[1] = '你没有权限选课';
                break;
            }
            
            $course_data = new CourseDataAction();
            
            $co_num = $course_data->get_course_num($co_id);
            $result = $course_data->check_leading_course($username, $co_num);
            if ($result === FALSE)
            {
                $ret[1] = '前导课未选';
                break;
            }
            
            $result = $course_data->check_course_time_conflict($username, $co_id);
            if ($result === TRUE)
            {
                $ret[1] = '选课时间冲突';
                break;
            }
            
            $co_select_nums = $course_data->get_course_select_num($co_id);
            if ($co_select_nums[0] === FALSE)
            {
                $ret[1] = '课程不存在';
                break;
            }
            if($co_select_nums[0] >= $co_select_nums[1])
            {
                $ret[1] = '该课程人数已满';
                break;
            }

            $result = $course_data->increase_selected_num($co_id);
            if ($result === FALSE) 
            {
                $ret[1] = '该课程人数已满';
                break;
            }

            $result = $course_data->create_course_student_data($co_id, $username, $co_num);
            if ($result === FALSE)
            {
                $ret[1] = '选课失败';
                $course_data->decrease_selected_num($co_id);
                break;
            }

            $ret[0] = TRUE;
            $ret[1] = '成功选中该课程。';

        } while (0);
        
        return $ret;
    }
    
    public function select_course_third_r($username, $grade, $co_id) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        return $redis->select_course_third($username, $grade, $co_id);
    }
    
    public function cancle_course_third($username, $grade, $co_id) {
        if (C('REDIS_ON') === TRUE)
        {
            $ret = $this->cancle_course_third_r($username, $grade, $co_id);
        }
        else
        {
            $ret = $this->cancle_course_third_m($username, $grade, $co_id);
        }
        
        return $ret;
    }
    
    public function cancle_course_third_m($username, $grade, $co_id) {
        $ret = array(false, '');
        
        do 
        {
            $setting_data = new SettingDataAction();
            if ($setting_data->is_select_third_time() === FALSE)
            {
                $ret[1] = '现在不是第三轮选课时间';
                break;
            }
            if ($setting_data->is_select_course_grade($grade) === FALSE)
            {
                $ret[1] = '你没有权限退课';
                break;
            }
            
            $course_data = new CourseDataAction();
            $result = $course_data->delete_coutse_student_data($co_id, $username);
            if(false === $result)
            {
                $ret[1] = "退课失败.";
                break;
            }

            $result = $course_data->decrease_selected_num($co_id);
            if ($result === FALSE)
            {
                $ret[1] = "退课失败.";
                break;
            }

            $ret[0] = TRUE;
            $ret[1] = '退课成功';
        } while (0);
        
        return $ret;
    }
    
    public function cancle_course_third_r($username, $grade, $co_id) {
        import('@.Redis.SelectRedis');
        $redis = new SelectRedis();
        return $redis->cancle_course_third($username, $grade, $co_id);
    }
    
    public function after_course_third() {
        
    }
    
    public function restore_course_third_from_redis() {
        
    }
}
