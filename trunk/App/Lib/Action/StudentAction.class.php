<?php

class StudentAction extends BaseAction
{
    private $_student_data = null;
    
    function _initialize()
    {
        parent::_initialize();
        
        $this->_student_data = new StudentDataAction();
        
        $acc = new AccountDataAction();
        $name = $acc->get_my_name();
        array_urlencode($name);
        $this->assign('name', urldecode(json_encode($name)));
    }

    public function index()
    {
        $this->redirect('./Student/student_course_manage');
    }

    public function student_course_manage() {
        $semester       = safe_get_b('semester');   
        $page           = safe_get_b('page');       
        $page_size      = safe_get_b('page_size');  

        $data = $this->_student_data->student_get_course_data($semester, $page, $page_size);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/student_course_manage');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }
    
    public function sutdent_course_schedule_manage() {
        $semester = safe_get_b('semester'); 

        $data = $this->_student_data->student_get_course_schedule($semester);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/sutdent_course_schedule_manage');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }

    public function student_direction_first_manage(){
        $data = $this->_student_data->student_get_direction_first_data();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/student_direction_first_manage');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }
    
    public function student_direction_second_manage() {
        G('get');
        $data = $this->_student_data->student_get_direction_second_data();
        echo G('get', 'end');
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/student_direction_second_manage');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }
    
    public function student_course_third_manage() {        
        $data = $this->_student_data->student_get_course_third_data();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/student_course_third_manage');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }

    public function student_credit_manage() {
        $data = $this->_student_data->student_get_credit_data();

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/student_credit_manage');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }


    public function student_information() {
        $data = $this->_student_data->student_get_student_info();
        
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Student/header');
        $content .= $this->fetch('./Student/student_information');
        $content .= $this->fetch('./Student/footer');
        
        $this->show($content);
    }

    public function student_single_course_data() {
        $id = safe_get_b('id');
        
        $data = $this->_student_data->student_get_single_course_data($id);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Student/student_single_course_data');
        
        $this->show($content);
    }


    public function student_single_teacher_info() {
        $t_id = safe_get_b('id');

        $data = $this->_student_data->student_get_single_teacher_info($t_id);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));

        $content = $this->fetch('./Student/student_single_teacher_info');
        $this->show($content);
    }




}
