<?php

class AccountDataAction extends BaseAction
{

    public function get_my_username() {
        return $_SESSION[C('SESSION_USERNAME')];
    }

    public function get_my_name() {
        if (!isset($_SESSION[C('SESSION_NAME')])) 
        {
            $_SESSION[C('SESSION_NAME')] = $this->get_name($this->get_my_username());
        }

        return $_SESSION[C('SESSION_NAME')];
    }


    public function get_name($username) {
        $ret = false;
        do 
        {
            if (empty($username)) 
            {
                break;
            }

            $rbac = new RbacModel();
            $user_id = $rbac->get_user_id($username);
            if (false === $user_id) 
            {
                break;
            }

            $role = $rbac->get_role($user_id);
            if (false === $role) 
            {
                break;
            }

            if (C('ROLE_ADMIN') == $role) 
            {
                $ret = '教务员';
            } 
            else if (C('ROLE_TEACHER') == $role) 
            {
                $teacher = new TeacherModel();
                $r = $teacher->t_get_name($username);
                if (false === $r) 
                {
                    break;
                }
                $ret = $r;
            } 
            else if (C('ROLE_STUDENT') == $role) 
            {
                $student = new StudentModel();
                $r = $student->s_get_name($username);
                if (false === $r) 
                {
                    break;
                }
                $ret = $r;
            }
        } while (0);

        return $ret;
    }


    public function get_name_a() {
        $ret_data = '';
        $ret_data = '查询失败';
        $ret_status = 0;
        
        $username = safe_get_b('username'); //帐号

        $ret_data = $this->get_name($username);
        if (false === $ret_data) 
        {
            $ret_info = '查询失败';
            $ret_status = 0;
        } 
        else 
        {
            $ret_info = '查询成功';
            $ret_status = 1;
        }

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }


    public function reset_pwd($username) {
        $rbac = new RbacModel();
        return $rbac->set_password($username, C('DEFAULT_PWD'));
    }


    public function reset_pwd_a() {
        $username = safe_post_b('username'); //帐号

        $ret_data = '';
        $ret_info = '重置失败';
        $ret_status = 0;

        if ($this->reset_pwd($username)) 
        {
            $ret_info = '重置成功';
            $ret_status = 1;
        }

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }


    public function change_pwd($username, $old_pwd, $new_pwd) {
        $map = array();
        $map['username'] = $username;
        import('ORG.Util.RBAC');
        $authInfo = RBAC::authenticate($map);

        if (false === $authInfo) 
        {
            return false;
        }

        if ($authInfo['password'] != RbacModel::encode_password($old_pwd)) 
        {
            return false;
        }

        $rbac = new RbacModel();
        return $rbac->set_password($username, $new_pwd);
    }


    public function change_pwd_a() {
        $ret_data = '';
        $ret_info = '修改密码失败';
        $ret_status = 0;
        
        $old_pwd = safe_post_b('old_pwd'); 
        $new_pwd = safe_post_b('new_pwd'); 

        $username = $this->get_my_username();

        if (false !== $old_pwd &&
            false !== $new_pwd &&
            $this->change_pwd($username, $old_pwd, $new_pwd)) 
        {
            $ret_info = '修改密码成功';
            $ret_status = 1;
        }

        $this->ajaxReturn($ret_data, $ret_info, $ret_status);
    }
}
