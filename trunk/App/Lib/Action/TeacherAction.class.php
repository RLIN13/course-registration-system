<?php

class TeacherAction extends BaseAction
{
    private $_teacher_data = null;
    
    function _initialize()
    {
        parent::_initialize();

        $this->_teacher_data = new TeacherDataAction();
        
        $account_data = new AccountDataAction();
        $name = $account_data->get_my_name();
        array_urlencode($name);
        $this->assign('name', urldecode(json_encode($name)));
    }

    public function index()
    {
        $this->redirect('./Teacher/teacher_course_manage');
    }

    public function teacher_course_manage() {
        $data = $this->_teacher_data->teacher_get_teacher_course();
        
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Teacher/header');
        $content .= $this->fetch('./Teacher/teacher_course_manage');
        $content .= $this->fetch('./Teacher/footer');
        
        $this->show($content);
    }

    public function teacher_information() {
        $data = $this->_teacher_data->teacher_get_teacher_info();
        
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Teacher/header');
        $content .= $this->fetch('./Teacher/teacher_information');
        $content .= $this->fetch('./Teacher/footer');
        
        $this->show($content);
    }
 
    public function teacher_course_data() {
        $co_id = safe_get_b('co_id');
        
        $data = $this->_teacher_data->teacher_get_course_data($co_id);
        
        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Teacher/teacher_course_data');
        
        $this->show($content);
    }
    
    public function teacher_course_student() {
        $co_id = safe_get_b('co_id');        
        $page = safe_get_b('page');
        $page_size = safe_get_b('page_size');
        
        $data = $this->_teacher_data->teacher_get_course_student($co_id, $page, $page_size);

        array_urlencode($data);
        $this->assign('data', urldecode(json_encode($data)));
        
        $content = $this->fetch('./Teacher/teacher_course_student');
        
        $this->show($content);
    }
}
