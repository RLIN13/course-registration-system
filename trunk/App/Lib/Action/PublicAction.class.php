<?php

class PublicAction extends Action
{
    public function login()
    {
        if (!isset($_SESSION[C('USER_AUTH_KEY')])) {
            $this->display();
        } else {
            $this->redirect('Index/index');
        }
    }

    public function checkLogin()
    {
        if (empty($_POST['username'])) {
            $this->error('帐号错误！');
        } elseif (empty($_POST['password'])) {
            $this->error('密码必须！');
        }

        $map = array();

        $map['username'] = $_POST['username'];
        import('ORG.Util.RBAC');
        $authInfo = RBAC::authenticate($map);

        if (false === $authInfo) {
            $this->error('帐号不存在或已禁用！');
        } else {
            if ($authInfo['password'] != RbacModel::encode_password($_POST['password'])) {
                $this->error('密码错误！');
            }
            $_SESSION[C('USER_AUTH_KEY')] = $authInfo['id'];
            $_SESSION[C('SESSION_USERNAME')] = $authInfo['username'];

            RBAC::saveAccessList();
            $this->redirect('Index/index');
        }
    }

    function loginout()
    {
        if (isset($_SESSION[C('USER_AUTH_KEY')])) {
            unset($_SESSION[C('USER_AUTH_KEY')]);
            unset($_SESSION);
            session_destroy();
        }

        $this->redirect('Index/index');
    }
}

