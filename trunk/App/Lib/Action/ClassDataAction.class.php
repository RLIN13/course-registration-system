<?php

class ClassDataAction extends BaseAction
{
    protected $_class = null;
            
    function _initialize() {
        parent::_initialize();
        $this->_class = new ClassModel();
    }
    
    public function get_grade_class_map() {
        return $this->_class->get_grade_class_map();
    }

    public function get($id = null, $grade = null, $class = null, $special = null)
    {
        $class = new ClassModel();
        $ret = $class->c_search($id, $grade, $class, $special);

        return $ret;
    }

    public function get_grade_list(){
        return $this->_class->get_grade_list();
    }
}