<?php

class DirectionDataAction extends BaseAction
{
    protected $_direction = null;

    function _initialize() {
        parent::_initialize();
        $this->_direction = new DirectionModel();
    }

    public function get_id_names() {
        return $this->_direction->get_id_names();
    }
    
    public function get_direction_datas() {
        return $this->_direction->d_get_all();
    }
    
    public function get_direction_name($d_id) {
        return $this->_direction->get_name($d_id);
    }

    public function get_direction_select_num($d_id) {
        $ret[] = $this->_direction->d_get_selected_num($d_id);
        $ret[] = $this->_direction->d_get_max_num($d_id);
        
        return $ret;
    }
    
    public function increase_selected_num($d_id) {
        return $this->_direction->d_increase_selected_num($d_id);
    }
    
    public function decrease_selected_num($d_id) {
        return $this->_direction->d_decrease_selected_num($d_id);
    }
    
    public function empty_selected_num() {
        return $this->_direction->empty_selected_num();
    }
    
    public function get_remain_selected_num($d_id) {
        $selected_num = $this->_direction->d_get_selected_num($d_id);
        $max_num = $this->_direction->d_get_max_num($d_id);
        if ($max_num >= $selected_num)
        {
            return $max_num - $selected_num;
        }
        else
        {
            return 0;
        }
    }

    public function create_direction_data($name, $num, $introduction)
    {
        if(empty($name))
        {
            trace_user(__CLASS__, __FUNCTION__, __LINE__);
            return FALSE;
        }

        $ret = $this->_direction->d_create($name, $num, $introduction);
        return $ret;
    }
    
    public function remove_direction_data($ids) {
        $ret = $this->_direction->batch_remove($ids);
        return $ret;
    }

    public static function get_name_in_array(&$arr, $id_key, $name_key, $unset = false)
    {
        if (count($id_key) == 1) {
            $key = reset($id_key);
            if (isset($arr[$key])) {
                if (0 == $arr[$key]) {
                    $arr[$name_key] = '无';
                } else {
                    $dir = new DirectionModel();
                    $ret = $dir->d_find($arr[$key]);
                    if (false !== $ret) {
                        $arr[$name_key] = $ret[0]['d_name'];
                    }
                }
                if ($unset) {
                    unset($arr[$key]);
                }
            }
            return;
        }

        if (reset($id_key) === '*') {
            array_shift($id_key);
            foreach ($arr as &$arr_next) {
                if (is_array($arr_next)) {
                    self::get_name_in_array($arr_next, $id_key, $name_key, $unset);
                }
            }
        } else {
            $key = reset($id_key);
            if (isset($arr[$key])) {
                array_shift($id_key);
                self::get_name_in_array($arr[$key], $id_key, $name_key, $unset);
            }
        }
    }
}