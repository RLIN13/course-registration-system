<?php

class SelectingDirectionModel extends Model{
    public function sd_remove($sd_student_id) {
        $ret = FALSE;
        
        do
        {
            $cond = array('sd_student_id' => $sd_student_id);
            
            $result = $this->where($cond)->delete();
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        return $ret;
    }
    
    public function sd_create($sd_student_id, $sd_direction_id, $sd_level) {
        $ret = FALSE;
        
        do
        {
            $data = array();
            $data['sd_student_id'] = $sd_student_id;
            $data['sd_direction_id'] = $sd_direction_id;
            $data['sd_level'] = $sd_level;

            $result = $this->add($data);

            if ($result === FALSE)
            {
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        return $ret;
    }

    public function sd_get_all() {
        $ret = $this->select();
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }

    public function sd_get($student_id) {
        $condition = array();
        if (!empty($student_id)) {
            $condition['sd_student_id'] = $student_id;
        }

        $ret = $this->where($condition)->select();
        
        if (empty($ret))
        {
            $ret = FALSE;
        }
        
        return $ret;
    }
    
    public function filter_invalid_student() {
        $this->execute(<<<SQL
            DELETE sd
            FROM selecting_direction as sd
            WHERE not exists (select 1 from student where sd.sd_student_id = student.s_id)
SQL
        );
    }

    public function filter_invalid_direction() {
        $this->execute(<<<SQL
            DELETE sd
            FROM selecting_direction as sd
            WHERE not exists (select 1 from direction where sd.sd_direction_id = direction.d_id)
SQL
        );
    }

    public function filter_invalid_grade($direction_grade) {
        $this->execute(<<<SQL
            DELETE sd
            FROM selecting_direction as sd, student as stu
            WHERE sd.sd_student_id = stu.s_id and stu.s_grade <> $direction_grade
SQL
        );
    }

    public function filtrate_by_lab($grade, $level) {
        $ret = FALSE;
        $this->startTrans();

        do{
            $sql_wish_table = <<<SQL
                    SELECT sd_student_id, sd_direction_id
                    FROM selecting_direction
                    WHERE sd_level = $level
SQL;
            $sql_lib_student_table = <<<SQL
                    SELECT s_id FROM student WHERE s_lib = 1 and s_grade = $grade
SQL;

            $sql_dir_stu_table = <<<SQL
                    SELECT wish.sd_student_id, wish.sd_direction_id
                    FROM ($sql_wish_table) as wish, ($sql_lib_student_table) as stu, lab_student_direction as lsd 
                    WHERE wish.sd_student_id = stu.s_id and 
                        wish.sd_student_id = lsd.lsd_student_id and 
                        (wish.sd_direction_id = lsd.lsd_direction_id or lsd.lsd_direction_id = -1)
SQL;

            $sql_dir_count_table = <<<SQL
                    select sd.sd_direction_id, count(distinct(sd.sd_student_id)) as count
                    FROM ($sql_dir_stu_table) as sd
                    group by sd.sd_direction_id
SQL;
            $ret_data = $this->query(<<<SQL
                    select dir.d_name
                    from direction as dir,($sql_dir_count_table) as dir_count
                    where dir.d_id = dir_count.sd_direction_id and dir.d_selected_num + dir_count.count > d_max_num
SQL
            );

            if (false === $ret_data) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__ . '\tdatabase error');
                break;
            } elseif (count($ret_data) > 0) {
                //$ret[1] .= $this->direction_array_to_string($ret_data) . '人数超过上限';
                trace_user(__CLASS__, __FUNCTION__, __LINE__ . 'num of people overflow');
                break;
            }
            $ret_data = $this->execute(<<<SQL
                    update direction as dir, ($sql_dir_count_table) as dir_count
                    set dir.d_selected_num = dir.d_selected_num + dir_count.count
                    where dir.d_id = dir_count.sd_direction_id;
SQL
            );

            if (false === $ret_data) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__ . '\tdatabase error');
                break;
            }

            $ret_data = $this->execute(<<<SQL
                    update student as stu, ($sql_dir_stu_table) as dir_stu
                    set stu.s_direction = dir_stu.sd_direction_id
                    where stu.s_id = dir_stu.sd_student_id;
SQL
            );

            if (false === $ret_data) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__ . '\tdatabase error');
                break;
            }

            $ret_data = $this->execute(<<<SQL
                    delete sd
                    from selecting_direction as sd, ($sql_dir_stu_table) as dir_stu
                    where sd.sd_student_id = dir_stu.sd_student_id;
SQL
            );

            if (false === $ret_data) {
                trace_user(__CLASS__, __FUNCTION__, __LINE__ . '\tdatabase error');
                break;
            }

            $ret = true;
        }while(0);

        if ($ret) {
            $this->commit();
        } else {
            $this->rollback();
        }

        return $ret;
    }

    public function filtrate_special_good($grade, $level, &$max_num){
        $ret = array(false, "第 $level 志愿处理卓越班且成绩好的学生失败，原因：");
        $this->startTrans();

        do{
            $sql_wish_table = <<<SQL
                    SELECT sd_student_id, sd_direction_id
                    FROM selecting_direction
                    WHERE sd_level = $level
SQL;
            $sql_student_table = <<<SQL
                    SELECT s_id
                    FROM student
                    WHERE s_grade = $grade and
                          s_good = 1 and
                          exists (select 1 from class where s_grade = c_grade and s_class = c_num and c_special = 1)
SQL;

            //任何方向优先
            $sql_dir_stu_table = <<<SQL
                    SELECT wish.sd_student_id, wish.sd_direction_id
                    FROM ($sql_wish_table) as wish, ($sql_student_table) as stu
                    WHERE wish.sd_student_id = stu.s_id
SQL;

            $sql_dir_count_table = <<<SQL
                    select sd.sd_direction_id, count(distinct(sd.sd_student_id)) as count
                    FROM ($sql_dir_stu_table) as sd
                    group by sd.sd_direction_id
SQL;

            $ret_dir_count = $this->query($sql_dir_count_table);

            if (false === $ret_dir_count) {
                $ret[1] .= '数据库错误';
                break;
            }

            $dir_model = new DirectionModel();
            $dir_empty_num = $dir_model->get_empty_num();

            //允许命中的最大人数
            $dir_limit_num = $this->_get_limit_num($ret_dir_count, $max_num, $dir_empty_num);

            foreach ($dir_limit_num as $dir => $limit) {
                $tmp_ret = $this->_make_lucky_gay($dir, $sql_dir_stu_table, $limit);
                if (false === $tmp_ret) {
                    $ret[1] .= "方向 $dir 筛选失败";
                    break;
                }
                $max_num[$dir] -= $limit;
            }

            //写入最终表格，删除临时数据
            $tmp_ret = $this->_save_lucky_guy();
            if (false === $tmp_ret) {
                $ret[1] .= "失败";
                break;
            }

            $ret[0] = true;
        }while(0);

        if ($ret[0]) {
            $this->commit();
            $ret[1] = '成功';
        } else {
            $this->rollback();
        }

        return $ret;
    }

    public function filtrate_normal_good($grade, $level, &$max_num) {
        $ret = array(false, "第 $level 志愿处理普通班且成绩好的学生失败，原因：");
        $this->startTrans();

        do{
            $sql_wish_table = <<<SQL
                    SELECT sd_student_id, sd_direction_id
                    FROM selecting_direction
                    WHERE sd_level = $level
SQL;
            $sql_student_table = <<<SQL
                    SELECT s_id
                    FROM student
                    WHERE s_grade = $grade and
                          s_good = 1 and
                          exists (select 1 from class where s_grade = c_grade and s_class = c_num and c_special = 0)
SQL;

            //按优先方向进行
            $sql_dir_stu_table = <<<SQL
                    SELECT wish.sd_student_id, wish.sd_direction_id
                    FROM ($sql_wish_table) as wish, ($sql_student_table) as stu, good_student_direction as gsd
                    WHERE wish.sd_student_id = stu.s_id and
                          gsd.gsd_student_id = stu.s_id and
                          gsd.gsd_direction_id in (wish.sd_direction_id, -1)
SQL;

            $sql_dir_count_table = <<<SQL
                    select sd.sd_direction_id, count(distinct(sd.sd_student_id)) as count
                    FROM ($sql_dir_stu_table) as sd
                    group by sd.sd_direction_id
SQL;

            $ret_dir_count = $this->query($sql_dir_count_table);

            if (false === $ret_dir_count) {
                $ret[1] .= '数据库错误';
                break;
            }

            $dir_model = new DirectionModel();
            $dir_empty_num = $dir_model->get_empty_num();

            //允许命中的最大人数
            $dir_limit_num = $this->_get_limit_num($ret_dir_count, $max_num, $dir_empty_num);

            foreach ($dir_limit_num as $dir => $limit) {
                $tmp_ret = $this->_make_lucky_gay($dir, $sql_dir_stu_table, $limit);
                if (false === $tmp_ret) {
                    $ret[1] .= "方向 $dir 筛选失败";
                    break;
                }
                $max_num[$dir] -= $limit;
            }

            //写入最终表格，删除临时数据
            $tmp_ret = $this->_save_lucky_guy();
            if (false === $tmp_ret) {
                $ret[1] .= "失败";
                break;
            }

            $ret[0] = true;
        }while(0);

        if ($ret[0]) {
            $this->commit();
            $ret[1] = '成功';
        } else {
            $this->rollback();
        }

        return $ret;
    }

    public function filtrate_other($grade, $level) {
        $ret = array(false, "第 $level 志愿处理其他学生失败，原因：");
        $this->startTrans();

        do{
            $sql_wish_table = <<<SQL
                    SELECT sd_student_id, sd_direction_id
                    FROM selecting_direction
                    WHERE sd_level = $level
SQL;
            $sql_student_table = <<<SQL
                    SELECT s_id
                    FROM student
                    WHERE s_grade = $grade
SQL;

            //按优先方向进行
            $sql_dir_stu_table = <<<SQL
                    SELECT wish.sd_student_id, wish.sd_direction_id
                    FROM ($sql_wish_table) as wish, ($sql_student_table) as stu
                    WHERE wish.sd_student_id = stu.s_id
SQL;

            $sql_dir_count_table = <<<SQL
                    select sd.sd_direction_id, count(distinct(sd.sd_student_id)) as count
                    FROM ($sql_dir_stu_table) as sd
                    group by sd.sd_direction_id
SQL;

            $ret_dir_count = $this->query($sql_dir_count_table);

            if (false === $ret_dir_count) {
                $ret[1] .= '数据库错误';
                break;
            }

            $dir_model = new DirectionModel();
            $dir_empty_num = $dir_model->get_empty_num();

            //允许命中的最大人数
            $dir_limit_num = $this->_get_limit_num($ret_dir_count, false, $dir_empty_num);

            foreach ($dir_limit_num as $dir => $limit) {
                $tmp_ret = $this->_make_lucky_gay($dir, $sql_dir_stu_table, $limit);
                if (false === $tmp_ret) {
                    $ret[1] .= "方向 $dir 筛选失败";
                    break;
                }
            }

            //写入最终表格，删除临时数据
            $tmp_ret = $this->_save_lucky_guy();
            if (false === $tmp_ret) {
                $ret[1] .= "失败";
                break;
            }

            $ret[0] = true;
        }while(0);

        if ($ret[0]) {
            $this->commit();
            $ret[1] = '成功';
        } else {
            $this->rollback();
        }

        return $ret;
    }

    protected function _get_limit_num($count_num, $sp_max_num, $empty_num) {
        $dir_allow_num = array();
        foreach ($count_num as $dir_count) {
            $dir = $dir_count['sd_direction_id'];
            $dir_allow_num[$dir] = $dir_count['count'];

            if ($sp_max_num) {
                if (isset($sp_max_num[$dir])) {
                    $dir_allow_num[$dir] = min($dir_allow_num[$dir], $sp_max_num[$dir]);
                } else {
                    $dir_allow_num[$dir] = 0;
                }
            }

            if ($empty_num) {
                if (isset($empty_num[$dir])) {
                    $dir_allow_num[$dir] =min($dir_allow_num[$dir], $empty_num[$dir]);
                } else {
                    $dir_allow_num[$dir] = 0;
                }
            }

            if ($dir_allow_num[$dir] < 1) {
                unset($dir_allow_num[$dir]);
            }
        }

        return $dir_allow_num;
    }

    public function _make_lucky_gay($dir_id, $sql_dir_stu, $limit){
        $ret = false;

        $ret = $this->execute(<<<SQL
                REPLACE INTO `tmp_selecting_direction` (`tsd_student_id`, `tsd_direction_id`, `tsd_lucky_num`)
                SELECT luck_sd.sd_student_id, luck_sd.sd_direction_id, rand() as lucky_num
                FROM ($sql_dir_stu) as luck_sd
                WHERE luck_sd.sd_direction_id = $dir_id
                ORDER BY lucky_num
                LIMIT $limit
SQL
        );

        return $ret;
    }

    public function _save_lucky_guy() {
        $ret = false;

        do{
            $sql_dir_count = <<<SQL
                    SELECT tsd_direction_id, count(tsd_student_id) as tsd_count
                    FROM tmp_selecting_direction
                    group by tsd_direction_id
SQL;

            //更新direction表
            $sql_update_dir = <<<SQL
                    UPDATE direction, ($sql_dir_count) as dir_count
                    SET direction.d_selected_num = dir_count.tsd_count + direction.d_selected_num
                    WHERE direction.d_id = dir_count.tsd_direction_id
SQL;
            $ret = $this->execute($sql_update_dir);
            if (false === $ret) {
                break;
            }

            //写入学生表
            $ret = $this->execute(<<<SQL
                    UPDATE student, tmp_selecting_direction as tsd
                    SET student.s_direction = tsd.tsd_direction_id
                    WHERE student.s_id = tsd.tsd_student_id
SQL
            );
            if (false === $ret) {
                break;
            }

            //删除selecting_direction表中对应学生的所有志愿
            $ret = $this->execute(<<<SQL
                    DELETE sd
                    FROM selecting_direction as sd, tmp_selecting_direction as tsd
                    WHERE sd.sd_student_id = tsd.tsd_student_id
SQL
);
            if (false === $ret) {
                break;
            }

            //清空tmp_selecting_direction
            $ret = $this->execute(<<<SQL
                    truncate table tmp_selecting_direction
SQL
            );
            if (false === $ret) {
                break;
            }

            $ret = true;
        }while(0);

        return $ret;
    }

    public function clean_wish($level) {
        $ret = $this->where(array('sd_level' => $level))->delete();

        return $ret;
    }

    public function direction_array_to_string($dir_array) {
//        $str = implode(',', $dir_array);
        foreach ($dir_array as $dir) {
            $str .= $dir['d_name'] .',';
        }

        if (strlen($str) > 0 && substr($str,-1) === ',') {
            $str = substr($str, 0, strlen($str) - 1);
        }

        return $str;
    }
}
