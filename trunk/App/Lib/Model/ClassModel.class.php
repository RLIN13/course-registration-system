<?php

class ClassModel extends Model
{

    public function c_search($id = null, $grade = null, $num = null, $special = null) {
        $condition = array();
        if (!empty($id)) {
            $condition['c_id'] = $id;
        }
        if (!empty($grade)) {
            $condition['c_grade'] = $grade;
        }
        if (!empty($num)) {
            $condition['c_num'] = $num;
        }
        if (!empty($special)) {
            $condition['c_special'] = $special;
        }

        $ret = $this->where($condition)->select();

        return $ret;
    }

    public function read_all($short_field = false){
        if ($short_field) {
            $this->field('c_grade, c_name, c_num');
        }

        $result = $this->select();

        return $result;
    }

    public function get_grade_class_map() {
        $ret = false;
        do {
            $data = $this->read_all(true);
            if (false === $data) {
                break;
            }

            $ret = array();
            foreach ($data as $line) {
                if (!isset($ret[$line['c_grade']])) {
                    $ret[$line['c_grade']] = array();
                }
                $ret[$line['c_grade']][] = array($line['c_num'], $line['c_name']);
            }
        } while (0);

        $ret = empty($ret) ? false : $ret;

        return $ret;
    }

    public function c_create($grade, $num, $name, $special){

        $data = array();
        $data['c_grade'] = $grade;
        $data['c_num'] = $num;

        $ret = $this->where($data)->count();
        if (false === $ret || $ret['count'] != 0) {
            return false;
        }

        $data['c_name'] = $name;
        $data['c_special'] = $special;
        
        $ret = $this->add($data);

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }

    public function c_update($id, $grade = null, $num = null, $name = null, $special = null){

        $data = array();
        $data['c_id'] = $id;

        if (!empty($grade)) {
            $data['c_grade'] = $grade;
        }
        if (!empty($num)) {
            $data['c_num'] = $num;
        }
        if (!empty($name)) {
            $data['c_name'] = $name;
        }
        if (null !== $special) {
            $data['c_special'] = $special ? 1 : 0;
        }

        $ret = $this->save($data);

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }

    public function get_grade_list() {
        return $this->Distinct(true)->getField('c_grade', true);
    }
}
