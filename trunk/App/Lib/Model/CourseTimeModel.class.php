<?php
class CourseTimeModel extends Model
{

    public static function time_array_to_db($course_id, $week, $time_list)
    {
        $ret = array();

        foreach ($time_list as $day => $time) {
            $ret[] = array(
                'ct_course_id' => $course_id,
                'ct_week' => $week[0],
                'ct_day' => $day,
                'ct_time' => $time,
            );
        }

        return $ret;
    }

    public static function db_to_time_array($time_list)
    {
        $ret = array();

        foreach ($time_list as $time) {
            $ret[$time['ct_day']] = (int)$time['ct_time'];
        }

        return $ret;
    }

    public static function string_to_time_array($time)
    {
        $ret = false;
        $time = str_replace('，', ',', $time);
        do {
            $time_pattern = '/周(.+?)第([\d,]+?)节/';
            $matches = array();
            if (!preg_match_all($time_pattern, $time, $matches)) {
                break;
            }

            $ret = array();

            foreach ($matches[1] as $key => $day) {
                switch ($day) {
                    case '一':
                        $day = 1;
                        break;
                    case '二':
                        $day = 2;
                        break;
                    case '三':
                        $day = 3;
                        break;
                    case '四':
                        $day = 4;
                        break;
                    case '五':
                        $day = 5;
                        break;
                    case '六':
                        $day = 6;
                        break;
                    default:
                        $day = 7;
                }
                $ret[$day] = 0;

                $class = $matches[2][$key];
                $class = explode(',', $class);
                foreach ($class as $num) {
                    if (!is_numeric($num)) {
                        $ret = false;
                        break;
                    }
                    $ret[$day] |= (1 << ((int)$num - 1));
                }

                if (false === $ret) {
                    break;
                }
            }

        } while (0);

        return $ret;
    }

    public static function time_array_to_string($time_list)
    {
        $ret = '';
        foreach ($time_list as $day => $time) {
            $ret .= '周';
            switch ($day) {
                case 1:
                    $ret .= '一';
                    break;
                case 2:
                    $ret .= '二';
                    break;
                case 3:
                    $ret .= '三';
                    break;
                case 4:
                    $ret .= '四';
                    break;
                case 5:
                    $ret .= '五';
                    break;
                case 6:
                    $ret .= '六';
                    break;
                default:
                    $ret .= '日';
                    break;
            }

            $ret .= '第';
            $count = 1;

            while ($time != 0) {
                if ($time % 2 == 1) {
                    $ret .= $count . ',';
                }

                ++$count;
                $time = $time >> 1;
            }

            if (substr($ret, -1, 1) == ',') {
                $ret = substr($ret, 0, strlen($ret) - 1);
            }

            $ret .= '节,';
        }

        if (substr($ret, -1, 1) == ',') {
            $ret = substr($ret, 0, strlen($ret) - 1);
        }

        return $ret;
    }

    public static function db_to_string($db_data)
    {
        return CourseTimeModel::time_array_to_string(
            CourseTimeModel::db_to_time_array($db_data));
    }

    public static function db_to_string_in_array(&$arr, $time_key, $str_key, $unset = false)
    {
        if (count($time_key) == 1) {
            $key = reset($time_key);
            if (isset($arr[$key])) {
                $arr[$str_key] = self::db_to_string($arr[$key]);

                if ($unset) {
                    unset($arr[$key]);
                }
            }
            return;
        }

        if (reset($time_key) === '*') {
            array_shift($time_key);
            foreach ($arr as &$arr_next) {
                if (is_array($arr_next)) {
                    self::db_to_string_in_array($arr_next, $time_key, $str_key, $unset);
                }
            }
        } else {
            $key = reset($time_key);
            if (isset($arr[$key])) {
                array_shift($time_key);
                self::db_to_string_in_array($arr[$key], $time_key, $str_key, $unset);
            }
        }
    }

    public static function db_to_time_array_in_array(&$arr, $time_key, $str_key, $unset = false)
    {
        if (count($time_key) == 1) {
            $key = reset($time_key);
            if (isset($arr[$key])) {
                $arr[$str_key] = self::db_to_time_array($arr[$key]);

                if ($unset) {
                    unset($arr[$key]);
                }
            }
            return;
        }

        if (reset($time_key) === '*') {
            array_shift($time_key);
            foreach ($arr as &$arr_next) {
                if (is_array($arr_next)) {
                    self::db_to_time_array_in_array($arr_next, $time_key, $str_key, $unset);
                }
            }
        } else {
            $key = reset($time_key);
            if (isset($arr[$key])) {
                array_shift($time_key);
                self::db_to_time_array_in_array($arr[$key], $time_key, $str_key, $unset);
            }
        }
    }
    
    public function ct_get($co_id) {
        $cond = array('ct_course_id' => $co_id);
        $ret = $this->where($cond)->select();
        
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
}
