<?php

class LeadingCourseModel extends RelationModel
{
    public function lc_get_leading_course_nums($co_num) {
        $cond = array('lc_course_num' => $co_num);
        $ret = $this->where($cond)->getField('lc_leading_course_num', true);
        
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
    
    public function lc_get_all() {
        $ret = $this->select();
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }

    public function batch_add($course_map)
    {
        $this->batch_remove(array_keys($course_map));

        foreach ($course_map as $course_id => $leading_course_id_array) {
            foreach ($leading_course_id_array as $leading_course_id) {
                $data[] = array(
                    'lc_course_id'=> $course_id,
                    'lc_leading_course_id' => $leading_course_id
                );
            }
        }
        $ret = $this->addAll($data, array(), true);

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }

    public function batch_remove($id)
    {
        $condition['lc_course_id'] = array('in', $id);
        $ret = $this->where($condition)->delete();
        return $ret;
    }
}