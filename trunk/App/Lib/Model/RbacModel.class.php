<?php

class RbacModel extends Model
{
    protected $_db_user = null;
    protected $_db_role_user = null;
    protected $_db_role = null;
    protected $_db_node = null;
    protected $_db_access = null;

    public static function encode_password($pwd){
        return md5($pwd);
    }

    public function _initialize()
    {
        $this->_db_user = M(C('USER_AUTH_MODEL'));
        $this->_db_role_user = M(C('RBAC_USER_TABLE'));
        $this->_db_role = M(C('RBAC_ROLE_TABLE'));
        $this->_db_node = M(C("RBAC_NODE_TABLE"));
        $this->_db_access = M(C("RBAC_ACCESS_TABLE"));
    }

    public function get_role($id)
    {
        $r = $this->_db_role_user->where(array('user_id' => $id))->getField('role_id');

        if (empty($r)) {
            $r = C('ROLE_NONE');
        }

        return $r;
    }

    public function get_user_id($account)
    {
        $r = $this->_db_user->where(array('username' => $account))->getField('id');

        if (empty($r)) {
            $r = false;
        }

        return $r;
    }

    public function set_password($account, $new_pwd){
        $r = $this->_db_user->where(array('username'=>$account))->save(array('password' => RbacModel::encode_password($new_pwd)));
        if (false !== $r) {
            $r = true;
        }

        return $r;
    }

    public function get_access_path()
    {
        $db = M();
        $r = $db->query('SELECT access.role_id, node.app_name, node.action_name, node.method_name
FROM  (SELECT role_id, node_id FROM rbac_access WHERE level=3) as access,
(SELECT node1.id, node3.name as app_name, node2.name as action_name, node1.name as method_name FROM rbac_node as node1, rbac_node as node2,rbac_node as node3 where node1.level = 3 and node1.pid = node2.id and node2.pid = node3.id
) as node
WHERE node.id = access.node_id');
        return $r;
    }

    public function update_access($data) {
        trace_user(__CLASS__, __FUNCTION__, "in");
        $id = 1;
        $level = 1;
        $pids = array();
        array_push($pids, 0);

        $node = array();
        $access = array();
        foreach ($data as $app => $actions) {
            $node[] = array(
                'id' => $id,
                'name' => $app,
                'title' => $app,
                'status' => 1,
                'remark' => null,
                'sort' => null,
                'pid' => end($pids),
                'level' => $level
            );

            array_push($pids, $id);
            ++$id;
            ++$level;

            foreach ($actions as $action => $methods) {
                $node[] = array(
                    'id' => $id,
                    'name' => $action,
                    'title' => $action,
                    'status' => 1,
                    'remark' => null,
                    'sort' => null,
                    'pid' => end($pids),
                    'level' => $level
                );

                array_push($pids, $id);
                ++$id;
                ++$level;
                foreach ($methods as $method => $roles) {
                    $node[] = array(
                        'id' => $id,
                        'name' => $method,
                        'title' => $method,
                        'status' => 1,
                        'remark' => null,
                        'sort' => null,
                        'pid' => end($pids),
                        'level' => $level
                    );

                    foreach($roles as $role => $value){
                        if (1 == $value){
                            foreach($pids as $key => $pid){
                                if ($key == 0) {
                                    continue;
                                }
                                $access[] = array(
                                    'role_id' => $role + 1,
                                    'node_id' => $pid,
                                    'level' => $key,
                                );
                            }
                            $access[] = array(
                                'role_id' => $role + 1,
                                'node_id' => $id,
                                'level' => $level,
                            );
                        }
                    }

                    ++$id;
                }

                array_pop($pids);
                --$level;
            }

            array_pop($pids);
            --$level;
        }

        $this->_db_node->execute("TRUNCATE TABLE " . C('RBAC_NODE_TABLE'));
        $this->_db_access->execute("TRUNCATE TABLE " . C('RBAC_ACCESS_TABLE'));
        
        $r = $this->_db_node->addAll($node);
        if (false === $r){
            trace_user(__CLASS__, __FUNCTION__, "add data to rbac_node fail");
            return false;
        }
                
        $r = $this->_db_access->addAll($access);
        if (false === $r){
            trace_user(__CLASS__, __FUNCTION__, "add data to rbac_access fail");
            return false;
        }

        $access = $this->_db_access->query('SELECT DISTINCT * FROM rbac_access');
        if (false === $access){
            trace_user(__CLASS__, __FUNCTION__, "query rbac_access fail");
            return false;
        }

        $this->_db_access->execute("TRUNCATE TABLE " . C('RBAC_ACCESS_TABLE'));
        $r = $this->_db_access->addAll($access);
        if (0 === $r) {
            trace_user(__CLASS__, __FUNCTION__, "query rbac_access result:%s", $r);
            return false;
        }
        else {
            trace_user(__CLASS__, __FUNCTION__, "query rbac_access result:%s", "success");
            return true;
        }
    }
    
    public function add_user($data, $role){
        $result = FALSE;
                
        //判断角色是否错误
        if($role != C('ROLE_ADMIN') &&
                $role != C('ROLE_TEACHER') &&
                $role != C('ROLE_STUDENT')){
            trace_user(__CLASS__, __FUNCTION__, __LINE__);
            $result = FALSE;
            return $result;
        }
        
        $this->startTrans();
        
        foreach ($data as $value) {
            
            $user_data = array();       //插入rbac_data表的数据
            $role_user_data = array();  //插入rbac_role_user表的数据
            
            $user_data['username'] = $value[1];
            $user_data['password'] = $this->encode_password(C('DEFAULT_PWD'));
            
            $result = $this->_db_user->add($user_data);
            
            if($result === FALSE){
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }
           
            $role_user_data['user_id'] = $this->_db_user->getLastInsID();
            $role_user_data['role_id'] = $role;
            
            $result = $this->_db_role_user->add($role_user_data);
            
            if($result === FALSE){
                trace_user(__CLASS__, __FUNCTION__, __LINE__);
                break;
            }
        }
        
        if($result === FALSE){
            $this->rollback();
        }
        else{
            $this->commit();
            $result = TRUE;
        }
        return $result;
    }
    
    public function delete_user($username) {
        $ret = FALSE;
        
        $this->startTrans();
        do
        {
            $cond = array('username' => $username);
            $id = $this->_db_user->where($cond)->getField('id');
            if ($id === FALSE || $id === NULL)
            {
                break;
            }
            
            $result = $this->_db_user->where(array('id' => $id))->delete();
            if ($result === FALSE || $result === 0)
            {
                break;
            }
            
            $result = $this->_db_role_user->where(array('user_id' => $id))->delete();
            if ($result === FALSE || $result === 0)
            {
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        if ($ret === FALSE)
        {
            $this->rollback();
        }
        else
        {
            $this->commit();
        }
        
        return $ret;
    }
    
    public function remove_users($usernames) {
        $ret = TRUE;
        
        foreach ($usernames as $username) 
        {
            $result = $this->delete_user($username);
            
            if ($result === FALSE)
            {
                $ret = FALSE;
                break;
            }
        }
        
        return $ret;
    }
}