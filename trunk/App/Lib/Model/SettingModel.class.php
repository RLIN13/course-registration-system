<?php

class SettingModel extends Model {
    private $_default_id = 1;

    public function get_times($format = true)
    {
        $keys = array(
            'se_direction_one_start_time',
            'se_direction_one_end_time',
            'se_direction_two_start_time',
            'se_direction_two_end_time',
            'se_course_start_time',
            'se_course_end_time'
        );

        $result = $this->field($keys)->find();

        if ($result && $format) {
            foreach ($keys as $key) {
                if (isset($result[$key])) {
                    $result[$key] = SettingModel::format_time_from_db($result[$key]);
                }
            }
        }
        
        return $result;
    }
    
    public function get_select_first_times() {
        $field = array('se_direction_one_start_time', 'se_direction_one_end_time');
        return $this->field($field)->select();
    }
    
    public function get_select_second_times() {
        $field = array('se_direction_two_start_time', 'se_direction_two_end_time');
        return $this->field($field)->select();
    }
    
    public function get_select_third_times() {
        $field = array('se_course_start_time', 'se_course_end_time');
        return $this->field($field)->select();
    }

    public function get_direction_grade(){
        return $this->getField('se_direction_grade');
    }

    public function get_special_good_percent() {
        return $this->getField('se_special_good_percent');
    }

    public function get_normal_good_percent() {
        return $this->getField('se_normal_good_percent');
    }

    public function se_update($one_start_time = false, $one_end_time = false, 
                                $two_start_time = false, $two_end_time = false,
                                $three_start_time = false, $three_end_time = false, 
                                $special_good_percent = false, $normal_good_percent = false, 
                                $grade = false)
    {
        $data = array();
        $cond = array('se_id' => $this->_default_id);
        
        if (!empty($one_start_time)) {
            $data['se_direction_one_start_time'] = $one_start_time;
        }
        if (!empty($one_end_time)) {
            $data['se_direction_$one_end_time'] = $one_end_time;
        }
        if (!empty($two_start_time)) {
            $data['se_direction_$two_start_time'] = $two_start_time;
        }
        if (!empty($two_end_time)) {
            $data['se_direction_$two_end_time'] = $two_end_time;
        }
        if (!empty($three_start_time)) {
            $data['se_course_start_time'] = $three_start_time;
        }
        if (!empty($three_end_time)) {
            $data['se_course_end_time'] = $three_end_time;
        }
        if (!empty($special_good_percent) && is_numeric($special_good_percent)) {
            $data['se_special_good_percent'] = $special_good_percent;
        }
        if (!empty($normal_good_percent) && is_numeric($normal_good_percent)) {
            $data['se_normal_good_percent'] = $normal_good_percent;
        }
        if (!empty($grade) && is_numeric($grade)) {
            $data['se_direction_grade'] = $grade;
        }

        $ret = $this->where($cond)->save($data); //更新选课时间，成功返回1，失败返回false

        if (FALSE !== $ret) {
            $ret = TRUE;
        }

        return $ret;
    }

    public static function format_time_from_db($str_time){
        return str_replace(' ', '_', $str_time);
    }

}
