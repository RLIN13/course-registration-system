<?php

class GoodStudentDirectionModel extends Model
{

    public function append($student, $direction)
    {
        $data['gsd_student_id'] = $student;
        $data['gsd_direction_id'] = $direction;

        $ret = $this->add($data, array(), true);

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }
}
