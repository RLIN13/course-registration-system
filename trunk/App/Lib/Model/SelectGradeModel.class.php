<?php

class SelectGradeModel extends Model {
    public function update_select_grades($grades) {
        $ret = FALSE;
        
        $this->startTrans();
        
        do
        {
            $result = $this->where(TRUE)->delete();
            
            if ($result === FALSE)
            {
                break;
            }
            
            foreach ($grades as $grade) {
                $data['sg_grade'] = $grade;
                $result = $this->add($data);
                
                if ($result === FALSE)
                {
                    break;
                }
            }
        } while (0);
        
        if ($result === FALSE)
        {
            $this->rollback();
        }
        else
        {
            $this->commit();
            $ret = TRUE;
        }
        
        return $ret;
    }
    
    public function get_select_grades() {
        return $this->getField('sg_grade', TRUE);
    }
}
