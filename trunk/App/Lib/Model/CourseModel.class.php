<?php

class CourseModel extends RelationModel
{
    protected $_link = array(
        'time' => array(
            'mapping_type' => HAS_MANY,
            'class_name' => 'CourseTime',
            'foreign_key' => 'ct_course_id',
            'mapping_name' => 'link_time',
            'mapping_fields' => 'ct_day, ct_time',
        ),
        'student' => array(
            'mapping_type' => HAS_MANY,
            'class_name' => 'CourseStudent',
            'foreign_key' => 'cs_course_id',
            'mapping_name' => 'link_student',
        ),
        'leading_course' => array(
            'mapping_type' => HAS_MANY,
            'class_name' => 'LeadingCourse',
            'foreign_key' => 'lc_course_id',
            'mapping_name' => 'link_leading_course',
            'mapping_key' => 'co_number',
            'mapping_fields' => 'lc_leading_course_id',
            'relation_deep' => 'link_leading_course_name',
        ),
    );

    public function get_grade_list() {
        $grade_list = $this->Distinct(true)->field('co_grade')->select();

        if (false !== $grade_list) {
            $ret = array();
            foreach ($grade_list as $grade) {
                $ret[] = $grade['co_grade'];
            }
        } else {
            $ret = false;
        }

        return $ret;
    }

    public function get_semester_list() {
        $semester_list = $this->Distinct(true)->order('co_semester desc')->getField('co_semester', true);
        $ret = empty($semester_list) ? false : $semester_list;

        return $ret;
    }

    public function co_upload($course_data) {
        $ret = false;

        $this->startTrans();

        foreach ($course_data as $line) {
            $data['co_id'] = $line['id'][0];
            $data['co_semester'] = $line['id'][1];
            $data['co_number'] = $line['id'][2];
            $data['co_teacher'] = $line['id'][3];
            $data['co_class_num'] = $line['id'][4];
            $data['co_name'] = $line['name'];
            $data['co_grade'] = $line['grade'];
            $data['co_direction'] = isset($line['direction']) ? $line['direction'] : 0;
            $data['co_max_num'] = $line['max_student_num'];
            $data['co_leading_course'] = $line['leading_course'] ? 1 : 0;
            $data['co_credit'] = $line['credit'];
            $data['link_time'] = CourseTimeModel::time_array_to_db($line['id'][0], $line['week'], $line['time']);

            $ret = $this->relation('link_time')->add($data, array(), true);

            if (false === $ret) {
                trace_user(__CLASS__, __FUNCTION__, "add fail");
                break;
            }
        }

        if (false === $ret) {
            $this->rollback();
        } else {
            $this->commit();
            $ret = true;
        }

        return $ret;
    }

    public function co_search($keyword, $grade, $semester, $page, $page_size = 10, $count = false) {
        $page = false === $page ? 1 : $page;
        $page_size = false === $page_size ? 10 : $page_size;
        $condition = array();

        if (!empty($keyword)) {
            $condition['co_id|co_name'] = array('like', '%' . $keyword . '%');
        }

        if (!empty($grade)) {
            $condition['co_grade'] = $grade;
        }

        if (!empty($semester)) {
            $condition['co_semester'] = $semester;
        }

        if ($count)
        {
            $ret = $this->where($condition)->count();
            $ret = false === $ret ? 0 : $ret;
            $ret = ceil($ret / $page_size);
        }
        else
        {
            $ret = $this->relation(array('link_time', 'link_leading_course'))->where($condition)->Page($page, $page_size)->select();
            if (false === $ret || null === $ret)
            {
                $ret = array();
            }
        }

        return $ret;
    }
    
    public function read_course_student($id) {
        $ret =  $this->query(<<<SQL
                SELECT s.s_id, s.s_grade, s.s_class, s.s_name
                FROM course_student as cs, student as s
                WHERE cs.cs_course_id = '$id' and cs.cs_student_id = s.s_id
                ORDER BY s.s_id
SQL
        );
        //dump($ret);
        return $ret;
    }

    public function co_get_selectable_course() {
        $cond = array('co_selected' => 0);
        
        $ret = $this->relation('link_time')->where($condition)->select();
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
    
    public function read_grab_course($page,$page_size,$grade, $count = false){
        $page = false === $page ? 1 : $page;
        $page_size = false === $page_size ? 10 : $page_size;
        $condition = array();

        if (!empty($grade)) {
            $condition['co_grade'] = $grade;
        }

        $condition['co_selected'] = 0;

        if ($count)
        {
            $ret = $this->where($condition)->count();
            $ret = false === $ret ? 0 : $ret;
            $ret = ceil($ret / $page_size);
        }
        else
        {
            $ret = $this->relation(array('link_time', 'link_leading_course'))->where($condition)->Page($page, $page_size)->select();
            if (false === $ret || null === $ret)
            {
                $ret = array();
            }
        }

        return $ret;
    }

    public function co_get_selected_num($co_id){
        $cond = array('co_id' => $co_id);
        
        $result = $this->where($cond)->getField('co_selected_num');
        
        if($result === null)
        {
            return FALSE;
        }
        
        return $result;
    }

    public function co_get_max_num($co_id){
        $cond = array('co_id' => $co_id);
        
        $result = $this->where($cond)->getField('co_max_num');
        
        if($result === null)
        {
            return FALSE;
        }
        
        return $result;
    }
    
    public function co_increase_selected_num($co_id){
        $cond['co_id'] = $co_id;
        $cond['co_selected_num'] = array('exp', '< co_max_num');
        
        $result = $this->where($cond)->setInc('co_selected_num');
        
        if (false === $result || 0 == $result) {
            $result = false;
        } else {
            $result = true;
        }

        return $result;
    }

    public function co_decrease_selected_num($co_id){
        $cond['co_id'] = $co_id;
        $cond['co_selected_num'] = array('gt', 0);
        $result = $this->where($cond)->setDec('co_selected_num');
        if (false === $result || 0 == $result) {
            $result = false;
        } else {
            $result = true;
        }

        return $result;
    }

    public function get_course_id_number($direction_id) {
      $cond = array('co_direction' => $direction_id);
      $result = $this->where($cond)->getField('co_id,co_number');
      if ($result === null)
      {
          $result = FALSE;
      }
      return $result;
    }

    public function  get_course($student_id, $semester, $page, $page_size, $count) {
        $page = empty($page) ? 1 : $page;
        $page_size = empty($page_size) ? 10 : $page_size;

        $condition = array();
        $condition['cs.cs_student_id'] = $student_id;

        if (!empty($semester)) {
            $condition['co.co_semester'] = $semester;
        }

        $this->alias('co')
            ->join('course_student as cs ON co.co_id = cs.cs_course_id')
            ->where($condition);
        if ($count)
        {
            $ret = $this->count();
            $ret = false === $ret ? 0 : $ret;
            $ret = ceil($ret / $page_size);
        }
        else
        {
            $ret = $this->relation('link_time')
                ->field('co_id, co_semester, co_number, co_teacher, co_name, co_direction, co_credit')
                ->Page($page, $page_size)->select();
            if (false === $ret || null === $ret)
            {
                $ret = array();
            }
        }

        return $ret;
    }
    
    public function get_course_by_teacher_id($t_id){
        $result = FALSE;
        
        $cond = array('co_teacher' => $t_id);
        $result = $this->relation('link_time')->where($cond)->select();
        
        if(NULL === $result){
            $result = FALSE;
        }
        
        return $result;
    }

    public function get_student_select_course($s_id){
        $condition = array();
        $condition['cs.cs_student_id'] = $s_id;
        $condition['co.co_selected'] = 0;

        $this->alias('co')
            ->join('course_student as cs ON co.co_id = cs.cs_course_id')
            ->where($condition);

        $ret = $this->relation('link_time')
            ->field('co_id, co_semester, co_number, co_teacher, co_name, co_direction, co_credit')
            ->select();
        if (false === $ret || null === $ret) {
            $ret = array();
        }

        return $ret;
    }

    public function set_all_selected() {
        $condition['co_selected'] = 0;
        $save_data['co_selected'] = 1;

        $ret = $this->where($condition)->save($save_data);
        dump($this->getLastSql());

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }
    
    public function co_find($id, $link = false) {
        $ret = FALSE;
        
        do
        {
            $cond = array('co_id' => $id);

            if ($link) 
            {
                $this->relation(array('link_time', 'link_leading_course'));
            }

            $result = $this->where($cond)->find();
            
            if ($result !== FALSE)
            {
                $ret = $result;
            }
        } while(0);
        
        return $ret;
    }
    
    public function co_update($id, $name = FALSE, $grade = FALSE, $direction = FALSE, 
                                $max_num = FALSE, $leading_course = FALSE, 
                                $introduction = FALSE, $credit = FALSE) {
        $ret = FALSE;

        do
        {
            $cond = array();
            $cond['co_id'] = $id;
            $data = array();
            
            if($name !== FALSE)
            {
                $data['co_name'] = $name;
            }
            if($grade !== FALSE)
            {
               $data['co_grade'] = $grade;
            }
            if($direction !== FALSE)
            {
                $data['co_direction'] = $direction;
            }
            if($max_num !== FALSE)
            {
                $data['co_max_num'] = $max_num;
            }
            if($leading_course !== FALSE)
            {
                $data['co_leading_course'] = $leading_course;
            }
            if($credit !== FALSE)
            {
                $data['co_credit'] = $credit;
            }
            if($introduction !== FALSE)
            {
                $data['co_introduction'] = $introduction;
            }
            
            $result = $this->where($cond)->save($data);
            
            if ($result === FALSE)
            {
                break;
            }
            
            $ret = $result;
        }while(0);
        
        return $ret;
    }
    
    public function co_get_name($co_id) {
        $cond = array('co_id' => $co_id);
        
        $ret = $this->where($cond)->getField('co_name');
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
    
    public function co_get($co_id) {
        $cond = array('co_id' => $co_id);
        
        $ret = $this->where($cond)->select();
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
}