<?php

class CourseStudentModel extends Model
{

    public function set_pass($pass_data, $score = false)
    {
        $ret = true;
        $this->startTrans();

        foreach ($pass_data as $data) {
            if (count($data) < 3) {
                continue;
            }

            $where = array();

            $where['cs_course_id'] = reset($data);
            $where['cs_student_id'] = next($data);

            if ($score) {
                $value = next($data) >= 60 ? 1 : 0;
            } else {
                $value = next($data);
            }

            $ret = $this->where($where)->setField('cs_pass', $value);

            if (false === $ret) {
                break;
            }
        }

        if (false === $ret) {
            $this->rollback();
        } else {
            $this->commit();
            $ret = true;
        }

        return $ret;
    }

    public function cs_create($course_id, $student_id, $course_number) {
        $data = array();
        $data['cs_course_id'] = $course_id;
        $data['cs_student_id'] = $student_id;
        $data['cs_course_number'] = $course_number;

        $ret = $this->add($data);

        if ($ret === FALSE)
        {
            return FALSE;
        }

        return TRUE;
    }

    public function cs_delete($student_id, $course_id)
    {
        $ret = false;

        do {
            $condition['cs_student_id'] = array('in', $student_id);
            $condition['cs_course_id'] = array('in', $course_id);
            $ret = $this->where($condition)->delete();

            if (false === $ret)
            {
                break;
            }

            $ret = true;

        }while(0);

        return $ret;
    }

    public function cs_get_course_numbers($s_id) {
        $cond = array('cs_student_id' => $s_id);
        $ret = $this->where($cond)->getField('cs_course_number', true);
        
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
    
    public function cs_get_course_ids($s_id) {
        $cond = array('cs_student_id' => $s_id);
        $ret = $this->where($cond)->getField('cs_course_id', true);
        
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }

    public function get_course_student_id($c_id, $page, $page_size = 10, $count = FALSE){
        $ret = FALSE;
        
        do
        {
            $page = empty($page) ? 1 : $page;
            $page_size = empty($page_size) ? 10 : $page_size;
            
            $cond = array('cs_course_id' => $c_id);
            
            if($count)
            {
                $ret = $this->where($cond)->count();
                if(FALSE === $ret)
                {
                    $ret = 0;
                }
                $ret = ceil($ret / $page_size);
                break;
            }
            else 
            {
                $result = $this->where($cond)->Page($page, $page_size)->field('cs_student_id')->select();
                if($result !== FALSE)
                {
                    $ret = $result;
                }
                break;
            }
        } while(0);

        return $ret;
    }
    
    public function cs_get_all() {
        $ret = $this->select();
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
}