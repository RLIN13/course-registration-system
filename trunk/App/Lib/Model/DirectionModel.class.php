<?php
class DirectionModel extends Model {
    
    public function d_create($d_name, $d_max_num, $d_introduction) {
        $ret = FALSE;
        
        do
        {
            $data['d_name'] = $d_name;
            $data['d_max_num'] = $d_max_num;
            $data['d_introduction'] = $d_introduction;

            $result = $this->add($data);
            if ($result === FALSE)
            {
                break;
            }
            
            $ret = TRUE;
        } while (0);
        
        return $ret;
    }

    public function d_find($d_id){
        $cond = array('d_id' => $d_id);
        
        $ret = $this->where($cond)->select();
        
        if (empty($ret)) 
        {
            $ret = FALSE;
        }
        
        return $ret;
    }
    
    public function d_update($d_id, $d_name = FALSE, $d_selected_num = FALSE, $d_max_num = FALSE, $d_introduction = FALSE) {
        $cond = array('d_id' => $d_id);
        $data = array();
        if ($d_name !== FALSE)
        {
            $data['d_name'] = $d_name;
        }
        if ($d_selected_num !== FALSE)
        {
            $data['d_selected_num'] = $d_selected_num;
        }
        if ($d_max_num !== FALSE)
        {
            $data['d_max_num'] = $d_max_num;
        }
        if ($d_introduction !== FALSE)
        {
            $data['d_introduction'] = $d_introduction;
        }
        
        $result = $this->where($cond)->save($data);
        
        if ($result === FALSE)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    public function d_get_all() {
        $ret = $this->select();
        
        if (empty($ret))
        {
            $ret = FALSE;
        }
        
        return $ret;
    }

    public function get_id_names() {
        return $this->field('d_id, d_name')->select();
    }
    
    
    public function get_name($id){
        $cond = array('d_id' => $id);
        $ret = $this->where($cond)->getField('d_name');
        
        if ($ret === null)
        {
            $ret = FALSE;
        }
        
        return $ret;
    }

    public function batch_remove($id)
    {
        $ret = false;

        do {
            $condition['d_id'] = array('in', $id);
            $ret = $this->where($condition)->delete();

            if (false === $ret)
            {
                break;
            }

            $ret = true;

        }while(0);

        return $ret;
    }

    public function get_dir_id_map()
    {
        $tmp = $this->d_get_all();

        if (false === $tmp) {
            return false;
        }

        $ret = array();
        foreach ($tmp as $value) {
            $ret[$value['d_name']] = $value['d_id'];
        }

        return $ret;
    }

    public function get_empty_num()
    {
        $dir_list = $this->d_get_all();

        if (false === $dir_list) {
            return false;
        }

        $ret = array(); //卓越班&成绩好
        foreach ($dir_list as $line) {
            $id = (int)$line['d_id'];
            $ret[$id] = $line['d_max_num'] - $line['d_selected_num'];
            if ($ret[$id] <= 0) {
                unset($ret[$id]);
            }
        }

        return $ret;
    }

    public function empty_selected_num()
    {
        $data = array('d_selected_num' => 0);
        $result = $this->where(1)->save($data);
        if ($result === FALSE)
        {
            return FALSE;
        }
        else 
        {
            return TRUE;
        }
    }

    public function d_increase_selected_num($did)
    {
        $cond['d_id'] = $did;
        $cond['d_selected_num'] = array('exp', '< d_max_num');
        $result = $this->where($cond)->setInc('d_selected_num');
        if (false === $result || 0 == $result) {
            $result = false;
        } else {
            $result = true;
        }

        return $result;
    }

    public function d_decrease_selected_num($did)
    {
        $cond['d_id'] = $did;
        $cond['d_selected_num'] = array('gt', 0);
        $result = $this->where($cond)->setDec('d_selected_num');
        if (false === $result || 0 == $result) {
            $result = false;
        } else {
            $result = true;
        }

        return $result;
    }

    public function d_get_selected_num($d_id) {
        $cond = array('d_id' => $d_id);
        $result = $this->where($cond)->getField('d_selected_num');
        if($result === null)
        {
            $result = FALSE;
        }
        return $result;
    }

    public function d_get_max_num($d_id) {
        $cond = array('d_id' => $d_id);
        $result = $this->where($cond)->getField('d_max_num');
        if($result === null)
        {
            return FALSE;
        }
        return $result;
    }
    
    
    
    public function get_id_by_name($name) {
        $cond = array('d_name' => $name);
        $ret = $this->where($cond)->getField('d_id');
        
        if ($ret === null)
        {
            $ret = FALSE;
        }
        
        return $ret;
    }
}
