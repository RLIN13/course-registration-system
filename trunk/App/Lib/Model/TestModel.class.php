<?php

class TestModel extends Model {
    //put your code here
    function truncate_table($table) {
        $ret = $this->execute("truncate table $table");
        if ($ret === false)
        {
            $msg = $this->getDbError();
            trace_user(__CLASS__, __FUNCTION__, "truncate table $table fail" . $msg);
        }
    }
    
    function disable_foreign_key() {
        $this->query('SET foreign_key_checks=0');
    }
    
    function enable_foreign_key() {
        $this->query('SET foreign_key_checks=1');
    }
            
    
    function init_class() {
        $this->truncate_table("class");
        
        for ($grade = 2011; $grade <= 2014; $grade++)
        {
            for ($class = 1; $class <= 7; $class++)
            {
                $name;
                $special = 0;
                switch ($class)
                {
                case 1:
                    $name = '一班';
                    break;
                case 2:
                    $name = '二班';
                    break;
                case 3:
                    $name = '三班';
                    break;
                case 4:
                    $name = '四班';
                    break;
                case 5:
                    $name = '五班';
                    break;
                case 6:
                    $name = '六班';
                    break;
                case 7:
                    $name = '卓越班';
                    $special = 1;
                    break;
                }
                $ret = $this->execute("insert into class (c_grade, c_num, c_name, c_special) values ($grade, $class, '$name', $special)");
                
                if ($ret === false)
                {
                    trace_user( __CLASS__, __FUNCTION__, "insert $grade $class fail");
                }
            }
        }
    }
    
    function init_course() {
        $this->truncate_table("course");
        
        $ret = $this->execute(<<<SQL
            INSERT INTO `course` (`co_id`, `co_semester`, `co_number`, `co_teacher`, `co_class_num`, `co_name`, `co_grade`, `co_direction`, `co_max_num`, `co_leading_course`, `co_introduction`, `co_credit`, `co_selected_num`, `co_selected`) VALUES
            ('2011-2012-1-155058-G10000-1', '2011-2012-1', '155058', 'G10000', 1, 'Java语言程序设计', 2012, 0, 47, 0, 'java', 2, 0, 0),
            ('2011-2012-2-155344-G10001-1', '2011-2012-2', '155344', 'G10001', 1, '计算模型与算法技术', 2012, 1, 43, 0, NULL, 2.5, 0, 0),
            ('2011-2012-2-155355-G10002-1', '2011-2012-2', '155355', 'G10002', 1, '程序设计方法学', 2011, 0, 47, 1, NULL, 2.5, 0, 0),
            ('2012-2013-1-155152-G10004-1', '2012-2013-1', '155152', 'G10004', 1, '计算机图形学', 2011, 3, 46, 0, NULL, 1, 0, 0),
            ('2012-2013-1-155328-G10005-1', '2012-2013-1', '155328', 'G10005', 1, '数字媒体处理技术', 2010, 3, 47, 1, NULL, 2.5, 0, 0),
            ('2012-2013-1-155353-G10006-1', '2012-2013-1', '155353', 'G10006', 1, '金融业务实务', 2011, 5, 42, 0, NULL, 2.5, 0, 0),
            ('2012-2013-1-155359-G10007-1', '2012-2013-1', '155359', 'G10007', 1, '游戏设计与开发', 2011, 3, 40, 0, NULL, 3, 0, 0),
            ('2012-2013-2-145042-G10008-1', '2012-2013-2', '145042', 'G10008', 1, '信息系统安全', 2011, 4, 50, 0, NULL, 1.5, 0, 0),
            ('2012-2013-2-155329-G10009-1', '2012-2013-2', '155329', 'G10009', 1, '嵌入式系统软件设计', 2011, 6, 47, 0, NULL, 1.5, 0, 0);
SQL
            );
        
        if ($ret === false)
        {
            trace_user(__CLASS__, __FUNCTION__, "init course fail");
        }
            
    }
    
    function init_course_time() {
        $this->truncate_table("course_time");
        
        $ret = $this->execute(<<<SQL
            INSERT INTO `course_time` (`ct_course_id`, `ct_week`, `ct_day`, `ct_time`) VALUES
            ('2011-2012-1-155058-G10000-1', '4-18', 2, '3'),
            ('2011-2012-1-155058-G10000-1', '4-18', 5, '3'),
            ('2011-2012-2-155344-G10001-1', '4-18', 2, '12'),
            ('2011-2012-2-155355-G10002-1', '4-18', 1, '112'),
            ('2011-2012-2-155355-G10002-1', '4-18', 5, '7'),
            ('2012-2013-1-155152-G10004-1', '4-18', 4, '192'),
            ('2012-2013-1-155328-G10005-1', '4-18', 3, '3'),
            ('2012-2013-1-155353-G10006-1', '4-18', 5, '12'),
            ('2012-2013-1-155359-G10007-1', '4-18', 1, '51'),
            ('2012-2013-1-155359-G10007-1', '4-18', 5, '51'),
            ('2012-2013-2-145042-G10008-1', '4-18', 6, '192'),
            ('2012-2013-2-155329-G10009-1', '4-18', 7, '3');
SQL
            );
        
        if ($ret === false)
        {
            trace_user(__CLASS__, __FUNCTION__, "init course time fail");
        }
        
    }
    
    function init_course_student(){
        $this->truncate_table("course_student");
        
        for ($student_id = 201100000001; $student_id <= 201100000030; $student_id++)
        {
            $ret = $this->execute("INSERT INTO course_student (cs_course_id, cs_student_id, "
                    . "cs_course_number, cs_pass) VALUES "
                    . "('2011-2012-1-155058-G10000-1', '$student_id', '155058', 0)");
            
            if ($ret === false)
            {
                trace_user(__CLASS__, __FUNCTION__, "init course student fail");
            }
        }
        
    }
    
    function init_direction() {
        $this->truncate_table("direction");
        
        $ret = $this->execute(<<<SQL
            INSERT INTO `direction` (`d_id`, `d_name`, `d_selected_num`, `d_max_num`, `d_introduction`) VALUES
            (1, '智能软件方向', 0, 60, NULL),
            (2, '信息服务方向', 0, 60, NULL),
            (3, '数字媒体方向', 0, 60, NULL),
            (4, '移动计算方向', 0, 60, NULL),
            (5, '金融软件方向', 0, 60, NULL),
            (6, '嵌入式软件方向', 0, 60, NULL);
SQL
            );
        if ($ret === false)
        {
            trace_user(__CLASS__, __FUNCTION__, "init direction fail");
        }
    }
    
    function init_rbac() {
        $this->truncate_table("rbac_user");
        $this->truncate_table("rbac_role_user");
        
        $pass = md5("123456");
        $this->execute("insert into rbac_user (username, password) values ('admin', '$pass')");
        $id = $this->getLastInsID();
        $this->execute("insert into rbac_role_user (role_id, user_id) values (1, $id)");
    }
            
    function init_student() {
        $this->truncate_table("student");
        
        for ($year = 2011; $year <= 2014; $year++)
        {
            $index = 1;
            for ($class = 1; $class <= 7; $class++)
            {
                for ($n = 0; $n < 40; $n++)
                {
                    $id = $year * 100000000 + $index;
                    $index++;
                    $name = '学生' . $id;
                    $sex = mt_rand(0, 1) == 0 ? '男': '女';
                    $phone = 13000000000 + mt_rand(000000000, 999999999);
                    $email = $id . '@qq.com';
                    $lib = mt_rand(0, 1);
                    $good = mt_rand(0, 1);
                    $this->execute("INSERT INTO student (s_id, s_grade, s_class, s_name, "
                            . "s_sex, s_phone, s_email, s_direction, s_select_credit, "
                            . "s_pass_credit, s_dir_select_credit, s_dir_pass_credit, "
                            . "s_lib, s_good) VALUES "
                            . "('$id', $year, $class, '$name', '$sex', '$phone', '$email', 0, 0, 0, 0, 0, $lib, $good)");

                    if ($ret === false)
                    {
                        trace_user(__CLASS__, __FUNCTION__, "init student fail");
                    }
                    
                    $pass = md5("123456");
                    $this->execute("insert into rbac_user (username, password) values ('$id', '$pass')");
                    $user_id = $this->getLastInsID();
                    $this->execute("insert into rbac_role_user (role_id, user_id) values (3, $user_id)");
                }
            }
        }
    }
    
    function init_teacher() {
        $this->truncate_table("teacher");
        
        for ($i = 10000; $i <= 10009; $i++)
        {
            $id = 'G' . $i;
            $name = '老师' . $i;
            $sex = mt_rand(0, 1) == 0 ? '男': '女';
            $phone = 13000000000 + mt_rand(000000000, 999999999);
            $email = $id . '@qq.com';
            $ret = $this->execute("INSERT INTO teacher (t_id, t_name, t_sex, t_phone, "
                    . "t_email, t_introduction) VALUES "
                    . "('$id', '$name', '$sex', '$phone', '$email', NULL)");
            
            if ($ret === false)
            {
                trace_user(__CLASS__, __FUNCTION__, "init teacher fail");
            }
            
            $pass = md5("123456");
            $this->execute("insert into rbac_user (username, password) values ('$id', '$pass')");
            $user_id = $this->getLastInsID();
            $this->execute("insert into rbac_role_user (role_id, user_id) values (2, $user_id)");
        }
        
        
    }
}
