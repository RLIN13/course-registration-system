<?php

class StudentModel extends RelationModel
{
    protected $_link = array(
        'ssd' => array(
            'mapping_type' => HAS_MANY,
            'class_name' => 'SpecialStudentDirection',
            'foreign_key' => 'ssd_student_id',
            'mapping_name' => 'link_ssd',
        ),
    );

    public function s_search($keyword, $grade, $class, $page, $page_size=10, $count = FALSE){
        $result = null;
        
        $page = false === $page ? 1 : $page;
        $page_size = false === $page_size ? 10 : $page_size;
        
        $cond = array();
        if(false !== $keyword){
            $cond['s_id|s_name'] = array('like', '%' . $keyword . '%');
        }
        if(FALSE !== $grade && FALSE !== $class){
            $cond['s_grade']     = $grade;
            $cond['s_class']     = $class;
        }
        
        if($count){
            $result = $this->where($cond)->count();
            $result = false === $result ? 0 : $result;
            $result = ceil($result / $page_size);
        }
        else {
            $result = $this
                    //->field('s_id,s_grade,s_name,s_sex')
                    ->where($cond)
                    ->page($page, $page_size)
                    ->select();
            if($result === FALSE || $result === NULL){
                $result = array();
            }
        }
        
        return $result;
    }
    public function s_search_credit($keyword, $grade, $select_credit, $credit_bottom, $credit_top, $page, $page_size = 10, $count = false) {
        $credit_bottom = empty($credit_bottom) ? 0 : $credit_bottom;
        $credit_top = empty($credit_top) ? 1000 : $credit_top;
        $page = empty($page) ? 1 : $page;
        $page_size = empty($page_size) ? 10 : $page_size;

        if (!empty($keyword)) {
            $cond['s_id|s_name'] = array('like', '%' . $keyword . '%');
        }

        if (!empty($grade)) {
            $cond['s_grade'] = $grade;
        }

        $type_array['1'] = 's_select_credit';
        $type_array['2'] = 's_pass_credit';
        $type_array['3'] = 's_dir_select_credit';
        $type_array['4'] = 's_dir_pass_credit';
        $credit = $type_array[$select_credit];
        $cond[$credit] = array(array('egt', $credit_bottom), array('elt', $credit_top), 'and');

        if ($count) {
            $ret = $this->where($cond)->count();
            $ret = false === $ret ? 0 : $ret;
            $ret = ceil($ret / $page_size);
        } else {
            $ret = $this->where($cond)->Page($page, $page_size)->select();
            if (false === $ret || null === $ret) {
                $ret = array();
            }
        }

        return $ret;
    }
    
    public function s_find($id){
        if (empty($id)) {
            return false;
        }

        $cond = array('s_id' => $id);
        $ret = $this->where($cond)->select();

        if (empty($ret)) {
            $ret = false;
        }

        return $ret;
    }

    public function s_update($id, $name, $sex, $grade, $class, $phone, $email, $direction, $lib, $good){
        $ret = false;
        
        do {
            $data = array();
            
            if (NULL !== $name) {
                $data['s_name'] = $name;
            }
            if (NULL !== $sex) {
                $data['s_sex'] = $sex;
            }
            if (NULL !== $grade) {
                $data['s_grade'] = $grade;
            }
            if (NULL !== $class) {
                $data['s_class'] = $class;
            }
            if (NULL !== $phone) {
                $data['s_phone'] = $phone;
            }
            if (NULL !== $email) {
                $data['s_email'] = $email;
            }
            if (NULL !== $direction) {
                $data['s_direction'] = $direction;
                if (empty($direction)) {
                    $data['s_direction'] = null;
                }
            }
            if (NULL !== $lib) {
                $data['s_lib'] = $lib;
            }
            if (NULL !== $good) {
                $data['s_good'] = $good;
            }
            
            $cond = array('s_id' => $id);
            
            $ret = $this->where($cond)->save($data);
            if (false !== $ret) {
                $ret = true;
            }
        } while(0);

        return $ret;
    }

    public function s_delete($s_id){
        $ret = TRUE;
        
        $cond = array('s_id' => $s_id);
        $result = $this->where($cond)->delete();
        
        if ($result === FALSE)
        {
            $ret = FALSE;
        }
        
        return $ret;
    }
    
    public function s_remove($s_ids) {
        $ret = TRUE;
        
        foreach ($s_ids as $s_id) 
        {
            $result = $this->s_delete($s_id);
            
            if ($result === FALSE)
            {
                $ret = FALSE;
                break;
            }
        }
        
        return $ret;
    }
    
    public function s_upload($data) {
        $result = FALSE;

        $this->startTrans();
        
        foreach ($data as $value) {
            if(count($value) < 2){
                continue;
            }
            
            $insert = array();
            $insert['s_id'] = $value[1];
            $insert['s_name'] = $value[2];
            if(isset($value[3])){
                $insert['s_grade'] = $value[3];
            }
            if(isset($value[4])){
                $insert['s_class'] = $value[4];
            }
            if(isset($value[5])){
                $insert['s_sex'] = $value[5];
            }
            if(isset($value[6])){
                $insert['s_phone'] = $value[6];
            }
            if(isset($value[7])){
                $insert['s_email'] = $value[7];
            }
            if(isset($value[8])){
                $insert['s_direction'] = $value[8];
            }
            if(isset($value[9])){
                $insert['s_select_credit'] = $value[9];
            }
            if(isset($value[10])){
                $insert['s_pass_credit'] = $value[10];
            }
            if(isset($value[11])){
                $insert['s_lib'] = $value[11];
            }else{
                $insert['s_lib'] = 0;
            }
            if(isset($value[12])){
                $insert['s_good'] = $value[12];
            }else{
                $insert['s_good'] = 0;
            }
            
            
            $result = $this->add($insert);
            
            if($result === FALSE){
                break;
            }
        }
        
        if($result === FALSE){
            $this->rollback();
        }
        else{
            $this->commit();
            $result = TRUE;
        }
        
        return $result;
    }
    
    public function s_get_name($account) {
        $r = $this->where(array('s_id' => $account))->getField('s_name');
        if (empty($r)) 
        {
            $r = false;
        }

        return $r;
    }
    
    public function s_get_grade($s_id){
        $cond = array('s_id' => $s_id);
        
        $resunlt = $this->where($cond)->getField('s_grade');
        
        if(empty($resunlt))
        {
            $resunlt = FALSE;
        }
        return $resunlt;
    }

    public function s_set_lib($id, $direction) {
        $ret = false;

        $this->startTrans();

        do {
            $cond['s_id'] = $id;
            $ret = $this->where($cond)->select();      //查询学生是否存在

            if (null === $ret || false === $ret) {
                $ret = false;
                break;
            }

            $ret = $this->where($cond)->setField('s_lib', true); //存在的学生更新s_lib = 1
            if (false === $ret) {
                break;
            }

            //增加ssd表记录
            $ssd = new GoodStudentDirectionModel();
            $ret = $ssd->append($id, $direction);

            if (false === $ret) {
                break;
            }
        } while (0);

        if (false === $ret) {
            $this->rollback();
        } 
        else {
            $this->commit();
            $ret = true;
        }

        return $ret;
    }

    public function s_set_good($id, $direction) {
        $ret = false;

        $this->startTrans();

        do {
            $cond['s_id'] = $id;

            $ret = $this->where($cond)->select();   //查询学生是否存在
            if(null === $ret || false === $ret) {
                $ret = false;
                break;
            }

            $ret = $this->where($cond)->setField('s_good', true); //存在的学生更新s_good = 1
            if (false === $ret) {
                break;
            }

            //增加ssd表记录
            $ssd = new GoodStudentDirectionModel();
            $ret = $ssd->append($id, $direction);
            
            if(false === $ret) {
               break;
            }
        }while(0);

        if (false === $ret) {
            $this->rollback();
        } 
        else {
            $this->commit();
            $ret = true;
        }

        return $ret;
    }
    
    public function update_pass_credit() {
        do {
            $ret = $this->execute(<<<SQL
                    UPDATE student,   (SELECT cs.cs_student_id, SUM(c.co_credit) as pass_credit
                                        FROM course_student as cs, course as c
                                        WHERE cs.cs_course_id = c.co_id and cs.cs_pass > 0
                                        group by cs.cs_student_id) as credit
                    SET s_pass_credit = credit.pass_credit
                    WHERE student.s_id = credit.cs_student_id;
SQL
            );

            if (false === $ret) {
                break;
            }

            $ret = $this->execute(<<<SQL
                    UPDATE student,   (SELECT cs.cs_student_id, SUM(c.co_credit) as dir_pass_credit
                                        FROM course_student as cs, course as c, student as s
                                        WHERE cs.cs_course_id = c.co_id and cs.cs_pass > 0
                                        and s.s_id = cs.cs_student_id
                                        and s.s_direction = c.co_direction
                                        group by cs.cs_student_id) as credit
                    SET s_dir_pass_credit = credit.dir_pass_credit
                    WHERE student.s_id = credit.cs_student_id;
SQL
            );

        } while (0);

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }

    public function update_select_credit() {
        do {
            $ret = $this->execute(<<<SQL
                    UPDATE student,   (SELECT cs.cs_student_id, SUM(c.co_credit) as select_credit
                                        FROM course_student as cs, course as c
                                        WHERE cs.cs_course_id = c.co_id
                                        group by cs.cs_student_id) as credit
                    SET s_select_credit = credit.select_credit
                    WHERE student.s_id = credit.cs_student_id;
SQL
            );

            if (false === $ret) {
                break;
            }

            $ret = $this->execute(<<<SQL
                    UPDATE student,   (SELECT cs.cs_student_id, SUM(c.co_credit) as dir_select_credit
                                        FROM course_student as cs, course as c, student as s
                                        WHERE cs.cs_course_id = c.co_id
                                        and s.s_id = cs.cs_student_id
                                        and s.s_direction = c.co_direction
                                        group by cs.cs_student_id) as credit
                    SET s_dir_select_credit = credit.dir_select_credit
                    WHERE student.s_id = credit.cs_student_id;
SQL
            );
        } while (0);

        if (false !== $ret) {
            $ret = true;
        }

        return $ret;
    }

    public function s_get_grades() {
        $grade_list = $this->Distinct(true)->field('s_grade')->select();

        if (false !== $grade_list) {
            $ret = array();
            foreach ($grade_list as $grade) {
                $ret[] = $grade['s_grade'];
            }
        } 
        else {
            $ret = false;
        }

        return $ret;
    }

    public function s_get_direction($id) {
        $cond = array('s_id' => $id);
        
        $ret = $this->where($cond)->getField('s_direction');

        if(empty($ret))
        {
            $ret = FALSE;
        }
        
        return $ret;
    }

    public function s_set_direction($id, $direction) {
        $cond = array('s_id' => $id);
        $data = array('s_direction' => $direction);
        
        $ret = $this->where($cond)->save($data);
        
        if ($ret === FALSE) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function s_get_id_did() {
        $result = $this->getField('s_id,s_direction');
        if($result === null) 
        {
            $result =FALSE;
        }
        return $result;
    }

    public function s_get_credit($id) {
        $ret = $this->where(array('s_id' => $id))->field("s_select_credit, s_pass_credit, s_dir_select_credit, s_dir_pass_credit")->find();
        if (empty($ret)) 
        {
            $ret = false;
        }

        return $ret;
    }
}