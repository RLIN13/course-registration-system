<?php

class TeacherModel extends Model
{
    public function t_search($key, $page, $page_size = 10, $count = false){
        $page = false === $page ? 1 : $page;
        $page_size = false === $page_size ? 10 : $page_size;
        
        $cond = array();
        $ret = null;
        if(false !== $key){
            $cond['t_id|t_name'] = array('like', '%' . $key . '%');
        }
        
        if($count){
            $ret = $this->where($cond)->count();
            $ret = false === $ret ? 0 : $ret;
            $ret = ceil($ret / $page_size);
        }
        else {
            $ret = $this->where($cond)->page($page, $page_size)->select();
            if($ret === false || $ret === NULL){
                $ret = array();
            }
        }
        
        return $ret;
    }

    public function t_find($id){
        if (empty($id)) {
            return false;
        }

        $cond = array('t_id' => $id);
        $ret = $this->where($cond)->select();
        if(empty($ret)){
            $ret = false;
        }
        return $ret;
    }

    public function t_update($id, $name, $sex, $phone, $email, $introduction){
        $ret = false;
        do{
            $data = array();
            if(NULL !== $name){
                $data['t_name'] = $name;
            }
            if(NULL !== $sex){
                $data['t_sex'] = $sex;
            }
            if(NULL !== $phone){
                $data['t_phone'] = $phone;
            }
            if(NULL !== $email){
                $data['t_email'] = $email;
            }
            if(NULL !== $introduction){
                $data['t_introduction'] = $introduction;
            }
            
            $cond = array('t_id' => $id);
            $ret = $this->where($cond)->save($data);

            if(false !== $ret){
                $ret = true;
            }
        }while (0);
        
        return $ret;
    }

    public function t_delete($id){
        $ret = false;
        $cond = array('t_id' => $id);
        $ret = $this->where($cond)->delete();
        
        //删除0行也认为是删除失败
        if(false !== $ret && 0 !== $ret){
            $ret = true;
        }
        return $ret;
    }
    
    public function t_remove($t_ids) {
        $ret = TRUE;
        
        $this->startTrans();
        
        foreach ($t_ids as $t_id) 
        {
            trace_user(__CLASS__, __FUNCTION__, $t_id);
            $result = $this->t_delete($t_id);
            
            if ($result === FALSE)
            {
                $ret = FALSE;
                break;
            }
        }
        
        if ($ret === FALSE)
        {
            $this->rollback();
        }
        else
        {
            $this->commit();
        }
        
        return $ret;
    }

    public function t_create($id, $name, $sex = '', $phone = '', $email = '', $introduction = ''){
        $ret = false;
        $insert = array();
        
        $insert['t_id'] = $id;
        $insert['t_name'] = $name;
        if('' !== $sex){
            $insert['t_sex'] = $sex;
        }
        if('' !== $phone){
            $insert['t_phone'] = $phone;
        }
        if('' !== $email){
            $insert['t_email'] = $email;
        }
        if('' !== $introduction){
            $insert['t_introduction'] = $introduction;
        }
        
        $ret = $this->add($insert);
        
        if(false !== $ret){
            $ret = true;
        }
        return $ret;
    }

    public function t_upload($data){
        $ret = false;
        $this->startTrans();
        
        foreach ($data as $value) {
            if(count($value) < 2){
                continue;
            }
            
            $insert = array();
            $insert['t_id'] = $value[1];
            $insert['t_name'] = $value[2];
            if(isset($value[3])){
                $insert['t_sex'] = $value[3];
            }
            if(isset($value[4])){
                $insert['t_phone'] = $value[4];
            }
            if(isset($value[5])){
                $insert['t_email'] = $value[5];
            }
            if(isset($value[6])){
                $insert['t_introduction'] = $value[6];
            }
            
            $ret = $this->add($insert);
            
            if($ret === false){
                break;
            }
        }
        
        if($ret === false){
            $this->rollback();
        }
        else{
            $this->commit();
            $ret = true;
        }
        
        return $ret;
    }

    public function t_get_name($id)
    {
        $ret = $this->where(array('t_id' => $id))->getField('t_name');
        if (empty($ret)) {
            $ret = false;
        }
        
        return $ret;
    }
}
