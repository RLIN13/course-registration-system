<?php
class RedisLib {
    private $redis = null;
    
    function __construct() {
        $this->redis = new Redis();
        $this->redis->connect(C('REDIS_HOST'), C('REDIS_PORT'));
    }
    public function flush_db() {
        $this->redis->flushDB();
    }
    
    public function watch($key) {
        $this->redis->watch($key);
    }
    
    public function multi() {
        $this->redis->multi();
    }

    public function exec() {
        return $this->redis->exec();
    }

    public function exists($key) {
        return $this->redis->exists($key);
    }
    
    public function del($key) {
        return $this->redis->del($key);
    }

        public function expire_at($key, $time) {
        $this->redis->expireAt($key, $time);
    }
    
    public function keys($patterm) {
        return $this->redis->keys($patterm);
    }
    
    public function set($key, $value) {
        $this->redis->set($key, $value);
    }
    
    public function set_nx($key, $value) {
        return $this->redis->setnx($key, $value);
    }
    
    public function get($key) {
        return $this->redis->get($key);
    }
    
    public function h_set($key, $field, $value) {
        $this->redis->hSet($key, $field, $value);
    }
    
    public function h_get($key, $field) {
        return $this->redis->hGet($key, $field);
    }
            
    public function h_get_all($key) {
        $ret = $this->redis->hGetAll($key);
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
    
    public function h_inc($key, $field) {
        $this->redis->hIncrBy($key, $field, 1);
    }
    
    public function h_dec($key, $field) {
        $this->redis->hIncrBy($key, $field, -1);
    }
    
    public function s_add($key, $valud) {
        $this->redis->sAdd($key, $valud);
    }
    
    public function s_member($key) {
        $ret = $this->redis->sMembers($key);
        if (empty($ret))
        {
            return FALSE;
        }
        
        return $ret;
    }
}
