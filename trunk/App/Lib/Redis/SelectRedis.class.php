<?php
include_once './App/Lib/Redis/RedisLib.php';

class SelectRedis extends RedisLib{
    public function init_direction_first() {
        $this->flush_db();
        
        $dirction = new DirectionModel();
        $direction_datas = $dirction->d_get_all();
        foreach ($direction_datas as $direction_data) 
        {
            $key = 'direction.' . $direction_data['d_id'];
            foreach ($direction_data as $field => $value) 
            {
                $this->h_set($key, $field, $value);
            }
        }
        
        $selecting_direction = new SelectingDirectionModel();
        $selecting_direction_datas = $selecting_direction->sd_get_all();
        foreach ($selecting_direction_datas as $selecting_direction_data) 
        {
            $key = 'selecting_direction.' . $selecting_direction_data['sd_student_id'];
            $this->h_set($key, $selecting_direction_data['sd_level'], $selecting_direction_data['sd_direction_id']);
        }
        
        $setting = new SettingModel();
        $grade = $setting->get_direction_grade();
        $this->set('selecting_direction_grade', $grade);
        
        $time_first = $setting->get_select_first_times();
        $current_time = strtotime('now');
        if ($time_first[0]['se_direction_one_start_time'] <= $current_time)
        {
            $key = 'direction_first';
            $this->set($key, TRUE);
            $this->expire_at($key, $time_first[0]['se_direction_one_end_time']);
        }
    }
    
    public function get_direction_first($username) {
        $temp = array();
        $direction_keys = $this->keys('direction.*');
        foreach ($direction_keys as $direction_key) 
        {
            $temp[] = $this->h_get_all($direction_key);
        }
        $ret['data'] = $temp;
        
        $ret['selected'] = $this->h_get_all('selecting_direction.' . $username);
        
        return $ret;
    }
    
    public function select_direction_first($username, $grade, $choices) {
        $ret = array(FALSE, '选课失败');
        
        do
        {
            if ($this->exists('direction_first') === FALSE)
            {
                $ret[1] = '现在不是第一轮选课时间';
                break;
            }
            if ($this->get('selecting_direction_grade') !== $grade)
            {
                $ret[1] = '你没有权限选方向';
                break;
            }
            
            foreach ($choices as $value) 
            {
                $key = 'selecting_direction.' . $username;
                $this->h_set($key, $value[1], $value[0]);
            }
            
            $ret[0] = TRUE;
            $ret[1] = '选课成功';
        } while (0);
        
        return $ret;
    }
    
    public function resotre_direction_first() {
        $selecting_direction = new SelectingDirectionModel();
        $selecting_direction_keys = $this->keys('selecting_direction.*');
        foreach ($selecting_direction_keys as $$selecting_direction_key) 
        {
            $data = $this->h_get_all($selecting_direction_key);
            $result = $selecting_direction->sd_create($data['sd_student_id'], $data['sd_direction_id'], $data['sd_level']);
            
            if ($result === FALSE)
            {
                trace_user(__CLASS__, __FUNCTION__, __LINE__ . $selecting_direction_key);
            }
        }
    }
    
    public function init_direction_second() {
        $this->flush_db();
        
        $dirction = new DirectionModel();
        $direction_datas = $dirction->d_get_all();
        foreach ($direction_datas as $direction_data) 
        {
            $key = 'direction.' . $direction_data['d_id'];
            foreach ($direction_data as $field => $value) 
            {
                $this->h_set($key, $field, $value);
            }
        }
        
        $student = new StudentModel();
        $student_id_did = $student->s_get_id_did();
        foreach ($student_id_did as $s_id => $d_id) 
        {
            if ($d_id == 0)
            {
                continue;
            }
            $key = 'selected_direction.' . $s_id;
            $d_name = $dirction->get_name($d_id);
            
            $this->h_set($key, 'd_id', $d_id);
            $this->h_set($key, 'd_name', $d_name);
        }
        
        $setting = new SettingModel();
        $grade = $setting->get_direction_grade();
        $this->set('selecting_direction_grade', $grade);
        
        $time_second = $setting->get_select_second_times();
        $current_time = strtotime('now');
        if ($time_second[0]['se_direction_two_start_time'] <= $current_time)
        {
            $key = 'direction_second';
            $this->set($key, TRUE);
            $this->expire_at($key, $time_second[0]['se_direction_two_end_time']);
        }
    }
    
    public function get_direction_second($username) {
        $temp = array();
        $direction_keys = $this->keys('direction.*');
        foreach ($direction_keys as $direction_key) 
        {
            $temp[] = $this->h_get_all($direction_key);
        }
        $ret['data'] = $temp;
        
        $key = 'selected_direction.' . $username;
        $ret['selected'] = $this->h_get_all($key);

        return $ret;
    }
    
    public function select_direction_second($username, $grade, $d_id) {
        $ret = array(FALSE, '选课失败');
        
        do
        {
            if ($this->exists('direction_second') === FALSE)
            {
                $ret[1] = '现在不是第二轮抢课时间';
                break;
            }
            
            if ($this->get('selecting_direction_grade') !== $grade)
            {
                $ret[1] = '你没有权限选方向';
                break;
            }
            
            $selected_direction_key = 'selected_direction.' . $username;
            if ($this->exists($selected_direction_key) === TRUE)
            {
                $ret[1] = '已有所属方向';
                break;
            }
            
            if ($this->exists('direction.'.$d_id) === FALSE)
            {
                $ret[1] = '没有该方向';
                break;
            }
            
            $direction_key = 'direction.' . $d_id;
            $result = FALSE;
            /*
            do
            {
                //开启事务
                $this->watch($direction_key);
                
                $max = $this->h_get($direction_key, 'd_max_num');
                $selected = $this->h_get($direction_key, 'd_selected_num');
                if ($max <= $selected)
                {
                    $ret[1] = '该方向人数已满';
                    break;
                }
                
                $this->multi();
                $this->h_inc($direction_key, 'd_selected_num');
                $result = $this->exec();
            } while ($result === FALSE);
            
            if ($result === FALSE)
            {
                break;
            }
            */
            $max = $this->h_get($direction_key, 'd_max_num');
            $selected = $this->h_get($direction_key, 'd_selected_num');
            if ($max <= $selected)
            {
                $ret[1] = '该方向人数已满';
                break;
            }
            $this->h_inc($direction_key, 'd_selected_num');
            $this->h_set($selected_direction_key, 'd_id', $d_id);
            $this->h_set($selected_direction_key, 'd_name', $this->h_get($direction_key, 'd_name'));
            
            $ret[0] = TRUE;
            $ret[1] = '选课成功';
        } while (0);
        
        return $ret;
    }
    
    public function cancle_direction_second($username, $grade, $d_id) {
        $ret = array(false, '退选失败');
        
        do
        {
            if ($this->exists('direction_second') === FALSE)
            {
                $ret[1] = '现在不是第二轮抢课时间';
                break;
            }
            
            if ($this->get('selecting_direction_grade') !== $grade)
            {
                $ret[1] = '你没有权限选方向';
                break;
            }
            
            $selected_direction_key = 'selected_direction.' . $username;
            $direction_key = 'direction.' . $d_id;
            if ($this->h_get($selected_direction_key, 'd_id') != $d_id)
            {
                $ret[1] = '没有所属方向';
                break;
            }
            
            $this->del($selected_direction_key);
            $this->h_dec($direction_key, 'd_selected_num');
            
            $ret[0] = true;
            $ret[1] = '退选成功';
        } while (0);
        
        return $ret;
    }
    
    public function restore_direction_second() {
        
    }
    
    public function init_course_third() {
        $this->flush_db();
        
        $course = new CourseModel();
        $course_datas = $course->co_get_selectable_course();
        TeacherDataAction::get_name_in_array($course_datas, array('*', 'co_teacher'), 'co_teacher_name');
        DirectionDataAction::get_name_in_array($course_datas, array('*', 'co_direction'), 'co_direction_name');
        CourseTimeModel::db_to_string_in_array($course_datas, array('*', 'link_time'), 'link_time');
        foreach ($course_datas as $course_data) 
        {
            $course_key = 'course.' . $course_data['co_id'];
            foreach ($course_data as $field => $value) 
            {
                $this->h_set($course_key, $field, $value);
            }
        }
        
        $course_student = new CourseStudentModel();
        $course_student_datas = $course_student->cs_get_all();
        foreach ($course_student_datas as $course_student_data) 
        {
            $course_student_key = 'selected_course.' . $course_student_data['cs_student_id'] . '.' . $course_student_data['cs_course_id'];
            $this->h_set($course_student_key, 'co_id', $course_student_data['cs_course_id']);
            $this->h_set($course_student_key, 'co_num', $course_student_data['cs_course_number']);
            $this->h_set($course_student_key, 'co_name', $course->co_get_name($course_student_data['cs_course_id']));
        }
        
        $leading_course = new LeadingCourseModel();
        $leading_course_datas = $leading_course->lc_get_all();
        foreach ($leading_course_datas as $leading_course_data) 
        {
            $leading_course_key = 'leading_course.' . $leading_course_data['lc_course_num'];
            $this->s_add($leading_course_key, $leading_course_data['lc_leading_course_num']);
        }
        
        $select_grade = new SelectGradeModel();
        $select_grades = $select_grade->get_select_grades();
        foreach ($select_grades as $grade)
        {
            $this->s_add('select_grade', $grade);
        }
        
        $setting = new SettingModel();        
        $time_second = $setting->get_select_third_times();
        $current_time = strtotime('now');
        if ($time_second[0]['se_course_start_time'] <= $current_time)
        {
            $key = 'course_third';
            $this->set($key, TRUE);
            $this->expire_at($key, $time_second[0]['se_course_end_time']);
        }
    }
    
    public function get_course_third($username) {
        $temp = array();
        $course_keys = $this->keys('course.*');
        foreach ($course_keys as $course_key) 
        {
            $temp[] = $this->h_get_all($course_key);
        }
        $ret['data'] = $temp;

        $selected_t = array();
        $selected_keys = $this->keys('selected_course.' . $username . '.*');
        foreach ($selected_keys as $selected_key) 
        {
            $selected_t[] = $this->h_get_all($selected_key);
        }
        $ret['selected'] = $selected_t;

        return $ret;
    }
    
    public function select_course_third($username, $grade, $co_id) {
        $ret = array(false, '');
        
        do
        {
            if ($this->exists('course_third') === FALSE)
            {
                $ret[1] = '现在不是第三轮选课时间';
                break;
            }
            
            $select_grades = $this->s_member('select_grade');
            if (in_array($grade, $select_grades) === FALSE)
            {
                $ret[1] = '你没有权限退课';
                break;
            }
            
            $leading_course_key = 'leading_course.' . $co_id;
            $leading_courses = $this->s_member($leading_course_key);
            if ($leading_courses !== FALSE)
            {
                //需要检查
            }
            
            $course_key = 'course.' . $co_id;
            $result = FALSE;
//            do
//            {
//                $this->watch($course_key);
//                $this->multi();
//                if ($this->h_get($course_key, 'co_max_num') <= $this->h_get($course_key, 'co_selected_num'))
//                {
//                    $ret[1] = '该课程人数已满';
//                    break;
//                }
//                
//                $this->h_inc($course_key, 'co_selected_num');
//                $result = $this->exec();
//            } while ($result === FALSE);
//            
//            if ($result === FALSE)
//            {
//                break;
//            }
            
            $max = $this->h_get($course_key, 'co_max_num');
            $selected = $this->h_get($course_key, 'co_selected_num');
            if ($max <= $selected)
            {
                $ret[1] = '该课程人数已满';
                break;
            }
                
            $this->h_inc($course_key, 'co_selected_num');
            
            $course_data = $this->h_get_all($course_key);
            $course_student_key = 'selected_course.' . $username . '.' . $co_id;
            $this->h_set($course_student_key, 'co_id', $co_id);
            $this->h_set($course_student_key, 'co_num', $course_data['co_number']);
            $this->h_set($course_student_key, 'co_name', $course_data['co_name']);
            
            $ret[0] = TRUE;
            $ret[1] = '选课成功';
        } while (0);
        
        return $ret;
    }
    
    public function cancle_course_third($username, $grade, $co_id) {
        $ret = array(false, '');
        
        do
        {
            if ($this->exists('course_third') === FALSE)
            {
                $ret[1] = '现在不是第三轮选课时间';
                break;
            }
            
            $select_grades = $this->s_member('select_grade');
            if (in_array($grade, $select_grades) === FALSE)
            {
                $ret[1] = '你没有权限退课';
                break;
            }
            
            $selected_course_key = 'selected_course.' . $username . '.' . $co_id;
            $course_key = 'course.' . $co_id;
            if ($this->exists($selected_course_key) === FALSE)
            {
                $ret[1] = '你没有选择该课';
                break;
            }
            
            $this->del($selected_course_key);
            $this->h_dec($course_key, 'co_selected_num');
            
            $ret[0] = TRUE;
            $ret[1] = '退课成功';
        } while (0);
        
        return $ret;
    }
    
    public function restore_course_third() {
        
    }
}
