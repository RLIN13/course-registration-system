<?php

//去除敏感字符，主要用于防SQL注入
//Chen Xuanyi
function scan_parameter(&$post_arr)
{
    if (is_array($post_arr)) {
        foreach ($post_arr as &$post_arr2) {
            scan_parameter($post_arr2);
        }
    } else {
        $post = &$post_arr;
        $post = trim($post); //去除首尾空白字符
        $post = strip_tags($post, ""); //清除HTML等代码
        $post = str_replace("\n", "", str_replace(" ", "", $post)); //去掉空格和换行
        $post = str_replace("\t", "", $post); //去掉制表符号
        $post = str_replace("\r\n", "", $post); //去掉回车换行符号
        $post = str_replace("\r", "", $post); //去掉回车
        $post = str_replace("'", "", $post); //去掉单引号
        $post = str_replace("\"", "", $post); //去掉双引号
        $post = str_replace("(", "", $post); //去掉(
        $post = str_replace(")", "", $post); //去掉)
        $post = trim($post);
    }
}

//安全的get，已SQL注入敏感字，_b表示没有该数据时返回布尔型的false
//Chen Xuanyi
function safe_get_b($key)
{
    $ret = I('get.'.$key, false, 'htmlspecialchars');
    $ret = 'false' === $ret ? false : $ret;
    $ret = 'true' === $ret ? true: $ret;
    if (false !== $ret) {
        scan_parameter($ret);
    }

    return $ret;
}

//安全的post，没有该数据时返回false
//Chen Xuanyi
function safe_post_b($key)
{
    $ret = I('post.'.$key, false, 'htmlspecialchars');
    if (false !== $ret) {
        scan_parameter($ret);
    }

    return $ret;
}

//接受*.xls文件，成功返回[true, 路径]，失败返回[false,errorMsg]
//Chen Xuanyi
function get_upload_xls($max_Size = 3145728, $replace = true)
{
    import('ORG.Net.UploadFile');
    $upload = new UploadFile(); // 实例化上传类
    $upload->maxSize = $max_Size; // 设置附件上传大小
    $upload->allowExts = array('xls'); // 设置附件上传类型
    $upload->savePath = './public/Uploads/'; // 设置附件上传目录
    $upload->uploadReplace = $replace;

    //目录不存在是创建，linux下起作用
    if (!file_exists('./public/Uploads/')) {
        //createFolder(dirname(''./Public/upload/excel/'));
        mkdir('./public/Uploads/', 0777);
    }

    if (!$upload->upload()) { // 上传错误
        return array(false, $upload->getErrorMsg());
    }

    //上传成功 获取上传文件信息
    $info = $upload->getUploadFileInfo();
    $name = $info[0]['savename'];
    $path = $info[0]['savepath'];
    return array(true, $path . $name);
}


//对数组进行urlencode，支持多维，不支持中文键值
//Chen Xuanyi
function array_urlencode(&$arr)
{
    if (!is_array($arr)) {
        $arr = urlencode($arr);
        return;
    }

    foreach ($arr as &$value) {
        array_urlencode($value);
    }
}

function trace_user($class, $function, $format) 
{
    $var_array = func_get_args();
    array_shift($var_array);
    array_shift($var_array);
    array_shift($var_array);
    
    $message = vsprintf($format, $var_array);
    trace($message, $class . ":" . $function, 'user', true);
}