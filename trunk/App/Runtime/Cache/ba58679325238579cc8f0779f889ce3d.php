<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/font-awesome.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/teacher/teacher_information.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_common.js"></script>
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = {"data":[{"t_id":"G10000","t_name":"老师10000","t_sex":"女","t_phone":"13238853172","t_email":"G10000@qq.com","t_introduction":""}]};
        var name = "老师10000";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="teacher-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username" id="header-username"></span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Teacher/teacher_course_manage"><em class="icon-book scolor"></em>课程管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">个人信息</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Teacher/teacher_information"><em class="icon-user scolor"></em>个人信息</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"> <div class="u-info-wrap">
        <form class="clearfix" id="single-teacher-form">
            <fieldset disabled>
        
                <div class="form-item">
                    <span class="form-item-name">工号</span>
                    <input class="form-item-value" id="single-teacher-form-id">
                </div>
                <div class="form-item">
                    <span class="form-item-name">姓名</span>
                    <input class="form-item-value" id="single-teacher-form-name"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">性别</span>
                    <select class="form-item-value" id="single-teacher-form-sex">
                        <option value="男">男</option>
                        <option value="女">女</option>
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">电话</span>
                    <input class="form-item-value" id="single-teacher-form-phone">
                </div>
                <div class="form-item">
                    <span class="form-item-name">邮箱</span>
                    <input class="form-item-value" id="single-teacher-form-email">
                </div>
                 
                <div> 
                <div class="form-item">
                    <span class="form-item-name">简介</span>
                    <textarea class="form-item-value" id="single-teacher-form-intro" maxlengh=50 disabled = "true" cols="30" rows="5"></textarea>
                </div>
                </div>
                
                 </fieldset>  
            <div id = "error" style="display:none; color:#F00;"></div>
                 
            
            <div class="modify-wrapper" style="margin-top:50px;">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            
            <div class="save-wrapper" style="margin-top:50px;">
                <button class="btn u-btn" id="save-btn" >保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div> 

    <script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_information.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始密码" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>