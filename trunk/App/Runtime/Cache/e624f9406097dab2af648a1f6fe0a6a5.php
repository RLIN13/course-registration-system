<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="__ROOT__/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="__ROOT__/public/css/font-awesome.min.css">  
    <link rel="stylesheet" href="__ROOT__/public/css/common.css">
    <link rel="stylesheet" href="__ROOT__/public/css/student/credit.css">
    <link rel="stylesheet" href="__ROOT__/public/css/student/student_course_manage.css">
    <link rel="stylesheet" href="__ROOT__/public/css/student/student_course_schedule.css">
    <script type="text/javascript" src="__ROOT__/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/common.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/student/student_common.js"></script>
    
    <script type="text/javascript">
        ROOT = "__ROOT__";
        var data = <?php echo (($data)?($data):0); ?>;
        var name = <?php echo (($name)?($name):"'未知'"); ?>;
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="student-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username" id="header-username"></span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "__ROOT__/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/student_course_manage"><em class="icon-tasks scolor"></em>课程管理</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/sutdent_course_schedule_manage"><em class="icon-user-md scolor"></em>个人课表</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >选课</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/student_direction_first_manage"><em class="icon-book scolor"></em>第一轮选方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/student_direction_second_manage"><em class="icon-bookmark scolor"></em>第二轮抢方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/student_course_third_manage"><em class="icon-suitcase scolor"></em>第三轮抢课</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学分统计</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/student_credit_manage"><em class="icon-plus-sign-alt scolor"></em>学分统计</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">个人信息</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Student/student_information"><em class="icon-user scolor"></em>个人信息</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container">