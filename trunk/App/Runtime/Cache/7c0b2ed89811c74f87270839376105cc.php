<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/credit.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/course.css">    
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/teacher.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/student.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/account.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/lhgcalendar.css">    
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/font-awesome.min.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/admin_common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/lhgcalendar.min.js"></script>
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = {"count":"112","page":"6","data":[{"s_id":"201100000051","s_grade":"2011","s_class":"2","s_name":"学生201100000051","s_sex":"女","s_phone":"13630676908","s_email":"201100000051@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000052","s_grade":"2011","s_class":"2","s_name":"学生201100000052","s_sex":"女","s_phone":"13874776026","s_email":"201100000052@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000053","s_grade":"2011","s_class":"2","s_name":"学生201100000053","s_sex":"男","s_phone":"13647520767","s_email":"201100000053@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000054","s_grade":"2011","s_class":"2","s_name":"学生201100000054","s_sex":"男","s_phone":"13266969465","s_email":"201100000054@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000055","s_grade":"2011","s_class":"2","s_name":"学生201100000055","s_sex":"男","s_phone":"13875212162","s_email":"201100000055@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000056","s_grade":"2011","s_class":"2","s_name":"学生201100000056","s_sex":"女","s_phone":"13412989057","s_email":"201100000056@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000057","s_grade":"2011","s_class":"2","s_name":"学生201100000057","s_sex":"男","s_phone":"13582178734","s_email":"201100000057@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000058","s_grade":"2011","s_class":"2","s_name":"学生201100000058","s_sex":"男","s_phone":"13988393614","s_email":"201100000058@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000059","s_grade":"2011","s_class":"2","s_name":"学生201100000059","s_sex":"女","s_phone":"13582179689","s_email":"201100000059@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000060","s_grade":"2011","s_class":"2","s_name":"学生201100000060","s_sex":"男","s_phone":"13542292472","s_email":"201100000060@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"}],"grade_list":{"2011":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]],"2012":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]],"2013":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]],"2014":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]]}};
        var name = "";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="admin-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username">教务员</span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_course_manage"><em class="icon-book scolor"></em>课程管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">选课管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_select_time_manage"><em class="icon-time scolor"></em>选课时间</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_special_student_manage"><em class="icon-lightbulb scolor"></em>导入优先学生</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_select_setting_manage"><em class="icon-cog scolor"></em>设置</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_direction_manage"><em class="icon-hand-left scolor"></em>方向管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">学分管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_credit_manage"><em class="icon-pencil scolor"></em>学分管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >教师管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_teacher_manage"><em class="icon-user-md scolor"></em>教师管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学生管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_student_manage"><em class="icon-user scolor"></em>学生管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">账号管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_account_manage"><em class="icon-cog scolor"></em>账号管理</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"><div class="student-manage container-wrapper">
    <form class="search-form form-inline clearfix" id="student-search-form" role="form">
        <span class="form-item-name">关键字：</span>
        <input class="col-md-2" id="student-form-keyword" name="keyword" placeholder="学号或姓名" type="text">  
         <span class="form-item-name">年级：</span>
         <select class="input-small" id="student-form-grade" name="grade">
             <option value="">请选择</option>
         </select>
         <span class="form-item-name" style="margin-left:20px;">班级：</span>
        <select class="input-small" id="student-form-class" name="class" >
            <option value="">请选择</option>
        </select>
        <input class="btn btn-sm u-btn" id="student-manage-submit-btn" type="submit" value="搜索"> 
    </form>
      
         
    <div class="table-wrapper" id="student-table-wrapper">
    </div>
    
    <div class="student-manage-footer clearfix">
        <input type="button" class="student-manage-del btn btn-sm u-btn" value="删除" id="delete-student-btn" />
    </div>

    <div class="student-manage-footer clearfix">
        <div class="page-nav">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
        <form id="import-grade-form" enctype="multipart/form-data">
            <span>导入学生：</span>
            <input id="import-student-btn" name="import-student-btn" type="file">
            <button id="import-student-submit">上传</button>
        </form>
    </div>
</div>

<script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/student_manage.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始密码" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>