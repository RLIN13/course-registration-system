<?php if (!defined('THINK_PATH')) exit();?><div class="teacher-manage container-wrapper">
    <form class="search-form form-inline clearfix" id="teacher-search-form" role="form">
        <span class="form-item-name">关键字：</span>
        <input class="col-md-2" id="credit-form-keyword" name="keyword" placeholder="教工号或姓名" type="text">
        <input class="btn btn-sm u-btn" id="teacher-manage-submit-btn" type="submit" value="搜索"> 
    </form>
      
         
    <div class="table-wrapper" id="teacher-table-wrapper">
    </div>
    
    <div class="teacher-manage-footer clearfix">
        <input type="button" class="teacher-manage-del btn btn-sm u-btn" value="删除" id="delete-teacher-btn" />
    </div>

    <div class="teacher-manage-footer clearfix">
        <div class="page-nav">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
        <div>
            <span>导入教师：</span>
            <input id="import-teacher-btn" name="import-teacher-btn" type="file">
            <button id="import-teacher-submit">上传</button>
        </div>
    </div>
</div>

<script type="text/javascript" src="__ROOT__/public/js/admin/teacher_manage.js"></script>