<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/credit.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/course.css">    
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/teacher.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/student.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/account.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/lhgcalendar.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/admin_common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/lhgcalendar.min.js"></script>
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = 0;
        var name = "教务员";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="admin-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username">教务员</span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_course_manage">课程管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">选课管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_select_time_manage">选课时间</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_special_student_manage">导入优先学生</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_select_setting_manage">设置</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_direction_manage">方向管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">学分管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_credit_manage">学分管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >教师管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_teacher_manage">教师管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学生管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_student_manage">学生管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">账号管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_account_manage">账号管理</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"><div class="special-student-manage container-wrapper">
    <div class="table-wrapper" id="special-student-table-wrapper">
        <table class="table table-striped table-hover u-table" id="special-student-manage-table">
            <tbody>
                <tr>
                    <td>实验室学生</td>
                    <td>
                        <input id="import-stu-lib-btn" type="file" name="stu_lib">
                        <button type="button" id="import-stu-lib-submit">提交</button>
                    </td>
                </tr>
                <tr>
                    <td>卓越班&成绩好的学生</td>
                    <td>
                        <input id="import-stu-special-btn" type="file" name="stu_special">
                        <button type="button" id="import-stu-special-submit">提交</button>
                    </td>
                </tr>
                <tr>
                    <td>普通班&成绩好的学生</td>
                    <td>
                        <input id="import-stu-normal-btn" type="file" name="stu_normal">
                        <button type="button" id="import-stu-normal-submit">提交</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/upload_special_student.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始密码" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>