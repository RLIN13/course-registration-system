<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/teacher/teacher_course_student.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"count":"3","page":"1","data":[{"s_id":"201100000001","s_grade":"0","s_class":"0","s_name":"0","s_sex":"","s_phone":"0","s_email":"0","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000002","s_grade":"2011","s_class":"1","s_name":"学生201100000002","s_sex":"女","s_phone":"13508009194","s_email":"201100000002@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000003","s_grade":"2011","s_class":"1","s_name":"学生201100000003","s_sex":"男","s_phone":"13943112769","s_email":"201100000003@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000004","s_grade":"2011","s_class":"1","s_name":"学生201100000004","s_sex":"女","s_phone":"13573483711","s_email":"201100000004@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000005","s_grade":"2011","s_class":"1","s_name":"学生201100000005","s_sex":"女","s_phone":"13631088717","s_email":"201100000005@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000006","s_grade":"2011","s_class":"1","s_name":"学生201100000006","s_sex":"女","s_phone":"13790794670","s_email":"201100000006@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000007","s_grade":"2011","s_class":"1","s_name":"学生201100000007","s_sex":"男","s_phone":"13345894358","s_email":"201100000007@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000008","s_grade":"2011","s_class":"1","s_name":"学生201100000008","s_sex":"男","s_phone":"13293454588","s_email":"201100000008@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000009","s_grade":"2011","s_class":"1","s_name":"学生201100000009","s_sex":"男","s_phone":"13773928531","s_email":"201100000009@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000010","s_grade":"2011","s_class":"1","s_name":"学生201100000010","s_sex":"男","s_phone":"13240147430","s_email":"201100000010@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"}],"co_name":""},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="course-studetn-container">
        <div id="course-student-table-wrapper">
        </div>
        <div class="teacher-course-student-footer clearfix">
            <div class="page-nav">
                <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
                <input class="page-current" value=""></span>
                <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
                <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
            </div>
        </div>
    </div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_course_student.js"></script>
</body>
</html>