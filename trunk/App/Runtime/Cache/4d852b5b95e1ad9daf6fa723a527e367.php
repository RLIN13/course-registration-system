<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/teacher/teacher_course_student.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"count":"3","page":"2","data":[{"s_id":"201100000011","s_grade":"2011","s_class":"1","s_name":"学生201100000011","s_sex":"女","s_phone":"13947475019","s_email":"201100000011@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000012","s_grade":"2011","s_class":"1","s_name":"学生201100000012","s_sex":"女","s_phone":"13594343534","s_email":"201100000012@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000013","s_grade":"2011","s_class":"1","s_name":"学生201100000013","s_sex":"女","s_phone":"13263281409","s_email":"201100000013@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000014","s_grade":"2011","s_class":"1","s_name":"学生201100000014","s_sex":"男","s_phone":"13133650051","s_email":"201100000014@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000015","s_grade":"2011","s_class":"1","s_name":"学生201100000015","s_sex":"男","s_phone":"13970856444","s_email":"201100000015@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000016","s_grade":"2011","s_class":"1","s_name":"学生201100000016","s_sex":"男","s_phone":"13948384641","s_email":"201100000016@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000017","s_grade":"2011","s_class":"1","s_name":"学生201100000017","s_sex":"女","s_phone":"13424658334","s_email":"201100000017@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000018","s_grade":"2011","s_class":"1","s_name":"学生201100000018","s_sex":"女","s_phone":"13981293464","s_email":"201100000018@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000019","s_grade":"2011","s_class":"1","s_name":"学生201100000019","s_sex":"男","s_phone":"13979245334","s_email":"201100000019@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000020","s_grade":"2011","s_class":"1","s_name":"学生201100000020","s_sex":"女","s_phone":"13455844829","s_email":"201100000020@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"}],"co_name":""},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="course-studetn-container">
        <div id="course-student-table-wrapper">
        </div>
        <div class="teacher-course-student-footer clearfix">
            <div class="page-nav">
                <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
                <input class="page-current" value=""></span>
                <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
                <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
            </div>
        </div>
    </div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_course_student.js"></script>
</body>
</html>