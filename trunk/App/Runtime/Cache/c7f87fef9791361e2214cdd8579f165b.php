<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/credit.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/course.css">    
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/teacher.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/student.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/account.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/lhgcalendar.css">    
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/font-awesome.min.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/admin_common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/lhgcalendar.min.js"></script>
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = {"count":"112","page":"1","grade":["0","2011","2012","2013","2014"],"data":[{"s_id":"201100000001","s_grade":"0","s_class":"0","s_name":"0","s_sex":"","s_phone":"0","s_email":"0","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000002","s_grade":"2011","s_class":"1","s_name":"学生201100000002","s_sex":"女","s_phone":"13508009194","s_email":"201100000002@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000003","s_grade":"2011","s_class":"1","s_name":"学生201100000003","s_sex":"男","s_phone":"13943112769","s_email":"201100000003@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000004","s_grade":"2011","s_class":"1","s_name":"学生201100000004","s_sex":"女","s_phone":"13573483711","s_email":"201100000004@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000005","s_grade":"2011","s_class":"1","s_name":"学生201100000005","s_sex":"女","s_phone":"13631088717","s_email":"201100000005@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000006","s_grade":"2011","s_class":"1","s_name":"学生201100000006","s_sex":"女","s_phone":"13790794670","s_email":"201100000006@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000007","s_grade":"2011","s_class":"1","s_name":"学生201100000007","s_sex":"男","s_phone":"13345894358","s_email":"201100000007@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000008","s_grade":"2011","s_class":"1","s_name":"学生201100000008","s_sex":"男","s_phone":"13293454588","s_email":"201100000008@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000009","s_grade":"2011","s_class":"1","s_name":"学生201100000009","s_sex":"男","s_phone":"13773928531","s_email":"201100000009@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000010","s_grade":"2011","s_class":"1","s_name":"学生201100000010","s_sex":"男","s_phone":"13240147430","s_email":"201100000010@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"}]};
        var name = "教务员";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="admin-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username">教务员</span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_course_manage"><em class="icon-book scolor"></em>课程管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">选课管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_select_time_manage"><em class="icon-time scolor"></em>选课时间</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_special_student_manage"><em class="icon-lightbulb scolor"></em>导入优先学生</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_select_setting_manage"><em class="icon-cog scolor"></em>设置</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_direction_manage"><em class="icon-hand-left scolor"></em>方向管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">学分管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_credit_manage">学分管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >教师管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_teacher_manage">教师管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学生管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_student_manage">学生管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">账号管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Admin/admin_account_manage">账号管理</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"><div class="credit-manage container-wrapper">
    <form class="search-form form-inline clearfix" id="credit-search-form" role="form">
        <span class="form-item-name">关键字：</span>
        <input class="col-md-2" id="credit-form-keyword" name="keyword" placeholder="学生学号或姓名" type="text">
        <span class="form-item-name">年级：</span>
        <select class="input-small" id="credit-form-grade" name="grade">
            <option value=""></option>
        </select>
        <select id="credit-form-credit-type" name="select_credit">
            <option value=""></option>
            <option value="1">已选学分</option>
            <option value="2">已修学分</option>
            <option value="3">方向已选学分</option>
            <option value="4">方向已修学分</option>
        </select>
        <input class="col-md-1" id="credit-form-credit-floor" name="credit_bottom" placeholder="学分下限" type="text">
        <span class="form-item-name">&nbsp;-&nbsp;</span>
        <input class="col-md-1" id="credit-form-credit-upper" name="credit_top" placeholder="学分上限" type="text">
        <input class="btn btn-sm u-btn" id="credit-manage-submit-btn" type="submit" value="搜索"> 
    </form>

    <div class="table-wrapper" id="credit-table-wrapper">
    </div>

    <div class="credit-manage-footer clearfix">
        <div class="page-nav">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
        <div>
            <span>导入成绩：</span>
            <input id="import-grade-btn" type="file" name="import-grade-btn">
            <button type="button" id="import-grade-submit">提交</button>
        </div>
    </div>
</div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/credit.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始密码" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>