<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/single_student_manage.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"data":[{"s_id":"201100000002","s_grade":"2011","s_class":"1","s_name":"学生201100000002","s_sex":"女","s_phone":"13508009194","s_email":"201100000002@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"}],"direction":[{"d_id":"1","d_name":"智能软件方向"},{"d_id":"2","d_name":"信息服务方向"},{"d_id":"3","d_name":"数字媒体方向"},{"d_id":"4","d_name":"移动计算方向"},{"d_id":"5","d_name":"金融软件方向"},{"d_id":"6","d_name":"嵌入式软件方向"},{"d_id":"7","d_name":"1"}],"grade_class":{"2011":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]],"2012":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]],"2013":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]],"2014":[["1","一班"],["2","二班"],["3","三班"],["4","四班"],["5","五班"],["6","六班"],["7","卓越班"]]}},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="single-page">
        <form class="clearfix" id="single-student-form">
            <div class="form-wrapper clearfix">
                <div class="form-item">
                    <span class="form-item-name">姓名</span>
                    <input class="form-item-value" id="single-student-form-name"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">电话</span>
                    <input class="form-item-value" id="single-student-form-phone">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学号</span>
                    <span class="form-item-value" id="single-student-form-id"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">邮箱</span>
                    <input class="form-item-value" id="single-student-form-email">
                </div>
                <div class="form-item">
                    <span class="form-item-name">方向</span>
                    <select class="form-item-value" id="single-student-form-direction">
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">是否为优秀学生</span>
                    <select class="form-item-value" id="single-student-form-good">
                        <option value="1">是</option>
                        <option value="0">否</option>
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">性别</span>
                    <select class="form-item-value" id="single-student-form-sex">
                        <option value="男">男</option>
                        <option value="女">女</option>
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">是否已进实验室</span>
                    <select class="form-item-value" id="single-student-form-lib">
                        <option value="1">是</option>
                        <option value="0">否</option>
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">年级</span>
                    <select class="form-item-value" id="single-student-form-grade">
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">已选学分</span>
                    <span class="form-item-value" id="single-student-form-select-credit"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">班级</span>
                    <select class="form-item-value" id="single-student-form-class">
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">已修学分</span>
                    <span class="form-item-value" id="single-student-form-pass-credit"></span>
                </div>
            </div>
            <div id = "error" style="display:none; color:#F00;"></div>
            <div class="modify-wrapper">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            <div class="save-wrapper">
                <button class="btn u-btn" id="save-btn">保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div>

    <script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/single_student_manage.js"></script>
</body>
</html>