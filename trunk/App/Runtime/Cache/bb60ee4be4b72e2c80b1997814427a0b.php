<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="__ROOT__/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="__ROOT__/public/css/common.css">
    <link rel="stylesheet" href="__ROOT__/public/css/student/student_teacher_info.css">
    
    <script type="text/javascript" src="__ROOT__/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = <?php echo ($data); ?>,
            ROOT = "__ROOT__";
    </script>
</head>

<body>
    <div class="student-teacher-information-page">
        <form class="clearfix" id="student-teacher-form">
            <fieldset disabled>
                <div class="form-item">
                    <span class="form-item-name">教工号</span>
                    <span class="form-item-value" id="student-teacher-information-form-id">
                </div>
                <div class="form-item">
                    <span class="form-item-name">姓名</span>
                    <span class="form-item-value" id="student-teacher-information-form-name"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">性别</span>
                    <span class="form-item-value" id="student-teacher-information-form-sex">
                </div>
                <div class="form-item">
                    <span class="form-item-name">电话</span>
                    <span class="form-item-value" id="student-teacher-information-form-phone">
                </div>
                <div class="form-item">
                    <span class="form-item-name">邮箱</span>
                    <span class="form-item-value" id="student-teacher-information-form-email">
                </div>
                <div> 
                <div class="form-item">
                    <span class="form-item-name">简介</span>
                    <span class="form-item-value" id="student-teacher-information-form-intro"></span>
                </div>
                </div>                  
        </form> 
    </div>

    <script type="text/javascript" src="__ROOT__/public/js/student/student_teacher_info.js"></script>
</body>
</html>