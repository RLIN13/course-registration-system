<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/teacher/teacher_course_info.css">

    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"data":{"co_id":"2011-2012-2-155355-G10002-1","co_semester":"2011-2012-2","co_number":"155355","co_teacher":"老师10002","co_class_num":"1","co_name":"","co_grade":"0","co_direction":"无","co_max_num":"0","co_leading_course":"0","co_introduction":"","co_credit":"0","co_selected_num":"2","co_selected":"0","link_time":"周一第1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16节,周五第1,2,3,5,6节","link_leading_course":[{"lc_leading_course_id":"155058"}]}},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="teacher-course-information-page">
        <form class="clearfix" id="teacher-course-information-form">
            <div class="form-wrapper clearfix">
                <div class="form-item">
                    <span class="form-item-name">课程ID</span>
                    <span class="form-item-value" id="teacher-course-information-form-id"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">名称</span>
                    <span class="form-item-value" id="teacher-course-information-form-name">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学期</span>
                    <span class="form-item-value" id="teacher-course-information-form-semester">
                </div>
                <div class="form-item">
                    <span class="form-item-name">年级</span>
                    <span class="form-item-value" id="teacher-course-information-form-grade">
                </div>
                <div class="form-item">
                    <span class="form-item-name">班级数量</span>
                    <span class="form-item-value" id="teacher-course-information-form-class-num">
                </div>
                <div class="form-item">
                    <span class="form-item-name">方向</span>
                    <span class="form-item-value" id="teacher-course-information-form-direction">
                </div>
                <div class="form-item">
                    <span class="form-item-name">前导课</span>
                    <span class="form-item-value" id="teacher-course-information-form-leading-course">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学分</span>
                    <span class="form-item-value" id="teacher-course-information-form-credit">
                </div>
                <div class="form-item">
                    <span class="form-item-name">最大容量</span>
                    <span class="form-item-value" id="teacher-course-information-form-max-number">
                </div>
                <div class="form-item">
                    <span class="form-item-name">已选人数</span>
                    <span class="form-item-value" id="teacher-course-information-form-selected-num">
                </div>
                <div class="form-item">
                    <span class="form-item-name">简介</span>
                    <input class="form-item-value" id="teacher-course-information-form-introduction">
                </div>
            </div>
            <div class="modify-wrapper">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            <div class="save-wrapper">
                <button class="btn u-btn" id="save-btn">保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_course_data.js"></script>
</body>
</html>