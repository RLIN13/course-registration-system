<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/admin/course_information.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"data":{"co_id":"2011-2012-2-155344-G10001-1","co_semester":"2011-2012-2","co_number":"155344","co_teacher":"老师10001","co_class_num":"1","co_name":"计算模型与算法技术","co_grade":"2012","co_direction":"智能软件方向","co_max_num":"45","co_leading_course":"1","co_introduction":"","co_credit":"2","co_selected_num":"0","co_selected":"0","link_time":"周二第2,5,6,9,13,14节","link_leading_course":""},"grade":["0","2012","2011","2010"],"direction":[{"d_id":"1","d_name":"智能软件方向"},{"d_id":"2","d_name":"信息服务方向"},{"d_id":"3","d_name":"数字媒体方向"},{"d_id":"4","d_name":"移动计算方向"},{"d_id":"5","d_name":"金融软件方向"},{"d_id":"6","d_name":"嵌入式软件方向"},{"d_id":"7","d_name":"1"}]},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="course-information-page">
        
        <form class="clearfix" id="course-information-form">
        <div class='page-header'><div class='page-header-title'>课程信息(*为可修改)</div></div>
            <div class="form-wrapper clearfix">
                <div class="form-item">
                    <span class="form-item-name" >课程ID</span>
                    <span class="form-item-value" id="course-information-form-id" style="background-color: rgb(232, 232, 232);"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">名称*</span>
                    <input class="form-item-value" id="course-information-form-name">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学期</span>
                    <span class="form-item-value" id="course-information-form-semester" style="background-color: rgb(232, 232, 232);">
                </div>
                <div class="form-item">
                    <span class="form-item-name">年级*</span>
                    <select class="form-item-value" id="course-information-form-grade">
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">授课老师</span>
                    <span class="form-item-value" id="course-information-form-teacher" style="background-color: rgb(232, 232, 232);"></span>
                 <!--   <input class="form-item-value" id="course-information-form-teacher">  -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">方向*</span>
                    <select class="form-item-value" id="course-information-form-direction">
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">前导课*</span>
                    <input class="form-item-value" id="course-information-form-leading-course">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学分*</span>
                    <input class="form-item-value" id="course-information-form-credit">
                </div>
                <div class="form-item">
                    <span class="form-item-name">最大容量*</span>
                    <input class="form-item-value" id="course-information-form-max-number">
                </div>
                <div class="form-item">
                    <span class="form-item-name">已选人数</span>
                    <span class="form-item-value" id="course-information-form-selected" style="background-color: rgb(232, 232, 232);">
                </div>
                <div class="form-item">
                    <span class="form-item-name">上课时间</span>
                    <span class="form-item-value" id="course-information-form-time" style="background-color: rgb(232, 232, 232);">
                </div>
                <div class="form-item">
                    <span class="form-item-name">简介*</span>
                    <input class="form-item-value" id="course-information-form-introduction">
                </div>
            </div>
            <div class="modify-wrapper">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            <div class="save-wrapper">
                <button class="btn u-btn" id="save-btn">保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/admin/course_information.js"></script>
</body>
</html>