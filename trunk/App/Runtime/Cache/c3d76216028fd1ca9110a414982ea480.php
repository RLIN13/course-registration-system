<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="__ROOT__/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="__ROOT__/public/css/common.css">
    <link rel="stylesheet" href="__ROOT__/public/css/admin/credit.css">
    <link rel="stylesheet" href="__ROOT__/public/css/admin/course.css">    
    <link rel="stylesheet" href="__ROOT__/public/css/admin/teacher.css">
    <link rel="stylesheet" href="__ROOT__/public/css/admin/student.css">
    <link rel="stylesheet" href="__ROOT__/public/css/admin/account.css">
    <link rel="stylesheet" href="__ROOT__/public/css/lhgcalendar.css">    
    <link rel="stylesheet" href="__ROOT__/public/css/font-awesome.min.css">
    
    <script type="text/javascript" src="__ROOT__/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/common.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/admin/admin_common.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/lhgcalendar.min.js"></script>
    <script type="text/javascript">
        ROOT = "__ROOT__";
        var data = <?php echo (($data)?($data):0); ?>;
        var name = <?php echo (($name)?($name):"未知"); ?>;
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="admin-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username">教务员</span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "__ROOT__/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_course_manage"><em class="icon-book scolor"></em>课程管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">选课管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_select_time_manage"><em class="icon-time scolor"></em>选课时间</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_special_student_manage"><em class="icon-lightbulb scolor"></em>导入优先学生</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_select_setting_manage"><em class="icon-cog scolor"></em>设置</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_direction_manage"><em class="icon-hand-left scolor"></em>方向管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">学分管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_credit_manage"><em class="icon-pencil scolor"></em>学分管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >教师管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_teacher_manage"><em class="icon-user-md scolor"></em>教师管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学生管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_student_manage"><em class="icon-user scolor"></em>学生管理</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">账号管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="__ROOT__/index.php/Admin/admin_account_manage"><em class="icon-cog scolor"></em>账号管理</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container">