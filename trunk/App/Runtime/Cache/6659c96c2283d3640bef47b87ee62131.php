<?php if (!defined('THINK_PATH')) exit();?><div class="student-course-manage container-wrapper">
    <form class="search-form form-inline clearfix" id="student-course-search-form" role="form">
        <span class="form-itme-name">学期</span>
        <select class="input-small" id="student-course-form-semester" name="semester">
            <option value="">请选择</option>
        </select>
    </form>

    <div id="student-course-table-wrapper">
    </div>

    <div class="course-manage-footer clearfix">
        <div class="page-nav">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
    </div>

</div>
<script type="text/javascript" src="__ROOT__/public/js/student/student_course_manage.js"></script>