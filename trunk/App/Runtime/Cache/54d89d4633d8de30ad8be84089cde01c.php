<?php if (!defined('THINK_PATH')) exit();?><div class="credit_wrap">
    <ul id="credit-info" class="u-info-wrap clearfix">
        <li class="u-info-item"><span class="u-info-title">已选学分</span><span class="u-info-value" id="select_credit"></span></li>
        <li class="u-info-item"><span class="u-info-title">已获学分</span><span class="u-info-value" id="pass_credit"></span></li>
        <li class="u-info-item"><span class="u-info-title">所选方向已选学分</span><span class="u-info-value" id="dir_select_credit"></span></li>
        <li class="u-info-item"><span class="u-info-title">所选方向已获学分</span><span class="u-info-value" id="dir_pass_credit"></span></li>
    </ul>
</div>
<script type="text/javascript" src="__ROOT__/public/js/student/credit.js"></script>