<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/font-awesome.min.css">  
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/credit.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_manage.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_schedule.css">
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_common.js"></script>
    
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = {"data":[{"co_id":"2011-2012-1-155058-G10000-1","co_semester":"2011-2012-1","co_number":"155058","co_teacher":"G10000","co_class_num":"1","co_name":"Java语言程序设计","co_grade":"2012","co_direction":"0","co_max_num":"47","co_leading_course":"0","co_introduction":"java","co_credit":"2","co_selected_num":"1","co_selected":"0","link_time":"周二第1,2,5,6节,周五第1,2,5,6节","co_teacher_name":"老师10000","co_direction_name":"无"},{"co_id":"2011-2012-2-155344-G10001-1","co_semester":"2011-2012-2","co_number":"155344","co_teacher":"G10001","co_class_num":"1","co_name":"计算模型与算法技术","co_grade":"2012","co_direction":"1","co_max_num":"43","co_leading_course":"0","co_introduction":"","co_credit":"2.5","co_selected_num":"0","co_selected":"0","link_time":"周二第2,5,6,9,13,14节","co_teacher_name":"老师10001","co_direction_name":"智能软件方向"},{"co_id":"2011-2012-2-155355-G10002-1","co_semester":"2011-2012-2","co_number":"155355","co_teacher":"G10002","co_class_num":"1","co_name":"程序设计方法学","co_grade":"2011","co_direction":"0","co_max_num":"47","co_leading_course":"1","co_introduction":"","co_credit":"2.5","co_selected_num":"2","co_selected":"0","link_time":"周一第1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16节,周五第1,2,3,5,6节","co_teacher_name":"老师10002","co_direction_name":"无"},{"co_id":"2012-2013-1-155152-G10004-1","co_semester":"2012-2013-1","co_number":"155152","co_teacher":"G10004","co_class_num":"1","co_name":"计算机图形学","co_grade":"2011","co_direction":"3","co_max_num":"46","co_leading_course":"0","co_introduction":"","co_credit":"1","co_selected_num":"1","co_selected":"0","link_time":"周四第1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16节","co_teacher_name":"老师10004","co_direction_name":"数字媒体方向"},{"co_id":"2012-2013-1-155328-G10005-1","co_semester":"2012-2013-1","co_number":"155328","co_teacher":"G10005","co_class_num":"1","co_name":"数字媒体处理技术","co_grade":"2010","co_direction":"3","co_max_num":"47","co_leading_course":"1","co_introduction":"","co_credit":"2.5","co_selected_num":"0","co_selected":"0","link_time":"周三第1,2,5,6节","co_teacher_name":"老师10005","co_direction_name":"数字媒体方向"},{"co_id":"2012-2013-1-155353-G10006-1","co_semester":"2012-2013-1","co_number":"155353","co_teacher":"G10006","co_class_num":"1","co_name":"金融业务实务","co_grade":"2011","co_direction":"5","co_max_num":"42","co_leading_course":"0","co_introduction":"","co_credit":"2.5","co_selected_num":"1","co_selected":"0","link_time":"周五第2,5,6,9,13,14节","co_teacher_name":"老师10006","co_direction_name":"金融软件方向"},{"co_id":"2012-2013-1-155359-G10007-1","co_semester":"2012-2013-1","co_number":"155359","co_teacher":"G10007","co_class_num":"1","co_name":"游戏设计与开发","co_grade":"2011","co_direction":"3","co_max_num":"40","co_leading_course":"0","co_introduction":"","co_credit":"3","co_selected_num":"0","co_selected":"0","link_time":"周一第1,5,6,9,11,13,14节,周五第1,5,6,9,11,13,14节","co_teacher_name":"老师10007","co_direction_name":"数字媒体方向"},{"co_id":"2012-2013-2-145042-G10008-1","co_semester":"2012-2013-2","co_number":"145042","co_teacher":"G10008","co_class_num":"1","co_name":"信息系统安全","co_grade":"2011","co_direction":"4","co_max_num":"50","co_leading_course":"0","co_introduction":"","co_credit":"1.5","co_selected_num":"0","co_selected":"0","link_time":"周六第1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16节","co_teacher_name":"老师10008","co_direction_name":"移动计算方向"},{"co_id":"2012-2013-2-155329-G10009-1","co_semester":"2012-2013-2","co_number":"155329","co_teacher":"G10009","co_class_num":"1","co_name":"嵌入式系统软件设计","co_grade":"2011","co_direction":"6","co_max_num":"47","co_leading_course":"0","co_introduction":"","co_credit":"1.5","co_selected_num":"0","co_selected":"0","link_time":"周日第1,2,5,6节","co_teacher_name":"老师10009","co_direction_name":"嵌入式软件方向"}],"selected":[{"co_id":"2011-2012-1-155058-G10000-1","co_semester":"2011-2012-1","co_number":"155058","co_teacher":"G10000","co_class_num":"1","co_name":"Java语言程序设计","co_grade":"2012","co_direction":"0","co_max_num":"47","co_leading_course":"0","co_introduction":"java","co_credit":"2","co_selected_num":"1","co_selected":"0"}]};
        var name = "";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="student-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username" id="header-username"></span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_course_manage"><em class="icon-tasks scolor"></em>课程管理</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/sutdent_course_schedule_manage"><em class="icon-user scolor"></em>个人课表</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >选课</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_direction_first_manage"><em class="icon-book scolor"></em>第一轮选方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_direction_second_manage"><em class="icon-bookmark scolor"></em>第二轮抢方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_course_third_manage"><em class="icon-briefcase scolor"></em>第三轮抢课</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学分统计</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_credit_manage">学分统计</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">个人信息</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_information">个人信息</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"><div class="direction-third container-wrapper">
    <div class="table-wrapper" id="direction-third-selected-table-wrapper"></div>
    <div class="table-wrapper" id="direction-third-table-wrapper"></div>

    <!-- <div class="direction-third-footer clearfix">
        <input type="button" class="direction-third-submit btn btn-sm u-btn" value="提交">
    </div> -->

    <div class="direction-third-footer clearfix">
        <!--
        <div class="page-nav u-fr">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
        -->
        <div class="u-fl">
            <input type="button" class="direction-third-submit btn btn-sm u-btn" value="提交">
        </div>
    </div>
</div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/student/show_direction_third.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始秘密" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>