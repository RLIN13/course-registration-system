<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_info.css">

    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"data":{"co_id":"2011-2012-1-155058-G10000-1","co_semester":"2011-2012-1","co_number":"155058","co_teacher":"老师10000","co_class_num":"1","co_name":"Java语言程序设计","co_grade":"2012","co_direction":"无","co_max_num":"47","co_leading_course":"0","co_introduction":"java","co_credit":"2","co_selected_num":"1","co_selected":"0","link_time":"周二第1,2,5,6节,周五第1,2,5,6节","link_leading_course":""}},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="student-course-information-page">
        <form class="clearfix" id="teacher-course-information-form">
            <div class="form-wrapper clearfix">
                <div class="form-item">
                    <span class="form-item-name">课程ID</span>
                    <span class="form-item-value" id="student-course-information-form-id"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">名称</span>
                    <span class="form-item-value" id="student-course-information-form-name">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学期</span>
                    <span class="form-item-value" id="student-course-information-form-semester">
                </div>
                <div class="form-item">
                    <span class="form-item-name">年级</span>
                    <span class="form-item-value" id="student-course-information-form-grade">
                </div>
                <div class="form-item">
                    <span class="form-item-name">老师</span>
                    <span class="form-item-value" id="student-course-information-form-teacher">
                </div>
                <div class="form-item">
                    <span class="form-item-name">班级数量</span>
                    <span class="form-item-value" id="student-course-information-form-class-num">
                </div>
                <div class="form-item">
                    <span class="form-item-name">方向</span>
                    <span class="form-item-value" id="student-course-information-form-direction">
                </div>
                <div class="form-item">
                    <span class="form-item-name">前导课</span>
                    <span class="form-item-value" id="student-course-information-form-leading-course">
                </div>
                <div class="form-item">
                    <span class="form-item-name">学分</span>
                    <span class="form-item-value" id="student-course-information-form-credit">
                </div>
                <div class="form-item">
                    <span class="form-item-name">最大容量</span>
                    <span class="form-item-value" id="student-course-information-form-max-number">
                </div>
                <div class="form-item">
                    <span class="form-item-name">已选人数</span>
                    <span class="form-item-value" id="student-course-information-form-selected-num">
                </div>
                <div class="form-item">
                    <span class="form-item-name">上课时间</span>
                    <span class="form-item-value" id="student-course-information-form-link-time">
                </div>
                <div class="form-item">
                    <span class="form-item-name">简介</span>
                    <span class="form-item-value" id="student-course-information-form-introduction">
                </div>
            </div>
        </form> 
    </div>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_course_info.js"></script>
</body>
</html>