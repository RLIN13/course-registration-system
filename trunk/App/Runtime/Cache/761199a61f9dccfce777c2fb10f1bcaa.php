<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/teacher/teacher_course_info.css">

    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"data":{"co_id":"2011-2012-1-155058-G10000-1","co_semester":"2011-2012-1","co_number":"155058","co_teacher":"老师10000","co_class_num":"1","co_name":"","co_grade":"0","co_direction":"无","co_max_num":"0","co_leading_course":"0","co_introduction":"java222121","co_credit":"0","co_selected_num":"1","co_selected":"0","link_time":"周二第1,2,5,6节,周五第1,2,5,6节","link_leading_course":""}},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="teacher-course-information-page">
        <form class="clearfix" id="teacher-course-information-form">
         <div class='page-header'><div class='page-header-title'>课程信息（*为可修改）</div></div>
        
            <div class="form-wrapper clearfix">
                <div class="form-item">
                    <span class="form-item-name" id = "a">课程ID</span>
                    <input class="form-item-value" id="teacher-course-information-form-id" disabled="diabled">
                 <!--   <span class="form-item-value" id="teacher-course-information-form-id"></span>   -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">名称</span>
                    <input class="form-item-value" id="teacher-course-information-form-name" disabled="diabled">
              <!--      <span class="form-item-value" id="teacher-course-information-form-name">   -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">学期</span>
                    <input class="form-item-value" id="teacher-course-information-form-semester" disabled="diabled">
              <!--      <span class="form-item-value" id="teacher-course-information-form-semester">  -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">年级</span>
                    <input class="form-item-value" id="teacher-course-information-form-grade" disabled="diabled">
                <!--    <span class="form-item-value" id="teacher-course-information-form-grade">   -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">班级数量</span>
                    <input class="form-item-value" id="teacher-course-information-form-class-num" disabled="diabled">
               <!--     <span class="form-item-value" id="teacher-course-information-form-class-num">   -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">方向</span>
                    <input class="form-item-value" id="teacher-course-information-form-direction" disabled="diabled">
                <!--    <span class="form-item-value" id="teacher-course-information-form-direction"> -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">前导课</span>
                    <input class="form-item-value" id="teacher-course-information-form-leading-course" disabled="diabled">
                    <!-- <span class="form-item-value" id="teacher-course-information-form-leading-course">  -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">学分</span>
                    <input class="form-item-value" id="teacher-course-information-form-credit" disabled="diabled">
                 <!--   <span class="form-item-value" id="teacher-course-information-form-credit">  -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">最大容量</span>
                    <input class="form-item-value" id="teacher-course-information-form-max-number" disabled="diabled">
                  <!--  <span class="form-item-value" id="teacher-course-information-form-max-number">   -->
                </div>
                <div class="form-item">
                    <span class="form-item-name">已选人数</span>
                    <input class="form-item-value" id="teacher-course-information-form-selected-num" disabled="diabled">
                 <!--   <span class="form-item-value" id="teacher-course-information-form-selected-num">   -->
                </div>
                <div class="form-item" style="margin-top:0px;">
                    <span class="form-item-name">简介*</span>
                    <input class="form-item-value" id="teacher-course-information-form-introduction">
                </div>
            </div>
            
            <div class="modify-wrapper">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            <div class="save-wrapper">
                <button class="btn u-btn" id="save-btn">保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_course_data.js"></script>
</body>
</html>