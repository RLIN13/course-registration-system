<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
<!--    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/font-awesome.min.css">  -->
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/credit.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_manage.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_schedule.css">
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_common.js"></script>
    
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = {"semester_list":["2012-2013-2","2012-2013-1","2011-2012-2","2011-2012-1"],"semester":"2012-2013-2","data":[]};
        var name = "";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="student-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username" id="header-username"></span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_course_manage"><i class="icon-home scolor"></i>课程管理</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/sutdent_course_schedule_manage">个人课表</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >选课</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_direction_first_manage">第一轮选方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_direction_second_manage">第二轮抢方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_course_third_manage">第三轮抢课</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学分统计</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_credit_manage">学分统计</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">个人信息</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_information">个人信息</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"><div class="student-course-schedule container-wrapper">
    <form class="search-form form-inline clearfix" id="student-course-schedule-search-form" role="form">
        <span class="form-itme-name">学期</span>
        <select class="input-small" id="student-course-schedule-form-semester" name="semester">
        </select>
    </form>

    <div id="student-course-schedule-table-wrapper">
    </div>
    
</div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_course_schedule.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始秘密" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>