<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="__ROOT__/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="__ROOT__/public/css/common.css">
    <link rel="stylesheet" href="__ROOT__/public/css/teacher/teacher_information.css">
    
    <script type="text/javascript" src="__ROOT__/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="__ROOT__/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = <?php echo ($data); ?>,
            ROOT = "__ROOT__";
    </script>
</head>

<body>
    <div class="single-page">
        <div class='page-header'><div class='page-header-title'>教师信息（*为可修改）</div></div>
        <form class="clearfix" id="single-teacher-form">
            <fieldset disabled>
        
                <div class="form-item">
                    <span class="form-item-name">工号</span>
                    <input class="form-item-value" id="single-teacher-form-id">
                </div>
                <div class="form-item">
                    <span class="form-item-name">姓名*</span>
                    <input class="form-item-value" id="single-teacher-form-name"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">性别*</span>
                    <select class="form-item-value" id="single-teacher-form-sex">
                        <option value="男">男</option>
                        <option value="女">女</option>
                    </select>
                </div>
                <div class="form-item">
                    <span class="form-item-name">电话*</span>
                    <input class="form-item-value" id="single-teacher-form-phone">
                </div>
                <div class="form-item">
                    <span class="form-item-name">邮箱*</span>
                    <input class="form-item-value" id="single-teacher-form-email">
                </div>
                 
                <div> 
                <div class="form-item">
                    <span class="form-item-name">简介*</span>
                    <textarea class="form-item-value" id="single-teacher-form-intro" maxlengh=50 disabled = "true" cols="30" rows="5"></textarea>
                </div>
                </div>
                
                 </fieldset>  
            <div id = "error" style="display:none; color:#F00;"></div>
                 
            
            <div class="modify-wrapper" style="margin-top:50px;">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            
            <div class="save-wrapper" style="margin-top:50px;">
                <button class="btn u-btn" id="save-btn">保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div>

    <script type="text/javascript" src="__ROOT__/public/js/admin/single_teacher_manage.js"></script>
</body>
</html>