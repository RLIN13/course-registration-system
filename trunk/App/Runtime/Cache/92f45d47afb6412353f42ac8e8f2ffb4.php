<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/credit.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_manage.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_course_schedule.css">
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_common.js"></script>
    
    <script type="text/javascript">
        ROOT = "/course_selection_system/trunk";
        var data = {"data":{"0":{"s_id":"201100000001","s_grade":"0","s_class":"0","s_name":"0","s_sex":"","s_phone":"0","s_email":"0","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},"direction_name":"","class_name":"一班"}};
        var name = "";
    </script>
</head>

<body>
    <div class="clearfix" id="wrap">
        <div class="header clearfix" id="student-header">
            <div class="header-logo" id="logo">
            </div>
            <div class="header-nav">
                <ul class="header-nav-list">
                    <li class="header-nav-list-item">
                        <span class="username" id="header-username"></span>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href="#myModal" role="button" data-toggle="modal">修改密码</a>
                    </li>
                    <li class="header-nav-list-item">
                        <a class="header-nav-list-item-link" href = "/course_selection_system/trunk/index.php/Public/loginout">退出</a>
                    </li>
                </ul>
                <span id="header-system-time"></span>
            </div>
        </div>

        <div class="clearfix" id="main">
            <ul class="sidebar-list">
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">课程管理</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_course_manage">课程管理</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/sutdent_course_schedule_manage">个人课表</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >选课</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_direction_first_manage">第一轮选方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_direction_second_manage">第二轮抢方向</a>
                        </li>
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_course_third_manage">第三轮抢课</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);" >学分统计</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_credit_manage">学分统计</a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-list-item">
                    <a class="sidebar-list-item-link" href="javascript:void(0);">个人信息</a>
                    <ul class="sidebar-sublist">
                        <li class="sidebar-sublist-item">
                            <a class="sidebar-sublist-item-link" href="/course_selection_system/trunk/index.php/Student/student_information">个人信息</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <div id="container"><div class="u-info-wrap">
    
        <form class="clearfix" id="single-student-form">
            <div class="form-wrapper clearfix">
                <div class="form-item">
                    <span class="form-item-name">姓名</span>
                    <span class="form-item-value" id="single-student-form-name"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">电话</span>
                    <input class="form-item-value" id="single-student-form-phone" >
                </div>
                <div class="form-item">
                    <span class="form-item-name">学号</span>
                    <span class="form-item-value" id="single-student-form-id"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">邮箱</span>
                    <input class="form-item-value" id="single-student-form-email">
                </div>
                <div class="form-item">
                    <span class="form-item-name">方向</span>
                    <span class="form-item-value" id="single-student-form-direction"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">是否为优秀学生</span>
                    <span class="form-item-value" id="single-student-form-good"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">性别</span>
                    <span class="form-item-value" id="single-student-form-sex"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">是否已进实验室</span>
                    <span class="form-item-value" id="single-student-form-lib"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">年级</span>
                    <span class="form-item-value" id="single-student-form-grade"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">已选学分</span>
                    <span class="form-item-value" id="single-student-form-select-credit"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">班级</span>
                    <span class="form-item-value" id="single-student-form-class"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">已修学分</span>
                    <span class="form-item-value" id="single-student-form-pass-credit"></span>
                </div>
            </div>
           
            <div id = "error" style="display:none; color:#F00;"></div>
           
            <div class="modify-wrapper">
                <button class="btn u-btn" id="modify-btn">修改</button>
            </div>
            <div class="save-wrapper">
                <button class="btn u-btn" id="save-btn">保存</button>
                <button class="btn u-btn" id="reset-btn">重置</button>
                <button class="btn u-btn" id="cancel-btn">取消</button>
            </div>
        </form> 
    </div>

<script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_info.js"></script>            </div><!-- End #container -->
        </div><!-- End #main -->
    </div><!-- End #wrap -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改密码</h4>
      </div>
      <div class="modal-body">
        <div class="control-group">
            <label class="control-label">原始密码</label>
            <div class="controls">
              <input  type="password" placeholder="输入原始秘密" id="oldpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" >确认新密码</label>
            <div class="controls">
              <input type="password" placeholder="输入新密码" id="newpass2">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" id="changePassword-btn">修改</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>