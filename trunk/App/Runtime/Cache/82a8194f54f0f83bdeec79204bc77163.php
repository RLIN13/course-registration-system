<?php if (!defined('THINK_PATH')) exit();?><div class="course-time-manage container-wrapper">
    <div class="table-wrapper" id="course-time-table-wrapper">
        <table class="table table-striped table-hover u-table" id="course-time-manage-table">
            <thead>
                <tr>
                    <th></th>
                    <th>开始时间</th>
                    <th>结束时间</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>第一轮（选方向）</td>
                    <td><input class="time round1-starttime" name="one_start_time" type="text" /></td>
                    <td><input class="time round1-endtime" name="one_end_time" type="text" /></td>
                </tr>
                <tr>
                    <td>第二轮（抢方向）</td>
                    <td><input class="time round2-starttime" name="two_start_time" type="text" /></td>
                    <td><input class="time round2-endtime" name="two_end_time" type="text" /></td>
                </tr>
                <tr>
                    <td>第三轮（抢课）</td>
                    <td><input class="time round3-starttime" name="three_start_time" type="text" /></td>
                    <td><input class="time round3-endtime" name="three_end_time" type="text" /></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="course-time-manage-footer clearfix">
        <input type="button" class="course-time-manage-submit btn btn-sm u-btn course-time-submit" value="提交">
    </div>
</div>
<script type="text/javascript" src="__ROOT__/public/js/admin/select_course_time.js"></script>