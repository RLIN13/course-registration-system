<?php if (!defined('THINK_PATH')) exit();?><div class="student-manage container-wrapper">
    <form class="search-form form-inline clearfix" id="student-search-form" role="form">
        <span class="form-item-name">关键字：</span>
        <input class="col-md-2" id="student-form-keyword" name="keyword" placeholder="学号或姓名" type="text">  
         <span class="form-item-name">年级：</span>
         <select class="input-small" id="student-form-grade" name="grade">
             <option value="">请选择</option>
         </select>
         <span class="form-item-name" style="margin-left:20px;">班级：</span>
        <select class="input-small" id="student-form-class" name="class" >
            <option value="">请选择</option>
        </select>
        <input class="btn btn-sm u-btn" id="student-manage-submit-btn" type="submit" value="搜索"> 
    </form>
      
         
    <div class="table-wrapper" id="student-table-wrapper">
    </div>
    
    <div class="student-manage-footer clearfix">
        <input type="button" class="student-manage-del btn btn-sm u-btn" value="删除" id="delete-student-btn" />
    </div>

    <div class="student-manage-footer clearfix">
        <div class="page-nav">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
        <form id="import-grade-form" enctype="multipart/form-data">
            <span>导入学生：</span>
            <input id="import-student-btn" name="import-student-btn" type="file">
            <button id="import-student-submit">上传</button>
        </form>
    </div>
</div>

<script type="text/javascript" src="__ROOT__/public/js/admin/student_manage.js"></script>