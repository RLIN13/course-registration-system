<?php if (!defined('THINK_PATH')) exit();?><div class="student-course-schedule container-wrapper">
    <form class="search-form form-inline clearfix" id="student-course-schedule-search-form" role="form">
        <span class="form-itme-name">学期</span>
        <select class="input-small" id="student-course-schedule-form-semester" name="semester">
        </select>
    </form>

    <div id="student-course-schedule-table-wrapper">
    </div>
    
</div>
<script type="text/javascript" src="__ROOT__/public/js/student/student_course_schedule.js"></script>