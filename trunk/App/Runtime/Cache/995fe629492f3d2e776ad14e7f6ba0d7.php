<?php if (!defined('THINK_PATH')) exit();?><div class="credit-manage container-wrapper">
    <form class="search-form form-inline clearfix" id="credit-search-form" role="form">
        <span class="form-item-name">关键字：</span>
        <input class="col-md-2" id="credit-form-keyword" name="keyword" placeholder="学生学号或姓名" type="text">
        <span class="form-item-name">年级：</span>
        <select class="input-small" id="credit-form-grade" name="grade">
            <option value=""></option>
        </select>
        <select id="credit-form-credit-type" name="select_credit">
            <option value=""></option>
            <option value="1">已选学分</option>
            <option value="2">已修学分</option>
            <option value="3">方向已选学分</option>
            <option value="4">方向已修学分</option>
        </select>
        <input class="col-md-1" id="credit-form-credit-floor" name="credit_bottom" placeholder="学分下限" type="text">
        <span class="form-item-name">&nbsp;-&nbsp;</span>
        <input class="col-md-1" id="credit-form-credit-upper" name="credit_top" placeholder="学分上限" type="text">
        <input class="btn btn-sm u-btn" id="credit-manage-submit-btn" type="submit" value="搜索"> 
    </form>

    <div class="table-wrapper" id="credit-table-wrapper">
    </div>

    <div class="credit-manage-footer clearfix">
        <div class="page-nav">
            <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
            <input class="page-current" value=""></span>
            <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
            <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
        </div>
        <div>
            <span>导入成绩：</span>
            <input id="import-grade-btn" type="file" name="import-grade-btn">
            <button type="button" id="import-grade-submit">提交</button>
        </div>
    </div>
</div>
<script type="text/javascript" src="__ROOT__/public/js/admin/credit.js"></script>