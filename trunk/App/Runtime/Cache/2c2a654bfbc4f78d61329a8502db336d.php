<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/teacher/teacher_course_student.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"count":"3","page":"3","data":[{"s_id":"201100000021","s_grade":"2011","s_class":"1","s_name":"学生201100000021","s_sex":"男","s_phone":"13205832982","s_email":"201100000021@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000022","s_grade":"2011","s_class":"1","s_name":"学生201100000022","s_sex":"男","s_phone":"13865295085","s_email":"201100000022@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000023","s_grade":"2011","s_class":"1","s_name":"学生201100000023","s_sex":"男","s_phone":"13886724544","s_email":"201100000023@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"1"},{"s_id":"201100000024","s_grade":"2011","s_class":"1","s_name":"学生201100000024","s_sex":"男","s_phone":"13772322559","s_email":"201100000024@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000025","s_grade":"2011","s_class":"1","s_name":"学生201100000025","s_sex":"男","s_phone":"13240407502","s_email":"201100000025@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"0"},{"s_id":"201100000026","s_grade":"2011","s_class":"1","s_name":"学生201100000026","s_sex":"男","s_phone":"13631195620","s_email":"201100000026@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000027","s_grade":"2011","s_class":"1","s_name":"学生201100000027","s_sex":"男","s_phone":"13427593630","s_email":"201100000027@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000028","s_grade":"2011","s_class":"1","s_name":"学生201100000028","s_sex":"男","s_phone":"13042324574","s_email":"201100000028@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"},{"s_id":"201100000029","s_grade":"2011","s_class":"1","s_name":"学生201100000029","s_sex":"女","s_phone":"13626460064","s_email":"201100000029@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"1","s_good":"1"},{"s_id":"201100000030","s_grade":"2011","s_class":"1","s_name":"学生201100000030","s_sex":"男","s_phone":"13186565196","s_email":"201100000030@qq.com","s_direction":"0","s_select_credit":"0","s_pass_credit":"0","s_dir_select_credit":"0","s_dir_pass_credit":"0","s_lib":"0","s_good":"0"}],"co_name":""},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="course-studetn-container">
    <div class='page-header'><div class='page-header-title'>课程学生信息</div></div>
        <div id="course-student-table-wrapper">
        </div>
        
        <div class="teacher-course-student-footer clearfix">
            <div class="page-nav">
                <a href="#" class="page-nav-prev btn btn-sm u-btn">上一页</a>
                <input class="page-current" value=""></span>
                <a href="#" class="page-nav-next btn btn-sm u-btn">下一页</a>
                <a href="" class="page-nav-goto hide"><span class="text">提交</span></a>
            </div>
        </div>
    </div>
<script type="text/javascript" src="/course_selection_system/trunk/public/js/teacher/teacher_course_student.js"></script>
</body>
</html>