<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>专业方向选课系统</title>
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/common.css">
    <link rel="stylesheet" href="/course_selection_system/trunk/public/css/student/student_teacher_info.css">
    
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/course_selection_system/trunk/public/js/common.js"></script>
    
    <script type="text/javascript">
        var data = {"data":{"t_id":"G10000","t_name":"老师10000","t_sex":"女","t_phone":"13238853172","t_email":"G10000@qq.com","t_introduction":""}},
            ROOT = "/course_selection_system/trunk";
    </script>
</head>

<body>
    <div class="student-teacher-information-page">
        <form class="clearfix" id="student-teacher-form">
            <fieldset disabled>
                <div class="form-item">
                    <span class="form-item-name">教工号</span>
                    <span class="form-item-value" id="student-teacher-information-form-id">
                </div>
                <div class="form-item">
                    <span class="form-item-name">姓名</span>
                    <span class="form-item-value" id="student-teacher-information-form-name"></span>
                </div>
                <div class="form-item">
                    <span class="form-item-name">性别</span>
                    <span class="form-item-value" id="student-teacher-information-form-sex">
                </div>
                <div class="form-item">
                    <span class="form-item-name">电话</span>
                    <span class="form-item-value" id="student-teacher-information-form-phone">
                </div>
                <div class="form-item">
                    <span class="form-item-name">邮箱</span>
                    <span class="form-item-value" id="student-teacher-information-form-email">
                </div>
                <div> 
                <div class="form-item">
                    <span class="form-item-name">简介</span>
                    <span class="form-item-value" id="student-teacher-information-form-intro"></span>
                </div>
                </div>                  
        </form> 
    </div>

    <script type="text/javascript" src="/course_selection_system/trunk/public/js/student/student_teacher_info.js"></script>
</body>
</html>