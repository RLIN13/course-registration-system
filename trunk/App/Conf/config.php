<?php
return array(
    //调试
//    'CONSOLE_ON' => true,
//    'CONSOLE_LABEL_ON' => true,

    //rbac
    'USER_AUTH_ON' => false,    //调试状态关闭，部署需要打开
    'USER_AUTH_KEY' => 'authId',
    'ADMIN_AUTH_KEY' => 'administrator',
    'USER_AUTH_MODEL' => 'rbac_user',
    'AUTH_PWD_ENCODER' => 'md5',
    'USER_AUTH_GATEWAY' => '/Public/login',
    'NOT_AUTH_MODULE' => 'Base,Public,Rbac,Test',
    'GUEST_AUTH_ON' => false,
    'RBAC_ROLE_TABLE' => 'rbac_role',
    'RBAC_USER_TABLE' => 'rbac_role_user',
    'RBAC_ACCESS_TABLE' => 'rbac_access',
    'RBAC_NODE_TABLE' => 'rbac_node',
    'DB_LIKE_FIELDS' => 'title|remark',

    //trace
    'SHOW_PAGE_TRACE' => true,
//    'SHOW_RUN_TIME' => true,
//    'SHOW_ADV_TIME' => true,
//    'SHOW_DB_TIMES' => true,
//    'SHOW_CACHE_TIMES' => true,
//    'SHOW_USE_MEM' => true,
//    'SHOW_ERROR_MSG' => true,

    //db
    'DB_HOST' => 'localhost',
    'DB_NAME' => 'css',
    'DB_USER' => 'root',
    'DB_PWD' => '',
    'DB_PORT' => '3306',
    'DB_PREFIX' => '', // 数据库表前缀
    'DB_SUFFIX' => '', // 数据库表后缀


    //角色
    'ROLE_NONE' => 0,
    'ROLE_ADMIN' => 1,
    'ROLE_TEACHER' => 2,
    'ROLE_STUDENT' => 3,

    //默认密码
    'DEFAULT_PWD' => '123456',

    //session中的username
    'SESSION_USERNAME' => 'username',
    'SESSION_NAME' => 'name',
    
    'REDIS_ON' => FALSE,
    'REDIS_HOST' => '127.0.0.1',
    'REDIS_PORT' => 6379,
);
