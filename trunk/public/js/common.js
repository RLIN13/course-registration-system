$(document).ready(function() {
	init();
});

function init() {
	var current_date = new Date(),
		current_year = current_date.getFullYear();
		current_month = current_date.getMonth() + 1;
		current_day = current_date.getDate();
		current_weekday = current_date.getDay();

	switch(current_weekday) {
		case 0: current_weekday = "星期日"; break;
		case 1: current_weekday = "星期一"; break;
		case 2: current_weekday = "星期二"; break;
		case 3: current_weekday = "星期三"; break;
		case 4: current_weekday = "星期四"; break;
		case 5: current_weekday = "星期五"; break;
		case 6: current_weekday = "星期六"; break;
		default: current_weekday = "星期日"; break;
	}

	$("#header-system-time").text(current_year + "年" + current_month + "月" + current_day + "日 " + current_weekday);

	$.each($(".sidebar-sublist-item-link"), function() {
		var href_temp = $(this)[0].href,
			key_temp = href_temp.indexOf('?') < 0 ? href_temp.substr(href_temp.lastIndexOf('/') + 1) : href_temp.substr(href_temp.lastIndexOf('/') + 1, href_temp.indexOf('?'));

		if(getUrlParam().current_menu == key_temp) {
			$(this).parent().addClass("sidebar-sublist-item-active");
			$(this).parent().parent().slideDown();
			$(this).parent().parent().parent().addClass("sidebar-list-item-active");
		}
	});

	$(".sidebar-list-item-link").click(function() {
		if(!$(this).parent().hasClass("sidebar-list-item-active")) {
			var item_array = $(this).parent().parent().find(".sidebar-list-item");

			for(var i = 0; i < item_array.length; i++) {
				if($(item_array[i]).hasClass("sidebar-list-item-active")) {
					$(item_array[i]).find(".sidebar-sublist").slideUp();
					$(item_array[i]).removeClass("sidebar-list-item-active");
				}
			}
		}

		$(this).siblings(".sidebar-sublist").slideToggle();
		$(this).parent().toggleClass("sidebar-list-item-active");
	});
}

/**
 * 创建内容列表
 * @param row_names 列名
 * @param data 表内数据
 * @target target 目标，将表格添加到该元素上
 */
function createTable(row_names, data, target, parent_node) {
	var data_length = data.length,
		row_names_length = row_names.length;
		//table = "<table class='table table-striped table-hover u-table' id='"  + target + "' ><thead><tr>";
	    
		table = "<table class='orders-table table' id='"  + target + "' ><thead><tr>";
	for (i = 0; i < row_names_length; i++) {
        table += '<th>' + row_names[i] + '</th>';
    }
    table +="</tr></thead><tbody>";

    for(var i = 0; i < data_length; i++) {
    	table += "<tr>";
    	for(d in data[i]) {
    		table += "<td>" + data[i][d] + "</td>";
    	}
    	table += "</tr>";
    }
    table +="</tbody></table>";

//    parent_node.append(span);
    parent_node.append(table);
   
  
}

/* 获取URL参数
* @return
*/

function getUrlParam() {
	var url = location.href,
		search_str = location.search;
		slash_pos = url.lastIndexOf('/'),
		question_mark_pos = url.indexOf('?') > -1 ? url.indexOf('?') : url.length,
		current_menu = url.substring(slash_pos + 1, question_mark_pos),
		url_param = {},
		search_param = {};

	if(search_str.length > 0) {
		search_str = search_str.substr(1);

		var params = search_str.split('&');

		for(var i = 0; i < params.length; i++) {
			search_param[params[i].split('=')[0]] = params[i].split('=')[1];
		}
	}

	url_param = {
		current_menu: current_menu,
		search_param: search_param
	}

	return url_param;
}

/*checkbox全选*/
function checkall() {
	var dom = {
		checkall: $(".checkall")
	}
	dom.checkall.on("change",function(){
		var checkbox_list = dom.checkall.parents(".table").find("[type='checkbox']");
		dom.checkall.is(':checked') ? checkbox_list.prop("checked",true) : checkbox_list.prop("checked",false);
	});
};



/**
* 上传文件
* @param button 上传事件按钮
* @param fileInput 上传文件input
* @param url 后台处理地址
*/
function initAjaxFileUpload(button, fileInput, url) {
    $('#' + button).unbind('click');
    $('#' + button)
        .ajaxStart(function () {
            $(this).addClass('disabled');
        })
        .ajaxComplete(function () {
            $(this).removeClass('disabled');
        })
        .click(function () {
            if ($('#' + fileInput).val() == '') {
                alert("请选择文件！");
            }
            else {
                $.ajaxFileUpload
                (
                    {
                        url:url,
                        secureuri:false,
                        fileElementId:fileInput,
                        dataType:'json',
                        success:function (data) {
                            if (data.status) {
                                alert("导入成功！");
                            } else {
                                alert(data.info);
                            }
                            	
                        },
                        error:function (data, status, e) {
                            alert(e);
                        }
                    }
                );
            }
        });
}

/**
 * 设置上一页和下一页的URL
 * @param prevBtn 上一页按钮
 * @param nextBtn 下一页按钮
 */
function setNextpageUrl(prevBtn, nextBtn, currentInput) {
	var nowUrl = window.location.href+'',
		nowUrlObj = urlStr2Obj(nowUrl),
		nowPageNum = nowUrlObj["para"]["page"]||"1",
		prevNum = parseInt(nowPageNum,10) - 1,
		nextNum = parseInt(nowPageNum,10) + 1,
		prevUrl = '',
		nextUrl = '';

	currentInput.val(nowPageNum + "/" + data.count);
	nowUrlObj["para"]["page"] = prevNum;
	prevUrl = urlObj2Str(nowUrlObj);
	nowUrlObj["para"]["page"] = nextNum;
	nextUrl = urlObj2Str(nowUrlObj);
	prevBtn.attr("href",prevUrl);
	nextBtn.attr("href",nextUrl);
	if(nowPageNum == "1"){
		prevBtn.addClass("disabled");
	}
	if(nowPageNum == data.count){
		nextBtn.addClass("disabled");
	}
}

/**
 * 把字符串链接转为对象
 * @param url 字符串链接
 * @return urlObj 字符串转换成的对象
 */
function urlStr2Obj(url) {
	var urlObj = {};
	urlObj["addr"] = url.split("?")[0];
	urlObj["para"] = {};
	if(url.split("?")[1]){
		for(var i=0,para=url.split("?")[1].split("&"),length=para.length;i<length;i++){
			var name = para[i].split("=")[0],
				value = para[i].split("=")[1];
			urlObj["para"][name] = value;
		}
	}
	return urlObj;
}

/**
 * 把对象链接转为字符串
 * @param urlObj 字符串转换成的对象
 * @return url 字符串链接
 */
function urlObj2Str(urlObj) {
	var urlStr = '' + urlObj["addr"];

	if(urlObj["para"]){
		urlStr += "?";
	}
	for(x in urlObj["para"]) {
		urlStr += x + "=" + urlObj["para"][x] + "&";
	}
	urlStr = urlStr.substring(0,urlStr.length-1);
	return urlStr;
}

/**
 * 绑定输入页数的失去焦点事件
 * @param inputObj 输入数字的输入框对象
 * @param gotoBtn 隐藏的提交按钮
 */
function gotoPage(inputObj,gotoBtn) {
	inputObj.on("blur",function(){
		var gotoPageNum = parseInt(inputObj.val().split("/")[0]) || 1,
		 	nowUrl = window.location.href+'',
			nowUrlObj = urlStr2Obj(nowUrl);

		nowUrlObj["para"]["page"] = gotoPageNum;
		gotoUrl = urlObj2Str(nowUrlObj);
		gotoBtn.attr("href",gotoUrl);
		gotoBtn.children(".text").click();
	});
}

function isEmail(target){
	var ele = target.val();
    var reg_email = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
            if(!reg_email.test(ele))
            {    
		
                 $("#error").css("display","block");
				 $("#error").html("请输入合法的邮箱名");
                return false;
           }
		   else
		   {     $("#error").css("display","none");
		          return true;
		   }
}

function isPhone(target){
	var ele = target.val();
    var reg_cell = /^(13+\d{9})|(159+\d{8})|(153+\d{8})$/;
	var reg_tel  = /^\d{7,8}$/;
            if((!reg_cell.test(ele))&&(!reg_tel.test(ele)))
            {
                 $("#error").css("display","block");
				 $("#error").html("请输入合法的电话号码");
                return false;
           }
		   else
		   {     $("#error").css("display","none");
		        return true;
		   }
}
