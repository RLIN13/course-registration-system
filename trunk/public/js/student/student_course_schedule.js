$(document).ready(function() {
    initCourseScheduleSemesterForm();
    initCourseScheduleTable();

    var semester = data.semester_list,
        url_param = getUrlParam();
    for(var i = 0; i < semester.length; i++) {
        if(url_param.search_param.semester && url_param.search_param.semester == semester[i]) {
            $("#student-course-schedule-form-semester").get(0).selectedIndex = i;
        }
    }

    $("#student-course-schedule-form-semester").change(function(){ 
        $("#student-course-schedule-search-form").submit();
    });
});

function initCourseScheduleSemesterForm(){
    var semester = data.semester_list;
    for(var i = 0; i < semester.length; i++) {
        $("#student-course-schedule-form-semester").append("<option value='" + semester[i] + "'>" + semester[i] + "</option>");
    }
}

function initCourseScheduleTable() {
    var span ="<div class='page-header'><div class='page-header-title'>个人课表</div></div>";
    $("#student-course-schedule-table-wrapper").append(span);

    var row_names = ["时间","星期一", "星期二","星期三", "星期四", "星期五","星期六","星期日"],
        colume_names = ["第一节", "第二节","第三节", "第四节", "第五节","第六节","第七节","第八节","第九节","第十节","第十一节","第十二节"],
        schedule_row_data = [],
        target = "student-course-schedule-manage-table";

        schedule_data = new Array();  
        for(var k=0;k<12;k++){     
            schedule_data[k]=new Array();  
            for(var j=0;j<7;j++){   
                schedule_data[k][j]="";    
            }
        }
        for (var i = 0; i < data.data.length; i++) {
            for (var day in data.data[i].link_time) {
                var bits = [];
                var dividend = data.data[i].link_time[day];
                var remainder = 0;
                while (dividend >= 2) {
                    remainder = dividend % 2;
                    bits.push(remainder);
                    dividend = (dividend - remainder) / 2;
                }
                bits.push(dividend);
                //bits.reverse();

                for (var j = 0; j < bits.length; j++) {
                    if (bits[j]) {
                        schedule_data[j][day-1] = data.data[i].co_name;
                    };
                };
            };
        };

        for (var i = 0; i < colume_names.length; i++) {
            schedule_row_data.push({
                time_number: colume_names[i],
                first: schedule_data[i][0],
                second: schedule_data[i][1],
                third: schedule_data[i][2],
                forth: schedule_data[i][3],
                fifth: schedule_data[i][4],
                sixth: schedule_data[i][5],
                seventh: schedule_data[i][6],
            });
        };
        createTable(row_names, schedule_row_data, target, $("#student-course-schedule-table-wrapper"));
}