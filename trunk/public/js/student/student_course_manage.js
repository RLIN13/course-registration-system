$(document).ready(function() {
	initCourseManagerTable();
	initCourseSemester();
	setNextpageUrl($(".page-nav-prev"),$(".page-nav-next"),$(".page-current"));
	gotoPage($(".page-current"),$(".page-nav-goto"));


	var semester = data.semester_list,
		url_param = getUrlParam();
	for(var i = 0; i < semester.length; i++) {
		if(url_param.search_param.semester && url_param.search_param.semester == semester[i]) {
			$("#student-course-form-semester").get(0).selectedIndex = i+1;
		}
	}

	$("#student-course-form-semester").change(function(){ 
		$("#student-course-search-form").submit();
	});
});

function initCourseSemester(){
	var semester = data.semester_list;
	for(var i = 0; i < semester.length; i++) {
		$("#student-course-form-semester").append("<option value='" + semester[i] + "'>" + semester[i] + "</option>");
	}
}

function initCourseManagerTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>课程列表</div></div>";
	$("#student-course-table-wrapper").append(span);
	
	var row_names = ["学期", "名称","学分", "方向", "上课时间","老师"],
		student_course_data = [],
		target = "student-course-manage-table";
		course_data = data.data;

	for(var i = 0; i < data.data.length; i++) {
		student_course_data.push({
			semester: data.data[i].co_semester,
			name: "<a class='u-table-link' href='./student_single_course_data/id/" + data.data[i].co_id + "' target='_blank' title='查看" + data.data[i].co_name + "的课程信息'>" + data.data[i].co_name + "</a>",
			credit: data.data[i].co_credit,
			direction: data.data[i].co_direction,
			time: data.data[i].link_time_string,
			teacher: "<a class='u-table-link' href='./student_single_teacher_info/id/" + data.data[i].co_teacher + "' target='_blank' title='查看" + data.data[i].co_teacher_name + "的学生名单'>" + data.data[i].co_teacher_name + "</a>",
		});
	}
	createTable(row_names, student_course_data, target, $("#student-course-table-wrapper"));


}