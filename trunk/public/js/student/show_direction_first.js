$(document).ready(function() {
    console.log(data);
	initDirectionFirst();
});

function initDirectionFirst() {
	var	span ="<div class='page-header'><div class='page-header-title'>填写志愿</div></div>";
	$("#direction-first-table-wrapper").prepend(span);
	var direction = data.data,
		direction_length = direction.length,
		option = '',
		direction_selected = data.selected;

	for(var i=0;i<direction_length;i++){
		option = '<option data-id=' + direction[i]['d_id'] + '>' + direction[i]['d_name'] + '</option>'
		$("#direction-first-wishone").append(option);
		$("#direction-first-wishtwo").append(option);
		$("#direction-first-wishthree").append(option);
	}

	for(var i = 0; i < direction_length; i++) {
		var d_id = direction[i]['d_id'];
		if(d_id == direction_selected['1']) {
			$("#direction-first-wishone").get(0).selectedIndex = i+1;
		}
		if(d_id == direction_selected['2']) {
			$("#direction-first-wishtwo").get(0).selectedIndex = i+1;
		}
		if(d_id == direction_selected['3']) {
			$("#direction-first-wishthree").get(0).selectedIndex = i+1;
		}
	}
}

$(".direction-first-submit").on("click",function(){
	var wishone = $("#direction-first-wishone option:selected").attr('data-id'),
		wishtwo = $("#direction-first-wishtwo option:selected").attr('data-id'),
		wishthree = $("#direction-first-wishthree option:selected").attr('data-id');
		
	if((wishone==wishtwo&&wishone!='-1')||(wishone==wishthree&&wishone!='-1')||(wishtwo==wishthree&&wishtwo!='-1')){
		alert("有方向冲突");
		return false;
	}

	$.post(ROOT + "/index.php/StudentData/student_select_direction_first_a",
		{
			"direction_id_1": wishone,
			"direction_id_2": wishtwo,
			"direction_id_3": wishthree
		},function(response) {
			if(response.status == "1"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert(response.info);
			}
		},"json");
});
