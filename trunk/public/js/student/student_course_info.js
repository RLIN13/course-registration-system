$(document).ready(function() {
	initCourseInfoForm();
});

function initCourseInfoForm() {
	var	span ="<div class='page-header'><div class='page-header-title'>课程信息</div></div>";
	$("#teacher-course-information-form").prepend(span);

	var course_data = data.data;
	$("#student-course-information-form-id").text(course_data.co_id);
	$("#student-course-information-form-name").text(course_data.co_name);
	$("#student-course-information-form-semester").text(course_data.co_semester);
	$("#student-course-information-form-grade").text(course_data.co_grade);
	$("#student-course-information-form-teacher").text(course_data.co_teacher);
	$("#student-course-information-form-class-num").text(course_data.co_class_num);
	$("#student-course-information-form-direction").text(course_data.co_direction);
	$("#student-course-information-form-leading-course").text(course_data.co_leading_course);
	$("#student-course-information-form-credit").text(course_data.co_credit);
	$("#student-course-information-form-max-number").text(course_data.co_max_num);
	$("#student-course-information-form-selected-num").text(course_data.co_selected_num);
	$("#student-course-information-form-link-time").text(course_data.link_time);
	$("#student-course-information-form-introduction").text(course_data.co_introduction);
}