$(document).ready(function() {
	initTeacherInfoForm();
});
function initTeacherInfoForm() {
	var	span ="<div class='page-header'><div class='page-header-title'>教师信息</div></div>";
	$("#student-teacher-form").prepend(span);

	var teacher_data = data.data;
	$("#student-teacher-information-form-id").text(teacher_data.t_id);
	$("#student-teacher-information-form-name").text(teacher_data.t_name);
	$("#student-teacher-information-form-sex").text(teacher_data.t_sex);
	$("#student-teacher-information-form-phone").text(teacher_data.t_phone);
	$("#student-teacher-information-form-email").text(teacher_data.t_email);
	$("#student-teacher-information-form-intro").text(teacher_data.t_introduction);
}