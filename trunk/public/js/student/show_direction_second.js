$(document).ready(function() {
	initDirectionSelected();
	initDirectionSecondTable();
        console.log(data);
});

$(".direction-second-submit").on("click",function(){
	var selected_direction = $("input[name='direction_second']:checked").val();

	$.post(ROOT + "/index.php/StudentData/student_select_direction_second_a",
		{
			"selected_direction": selected_direction
		},function(response) {
			if(response.status == "1"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert(response.info);
			}
		},"json");
});

function initDirectionSecondTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>可选方向列表</div></div>";
	$("#direction-selected-table-wrapper").append(span);
	var row_names = ['', '方向', '最大容量', '已选人数', '方向简介'],
		direction_second_data = [],
		target = "direction-second-table",
		direction_second_array = data.data,
		direction_second_array_length = direction_second_array.length;

	for(var i = 0; i < direction_second_array_length; i++) {
		direction_second_data.push({
			'checkbox': '<input type="radio" name="direction_second" value="' + direction_second_array[i].d_id + '">',
			"name": direction_second_array[i].d_name,
			'max_num': direction_second_array[i].d_max_num,
			'selected_num': direction_second_array[i].d_selected_num,
			'introduction': direction_second_array[i].d_introduction
		});
	}

	createTable(row_names, direction_second_data, target, $("#direction-second-table-wrapper"));
}

var initDirectionSelected = function(){
	var row_names = '',
		direction_selected_data = [],
		target = "direction-selected-table",
		direction_selected = data.selected;

	direction_selected_data.push({
		"name": direction_selected['d_name'],
		"delete": '<input type="button" class="direction_selected_delete btn btn-sm u-btn" value="退选" data-id="'+direction_selected['d_id']+'">'
	});

	if(direction_selected){
		createTable(row_names, direction_selected_data, target, $("#direction-selected-table-wrapper"));
		var	span ="<div class='page-header'><div class='page-header-title'>已选方向列表</div></div>";
	    $("#direction-selected-table-wrapper").prepend(span);
	}

	$(".direction_selected_delete").on("click",function(){
	var d_id = $(this).attr("data-id");

	$.post(ROOT + "/index.php/StudentData/student_cancle_direction_second_a",
		{
			"d_id": d_id
		},function(response) {
			if(response.status == "1"){
				alert("退选成功");
				window.location.reload();
			}else{
				alert(response.info);
			}
		},"json");
});
}