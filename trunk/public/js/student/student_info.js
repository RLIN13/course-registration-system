$(window).resize(function(){
   var form_width = $("#single-student-form").width();
	$(".page-header").width(form_width);
});

$(document).ready(function() {
	initStudentForm();
	/*
    var if_phone_right =isPhone($("#single-student-form-phone"));
	var if_email_right =isEmail($("#single-student-form-email"));
	
	if (if_phone_right == true){
		isEmail($("#single-student-form-email"));
	}
	
	if (if_email_right == true){
		isPhone($("#single-student-form-phone"));
	}
	*/

	$("#single-student-form input").attr("disabled", "disabled");
	$("#single-student-form select").attr("disabled", "disabled");
	$("#single-student-form-direction").css("background-color", "#e8e8e8");
	

	$("#modify-btn").click(function() {
		$("#single-student-form input").removeAttr("disabled");
		$("#single-student-form select").removeAttr("disabled");
		$(".before-modify-wrapper").css("display", "block");
		$(".modify-wrapper").css("display", "none");
		$(".save-wrapper").css("display", "block");
		$("#single-student-form-phone").css("background-color", "#FFF");
		$("#single-student-form-email").css("background-color", "#FFF");
		return false;
	});

	$("#reset-btn").click(function() {
		initStudentForm();
		return false;
	});

	$("#save-btn").click(function() {
		$.ajax({
			url: ROOT + "/index.php/StudentData/student_update_student_info_a",
			data: {
				s_phone:      $("#single-student-form-phone").val(),
				s_email:      $("#single-student-form-email").val(),
			},
			type: "POST",
			dataType: "json",
			success: function(data) {
				location.reload();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest.status);
				console.log(XMLHttpRequest.readyState);
				console.log(textStatus);
			}
		});
		
		
		$("#single-student-form fieldset").attr("disabled", "disabled");
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("#single-student-form-phone").css("background-color", "#e8e8e8");
		$("#single-student-form-email").css("background-color", "#e8e8e8");
		

		return false;
	});

	$("#cancel-btn").click(function() {
		initStudentForm();
		$("#single-student-form input").attr("disabled", "disabled");
		$("#single-student-form select").attr("disabled", "disabled");
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("#single-student-form-phone").css("background-color", "#e8e8e8");
		$("#single-student-form-email").css("background-color", "#e8e8e8");
		/*isEmail($("#single-student-form-email"));
		
	    if (if_phone_right == true){
		  isEmail($("#single-student-form-email"));
	    }
		
		if (if_email_right == true){
		isPhone($("#single-student-form-phone"));
	    }*/
		
		return false;
	})

	$("#single-student-form-grade").change(function() {
		changeClassOption();
	});
});

    $("#single-student-form-email").blur(function(){
		isEmail($("#single-student-form-email"));
	})
	
	$("#single-student-form-phone").blur(function(){
		isPhone($("#single-student-form-phone"));
	})




function initStudentForm() {
//	var	span ="<div class='page-header'><div class='page-header-title'>个人信息</div></div>";
//	$(".u-info-wrap").prepend(span);
	var form_width = $("#single-student-form").width();
	$(".page-header").width(form_width);

	var student_data = data.data[0];
	var class_data = data.data.class_name;
	var direction_data = data.data.direction_name;
	
	$("#single-student-form-name").html(student_data.s_name);
	$("#single-student-form-id").html(student_data.s_id);
	$("#single-student-form-phone").val(student_data.s_phone);
	$("#single-student-form-email").val(student_data.s_email);
	$("#single-student-form-select-credit").html(student_data.s_select_credit);
	$("#single-student-form-grade").html(student_data.s_grade);
	$("#single-student-form-pass-credit").html(student_data.s_pass_credit);
	$("#single-student-form-class").html(class_data);
	$("#single-student-form-direction").html(direction_data);


	if(student_data.s_sex == "男") {
		$("#single-student-form-sex").html("男");
	} else {
		$("#single-student-form-sex").html("女");
	}

	if(student_data.s_good == '1') {
		$("#single-student-form-good").html("是");
	} else {
		$("#single-student-form-good").html("否");
	}

	if(student_data.s_lib == '1') {
		$("#single-student-form-lib").html("是");
	} else {
		$("#single-student-form-lib").html("否");
	}


}


