$(document).ready(function() {
	initDirectionThirdSelected();
	initDirectionThirdTable();
	checkall();

    $(".direction_third_selected_delete").on("click",function(){
	var co_id = $(this).attr("data-id");

	$.post(ROOT + "/index.php/StudentData/student_cancle_course_third_a",
		{
			"co_id": co_id
		},function(response) {
                    console.log(response);
			if(response.status == "1"){
				alert(response.info);
				window.location.reload();
			}else{
				alert(response.info);
			}
		},"json");
});
});

function initDirectionThirdSelected(){
	var row_names = '',
		direction_third_selected_data = [],
		target = "direction-third-selected-table",
		direction_third_selected = data['selected'],
		direction_third_selected_length = direction_third_selected.length;

	for(var i = 0; i < direction_third_selected_length; i++) {
		direction_third_selected_data.push({
			"name": direction_third_selected[i].co_name,
			"delete": '<input type="button" class="direction_third_selected_delete btn btn-sm u-btn" value="退选" data-id="'+direction_third_selected[i]['co_id']+'">'
		});
	}

	if(direction_third_selected){
		createTable(row_names, direction_third_selected_data, target, $("#direction-third-selected-table-wrapper"));
		$("#direction-third-selected-table").attr("class","table table-striped table-hover u-table");
	}	
}

function initDirectionThirdTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>课程列表</div></div>";
	$("#direction-third-table-wrapper").append(span);
	var row_names = ['',"课程名称", "上课时间", "老师", "方向", "余量"],
		direction_third_data = [],
		target = "direction-third-table",
		direction_third_array = data.data,
		direction_third_array_length = direction_third_array.length;

	for(var i = 0; i < direction_third_array_length; i++) {
		direction_third_data.push({
			'checkbox': '<input type="radio" name="direction_third" value="' + direction_third_array[i].co_id + '">',
			"name": direction_third_array[i].co_name,
			'time': direction_third_array[i].link_time,
			'teacher': direction_third_array[i].co_teacher_name,
			'direction': direction_third_array[i].co_direction_name||'',
			'max_num': direction_third_array[i].co_max_num - direction_third_array[i].co_selected_num
		});
	}

	createTable(row_names, direction_third_data, target, $("#direction-third-table-wrapper"));
}

$(".direction-third-submit").on("click",function(){
	var co_id = $("input[name='direction_third']:checked").val();

//	$.post(ROOT + "/index.php/StudentData/student_select_course_third_a",
//		{
//			"co_id": co_id
//		},function(response) {
//                    alert("111");
//			if(response.status == "1"){
//				alert("提交成功");
//				window.location.reload();
//			}else{
//				alert(response.info);
//			}
//		},"json");
                
        $.ajax({
				url: ROOT + "/index.php/StudentData/student_select_course_third_a",
				data: {
					"co_id": co_id
				},
				type: "POST",
				dataType: "json",
				success: function(response) {
					 if(response.status == "1"){
                                            alert("选课成功");
                                            window.location.reload();
                                        }else{
                                                alert(response.info);
                                    }
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				   console.log(XMLHttpRequest.status);
                                   console.log(XMLHttpRequest.readyState);
                                   console.log(textStatus);
				}
			});
});

