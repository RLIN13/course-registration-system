$(document).ready(function() {
	$("title").html(data.co_name+"课程的学生名单");
	courseStudentTable();
	setNextpageUrl($(".page-nav-prev"),$(".page-nav-next"),$(".page-current"));
	gotoPage($(".page-current"),$(".page-nav-goto"));
});

function courseStudentTable() {
	var row_names = ['学号', '年级', '班级', '姓名',  '性别'],
		course_student_data = [],
		target = "course-student-table",
		data_length = data.data.length;

	for(var i = 0; i < data_length; i++) {
		course_student_data.push({
			id: data.data[i].s_id,
			name: data.data[i].s_grade,
			sClass: data.data[i].s_class,
			grade: data.data[i].s_name,
			sex: data.data[i].s_sex
		});
	}
	createTable(row_names, course_student_data, target, $("#course-student-table-wrapper"));

}
