$(document).ready(function() {
	initCourseInfoForm();

	$("title").html(data.data.co_name);
	$("teacher-course-information-title").text(data.data.co_name);
	$("#teacher-course-information-form input").attr("disabled", "disabled");
	$("#teacher-course-information-form select").attr("disabled", "disabled");

	$("#modify-btn").click(function() {
	//	$("#teacher-course-information-form input").removeAttr("disabled");
		$("#teacher-course-information-form select").removeAttr("disabled");
		$("#teacher-course-information-form-introduction").removeAttr("disabled");
		$(".before-modify-wrapper").css("display", "block");
		$(".modify-wrapper").css("display", "none");
		$(".save-wrapper").css("display", "block");
		return false;
	});

	$("#reset-btn").click(function() {
		initCourseInfoForm();
		return false;
	});

	$("#save-btn").click(function() {
		$.ajax({
			url: ROOT + "/index.php/TeacherData/teacher_modify_course_data_a",
			data: {
				co_id: data.data.co_id,
				co_introduction:$("#teacher-course-information-form-introduction").val()
			},
			type: "POST",
			dataType: "json",
			success: function(data) {
				console.log(data);
				location.reload();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest.status);
				console.log(XMLHttpRequest.readyState);
				console.log(textStatus);
			}
		});
		
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("#teacher-course-information-form-name").attr("disabled", "disabled");
		$("#teacher-course-information-form-semester").attr("disabled", "disabled");
		$("#teacher-course-information-form-grade").attr("disabled", "disabled");
		$("#teacher-course-information-form-class-num").attr("disabled", "disabled");
		$("#teacher-course-information-form-direction").attr("disabled", "disabled");
		$("#teacher-course-information-form-leading-course").attr("disabled", "disabled");
		$("#teacher-course-information-form-credit").attr("disabled", "disabled");
		$("#teacher-course-information-form-max-number").attr("disabled", "disabled");
		$("#teacher-course-information-form-selected-num").attr("disabled", "disabled");
		$("#teacher-course-information-form-introduction").attr("disabled", "disabled");
		return false;
	});
});

function initCourseInfoForm() {
/*	var a = $("#a").width();
	var b = $("#teacher-course-information-form-id").width();  */
    
//    $("#teacher-course-information-form-introduction").width(a+b);  
    console.log(data);
	var course_data = data.data;
	$("#teacher-course-information-form-id").val(course_data.co_id);
	$("#teacher-course-information-form-name").val(course_data.co_name);
	$("#teacher-course-information-form-semester").val(course_data.co_semester);
	$("#teacher-course-information-form-grade").val(course_data.co_grade);
	$("#teacher-course-information-form-class-num").val(course_data.co_class_num);
	$("#teacher-course-information-form-direction").val(course_data.co_direction);
	$("#teacher-course-information-form-leading-course").val(course_data.co_leading_course);
	$("#teacher-course-information-form-credit").val(course_data.co_credit);
	$("#teacher-course-information-form-max-number").val(course_data.co_max_num);
	$("#teacher-course-information-form-selected-num").val(course_data.co_selected_num);
	$("#teacher-course-information-form-introduction").val(course_data.co_introduction);
}