$(document).ready(function() {
	initCourseManagerTable();
});

function initCourseManagerTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>课程管理</div></div>";
	$("#teacher-course-table-wrapper").prepend(span);
	var row_names = ["学期", "名称","学分", "年级", "方向", "前导课",  "最大容量", "已选人数", "学生名单"],
		teacher_course_data = [],
		target = "teacher-course-manage-table";
		course_data = data.data;

	for(var i = 0; i < data.data.length; i++) {
		teacher_course_data.push({
			semester: data.data[i].co_semester,
			name: "<a class='u-table-link' href='./teacher_course_data/co_id/" + data.data[i].co_id + "' target='_blank' title='查看" + data.data[i].co_name + "的课程信息'>" + data.data[i].co_name + "</a>",
			credit: data.data[i].co_credit,
			grade: data.data[i].co_grade,
			direction: data.data[i].co_direction,
			leading_course: data.data[i].co_leading_course,
			max_num: data.data[i].co_max_num,
			selected: data.data[i].co_selected,
			namelist: "<a class='u-table-link' href='./teacher_course_student/co_id/" + data.data[i].co_id + "' target='_blank' title='查看" + data.data[i].co_name + "的学生名单'>" + "查看" + "</a>",
		});
	}
	createTable(row_names, teacher_course_data, target, $("#teacher-course-table-wrapper"));
}