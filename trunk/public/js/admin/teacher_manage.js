$(document).ready(function() {
	initTeacherTable();
	console.log(data);
	setNextpageUrl($(".page-nav-prev"),$(".page-nav-next"),$(".page-current"));
	gotoPage($(".page-current"),$(".page-nav-goto"));
	initAjaxFileUpload("import-teacher-submit", "import-teacher-btn", ROOT + "/index.php/AdminData/admin_upload_teacher_a");
});

$('#delete-teacher-btn').click(function(){

			var idAry=[];
			$("#teacher-manage-table input:checked").each(function(i){
				idAry.push( $(this).attr('data-id') );
			});

			$.ajax({
				url: ROOT + "/index.php/AdminData/admin_remove_teacher_a",
				data: {
					t_id:idAry.join(',')
				},
				type: "POST",
				dataType: "json",
				success: function(data) {
					 alert(data.info);
					 location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				
				}
			});
	});



function initTeacherTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>教师管理</div></div>";
	$("#teacher-table-wrapper").prepend(span);
	var row_names = ['<input type="checkbox" class="checkall">',"教工号", "姓名", "性别", "联系电话", "邮箱地址"],
		teacher_data = [],
		target = "teacher-manage-table";

	for(var i = 0; i < data.data.length; i++) {
		teacher_data.push({
			check: '<input type="checkbox" data-id="' + data.data[i].t_id + '">',
			id: "<span class='grade'>" + data.data[i].t_id + "</span>",
			name: "<a class='u-table-link' href='./admin_single_teacher_manage/t_id/" + data.data[i].t_id + "' target='_blank' title='查看" + data.data[i].t_name + "个人信息'>" + data.data[i].t_name + "</a>",
			sex: data.data[i].t_sex,
			phone: data.data[i].t_phone,
			email: data.data[i].t_email
		});
	}

	createTable(row_names, teacher_data, target, $("#teacher-table-wrapper"));

}




