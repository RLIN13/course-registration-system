$(document).ready(function() {
	initSelectSetting();
});

function initSelectSetting() {
	var	span ="<div class='page-header'><div class='page-header-title'>设置</div></div>";
	$("#course-setting-table-wrapper").prepend(span);

	var grade = data.grade_list,
		grade_length = grade.length,
		selectCourseOption = '',
		checked = '',
		direction_grade = data.direction_grade;

	$("#special-good-percent").val(data["special_good_percent"]);
	$("#normal-good-percent").val(data["normal_good_percent"]);

	//生成可以选方向的年级的选项，并显示已经选择的年级
	//生成可以选课的年级的选项，并判断是否已经选择
	for(var i in grade) {
		$("#select-direction-grade").append("<option value='" + grade[i][0] + "'>" + grade[i][0] + "</option>");

		checked = grade[i][1] == 1 ? 'checked="checked"' : '';
//		selectCourseOption = '<tr><td></td><td><input type="checkbox"' + checked + 'data-id="'+ grade[i][0] +'">' + grade[i][0] +'</td>';       
        selectCourseOption = '<input type="checkbox"' + checked + 'data-id="'+ grade[i][0] +'">' + grade[i][0] +'</td>&nbsp&nbsp';
        $("#select_grade").append(selectCourseOption);
//        $("#course-setting-manage-table tbody").append(selectCourseOption);
	} 

	for(var i = 0; i < grade_length; i++) {
		if(direction_grade == grade[i][0]) {
			$("#select-direction-grade").get(0).selectedIndex = i+1;
		}
	}

	var width = $("#course-setting-confirm-table").width();
	$("#confirm-table-head").width(width/3);

}

$("#first").on("click",function(){
       $.ajax({
			url: ROOT + "/index.php/AdminData/admin_after_direction_first_a",
			type: "POST",
			dataType: "json",
			success: function(data) {	
			    alert(data.info);		
				location.reload();
			      },
	         });
});

$("#second").on("click",function(){
       $.ajax({
			url: ROOT + "/index.php/AdminData/admin_after_direction_second_a",
			type: "POST",
			dataType: "json",
			success: function(data) {	
			    alert(data.info);		
				location.reload();
			      },
	         });
});

$("#third").on("click",function(){
       $.ajax({
			url: ROOT + "/index.php/AdminData/admin_after_course_third_a",
			type: "POST",
			dataType: "json",
			success: function(data) {	
			    alert(data.info);		
				location.reload();
			      },
	         });
});

$("#course-setting-submit").on("click",function(){
	var direction_grade = $("#select-direction-grade").val(),
		special_good_percent = $("#special-good-percent").val(),
		normal_good_percent = $("#normal-good-percent").val(),
		course_grades = [],
		check_list = $(":checkbox");

	direction_grade = (direction_grade == "请选择") ? "" : direction_grade;

	check_list.each(function(){
		if($(this).is(':checked')){
			course_grades.push($(this).attr("data-id"));
		}
	});

	$.post(ROOT + "/index.php/AdminData/admin_update_select_setting_a",
		{
			"course_grades": course_grades,
			"special_good_percent": special_good_percent,
			"normal_good_percent": normal_good_percent,
			"direction_grade": direction_grade == '请选择' ? '' : direction_grade
		},function(response) {
			if(response.status == "1"){
				alert("提交成功");
				window.location.reload();
			}else{
				alert(response.info);
			}
		},"json");
});


