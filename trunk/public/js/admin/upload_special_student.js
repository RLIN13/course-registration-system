$(document).ready(function() {
	var	span ="<div class='page-header'><div class='page-header-title'>导入学生</div></div>";
	$("#special-student-table-wrapper").prepend(span);
    initAjaxFileUpload("import-stu-lib-submit", "import-stu-lib-btn", ROOT + "/index.php/AdminData/admin_upload_lab_student_a");
    initAjaxFileUpload("import-stu-special-submit", "import-stu-special-btn", ROOT + "/index.php/AdminData/admin_upload_special_student_a");
    initAjaxFileUpload("import-stu-normal-submit", "import-stu-normal-btn", ROOT + "/index.php/AdminData/admin_upload_normal_student_a");
});
