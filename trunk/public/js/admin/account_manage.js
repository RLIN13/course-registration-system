/*--------------------------
admin Account manage
--------------------------*/
$(document).ready(function() {
	queryAccount();
});

function queryAccount(){
	//accout 管理
	$("#account-manage-submit-btn").click(function() {
		$.ajax({
			url: ROOT + "/index.php/AccountData/get_name_a",
			data: {
				username : $("#account-form-name").val(),
			},
			type: "GET",
			dataType: "json",
			success: function(data) {
				if (data.data !== false) {
					if(window.confirm('确定需要重置'+data.data+'的密码吗？')){
						$.ajax({
							url: ROOT + "/index.php/AccountData/reset_pwd_a",
							data: {
								username : $("#account-form-name").val(),
							},
							type: "POST",
							dataType: "json",
							success: function(data) {
								alert(data["info"]);
							}
						});
						return true;
					}else{
						return false;
					}
				}
				else
				{
					alert(data.info + ",请输入正确的账号。");
					return false;
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {

			}
		});
		return false;
	});
}