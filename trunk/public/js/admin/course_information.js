$(document).ready(function() {
	$("title").html(data.data.co_name);
	initCourseInfoForm();
	$("#course-information-form input").attr("disabled", "disabled");
	$("#course-information-form select").attr("disabled", "disabled");

	$("#modify-btn").click(function() {
		$("#course-information-form input").removeAttr("disabled");
		$("#course-information-form select").removeAttr("disabled");
		$("#course-information-form-teacher").attr("disabled","disabled");
		$(".before-modify-wrapper").css("display", "block");
		$(".modify-wrapper").css("display", "none");
		$(".save-wrapper").css("display", "block");
		return false;
	});

	$("#reset-btn").click(function() {
		initCourseInfoForm();
		return false;
	});

	$("#save-btn").click(function() {
		$.ajax({
			url: ROOT + "/index.php/AdminData/admin_update_course_data_a",
			data: {
				id: data.data.co_id,
				name: $("#course-information-form-name").val(),
				grade: $("#course-information-form-grade").val(),
				direction: $("#course-information-form-direction").val(),
				leading_course: $("#course-information-form-leading-course").val(),
				credit: $("#course-information-form-credit").val(),
				max_num: $("#course-information-form-max-number").val(),
				introduction:$("#course-information-form-introduction").val()
			},
			type: "POST",
			dataType: "json",
			success: function(data) {
				var test = $("#course-information-form-teacher").val();
				console.log(test);
				location.reload();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest.status);
				console.log(XMLHttpRequest.readyState);
				console.log(textStatus);
			}
		});
		
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("#course-information-form-name").attr("disabled", "disabled");
		$("#course-information-form-semester").attr("disabled", "disabled");
		$("#course-information-form-grade").attr("disabled", "disabled");
		$("#course-information-form-teacher").attr("disabled", "disabled");
		$("#course-information-form-direction").attr("disabled", "disabled");
		$("#course-information-form-leading-course").attr("disabled", "disabled");
		$("#course-information-form-credit").attr("disabled", "disabled");
		$("#course-information-form-max-number").attr("disabled", "disabled");
		$("#course-information-form-selected").attr("disabled", "disabled");
		$("#course-information-form-time").attr("disabled", "disabled");
		$("#course-information-form-introduction").attr("disabled", "disabled");
		return false;
	});
});

function initCourseInfoForm() {
//	var	span ="<div class='page-header'><div class='page-header-title'>课程信息</div></div>";
//	$("#course-information-form").prepend(span);
	var form_width = $("#single-teacher-form").width();
	$(".page-header").width(form_width);

	var course_data = data.data;
		direction_data = data.direction;
		grade_data = data.grade;

	$("#course-information-title").val(course_data.co_name);
	$("#course-information-form-id").text(course_data.co_id);
	$("#course-information-form-name").val(course_data.co_name);
	$("#course-information-form-semester").text(course_data.co_semester);
	$("#course-information-form-teacher").text(course_data.co_teacher);
	$("#course-information-form-leading-course").val(course_data.co_leading_course);
	$("#course-information-form-credit").val(course_data.co_credit);
	$("#course-information-form-max-number").val(course_data.co_max_num);
	$("#course-information-form-selected").text(course_data.co_selected_num);
	$("#course-information-form-time").text(course_data.link_time);
	$("#course-information-form-introduction").val(course_data.co_introduction);

	$("#course-information-form-direction").append("<option value=''>无方向</option>");
	for(var i = 0; i < direction_data.length; i++) {
		$("#course-information-form-direction").append("<option value='" + direction_data[i].d_id + "'>" + direction_data[i].d_name + "</option>");
		if(course_data.co_direction == direction_data[i].d_name) {
			$("#course-information-form-direction").get(0).selectedIndex = i+1;
		}
	}

	for(var i = 0; i < grade_data.length; i++) {
		$("#course-information-form-grade").append("<option value='" + grade_data[i] + "'>" + grade_data[i] + "</option>");
		if(course_data.co_grade == grade_data[i]) {
			$("#course-information-form-grade").get(0).selectedIndex = i;
		}
	}
}