$(document).ready(function() {
	initStudentTable();
	setNextpageUrl($(".page-nav-prev"),$(".page-nav-next"),$(".page-current"));
	gotoPage($(".page-current"),$(".page-nav-goto"));
        console.log(data);
        initStudentForm();
        initAjaxFileUpload("import-student-submit", "import-student-btn", ROOT + "/index.php/AdminData/admin_upload_student_a");
});

$("#student-form-grade").change(function(){
	   $("#student-form-class").empty();
	   var grade_value = $("#student-form-grade").val();
	   var grade_length = data.grade_list[grade_value].length;
	   
	   
	   for (var j = 0 ; j < grade_length ; j++){
		   $("#student-form-class").append("<option value='" + data.grade_list[grade_value][j][0] + "'>" + data.grade_list[grade_value][j][1] + "</option>");
		   
	   }
	   
});

$('#delete-student-btn').click(function(){

			var idAry=[];
			$("#student-manage-table input:checked").each(function(i){
				console.log($(this).attr('data-id'));
				idAry.push( $(this).attr('data-id') );
			});

			console.log(idAry);

			$.ajax({
				url: ROOT + "/index.php/AdminData/admin_remove_student_a",
				data: {
					ids:idAry.join(',')
				},
				type: "POST",
				dataType: "json",
				success: function(data) {
					 alert(data.info);
					 location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				
				}
			});
	});

function initStudentTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>学生管理</div></div>";
	$("#student-table-wrapper").prepend(span);

	var row_names = ["","学号", "年级", "班级", "姓名", "性别","电话","邮箱","方向","已选学分","通过学分","实验室","成绩"],
		student_data = [],
		target = "student-manage-table";

	for(var i = 0; i < data.data.length; i++) {
		student_data.push({
			check: '<input type="checkbox" data-id="' + data.data[i].s_id + '">',
			id:  "<span class='grade'>" + data.data[i].s_id + "</span>",
			grade: data.data[i].s_grade,
			class: data.data[i].s_class,
			name: "<a class='u-table-link' href='./admin_single_student_manage/id/" + data.data[i].s_id + "' target='_blank' title='查看" + data.data[i].s_name + "个人信息'>" + data.data[i].s_name + "</a>",
			sex  : data.data[i].s_sex,
			phone : data.data[i].s_phone,
			email : data.data[i].s_email,
	        direction : data.data[i].s_direction,
			select_credit : data.data[i].s_select_credit,
			pass_credit : data.data[i].s_pass_credit,
			lib : data.data[i].s_lib,
			good : data.data[i].s_good
		});
	}

	createTable(row_names, student_data, target, $("#student-table-wrapper"));

}

function initStudentForm() {

	var	 url_param = getUrlParam();	
	var grade = new Array();
//	var first = "";
//	grade.push(first);
	for(var key in data.grade_list){  
         grade.push(key);
	  }

	var grade_length = grade.length;
	
	for(var i = 0; i < grade_length; i++) {
		$("#student-form-grade").append("<option value='" + grade[i] + "'>" + grade[i] + "</option>");
		if(url_param.search_param.grade == grade[i]) {
			$("#student-form-grade").get(0).selectedIndex = i;
		}
		
	}

	 $("#student-form-class").empty();
	   var grade_value = $("#student-form-grade").val();
           if (grade_value == "")
           {
               return;
           }
	   var grade_length = data.grade_list[grade_value].length;
	   
	   for (var j = 0 ; j < grade_length ; j++){
		   $("#student-form-class").append("<option value='" + data.grade_list[grade_value][j][0] + "'>" + data.grade_list[grade_value][j][1] + "</option>");
		   
	   }

}



