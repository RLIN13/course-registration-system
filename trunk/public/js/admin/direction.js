$(document).ready(function() {
	directionTable();
	checkall();

	$('#delete-direction-btn').click(function(){

			var idAry=[];
			$("#direction-manage-table input:checked").each(function(i){
				console.log($(this).attr('data-id'));
				idAry.push( $(this).attr('data-id') );
			});

			$.ajax({
				url: ROOT + "/index.php/AdminData/admin_remove_direction_a",
				data: {
					direction_id:idAry.join(',')
				},
				type: "POST",
				dataType: "json",
				success: function(data) {
					 alert(data.info);
					 location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				
				}
			});
	});

	$('#add-direction-btn').click(function()
		{$.ajax({
			url: ROOT + "/index.php/AdminData/admin_create_direction_a",
			data: {
				name: $("#new-direction").val(),
				num: $("#direction-max-num").val(),
				introduction:$("#direction-intro").val()
			},
				type: "POST",
				dataType: "json",
				success: function(data) {
					 alert(data.info);
					 location.reload();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				}
			});
	});
});

function directionTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>设置</div></div>";
	$("#direction-table-wrapper").prepend(span);
	var row_names = ['<input type="checkbox" class="checkall">', '方向', '最大容量', '方向简介'],
		direction_data = [],
		target = "direction-manage-table",
		data_length = data.data.length;

	for(var i = 0; i < data_length; i++) {
		direction_data.push({
			'checkbox': '<input type="checkbox" data-id="' + data.data[i].d_id + '">',
			"name": data.data[i].d_name,
			'max_num': data.data[i].d_max_num,
			'introduction': data.data[i].d_introduction
		});
	}
	createTable(row_names, direction_data, target, $("#direction-table-wrapper"));

}
