$(document).ready(function() {
	initStudentForm();

	
	$("#single-student-form input").attr("disabled", "disabled");
	$("#single-student-form select").attr("disabled", "disabled");
	$("#single-student-form-id").css("background-color", "#e8e8e8");
	$("#single-student-form-select-credit").css("background-color", "#e8e8e8");
	$("#single-student-form-pass-credit").css("background-color", "#e8e8e8");

	$("#modify-btn").click(function() {
		$("#single-student-form input").removeAttr("disabled");
		$("#single-student-form select").removeAttr("disabled");
		$(".before-modify-wrapper").css("display", "block");
		$(".modify-wrapper").css("display", "none");
		$(".save-wrapper").css("display", "block");
		return false;
	});

	$("#reset-btn").click(function() {
		initStudentForm();
		return false;
	});

	$("#save-btn").click(function() {
		$.ajax({
			url: ROOT + "/index.php/AdminData/admin_update_student_a",
			data: {
				s_id: data.data[0].s_id,
				s_name: $("#single-student-form-name").val(),
				s_sex: $("#single-student-form-sex").val(),
				s_phone: $("#single-student-form-phone").val(),
				s_email: $("#single-student-form-email").val(),
				s_direction: $("#single-student-form-direction").val(),
				s_grade: $("#single-student-form-grade").val(),
				s_class: $("#single-student-form-class").val(),
				s_lib: $("#single-student-form-lib").val(),
				s_good: $("#single-student-form-good").val()
			},
			type: "POST",
			dataType: "json",
			success: function(data) {
				location.reload();
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest.status);
				console.log(XMLHttpRequest.readyState);
				console.log(textStatus);
			}
		});
		
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("#single-student-form-name").attr("disabled", "disabled");
		$("#single-student-form-sex").attr("disabled", "disabled");
		$("#single-student-form-phone").attr("disabled", "disabled");
		$("#single-student-form-email").attr("disabled", "disabled");
		$("#single-student-form-direction").attr("disabled", "disabled");
		$("#single-student-form-grade").attr("disabled", "disabled");
		$("#single-student-form-class").attr("disabled", "disabled");
		$("#single-student-form-lib").attr("disabled", "disabled");
		$("#single-student-form-good").attr("disabled", "disabled");
		

		return false;
	});
	
	$("#single-student-form-email").blur(function(){
		isEmail($("#single-student-form-email"));
	})
	
	$("#single-student-form-phone").blur(function(){
		isPhone($("#single-student-form-phone"));
	})

	$("#cancel-btn").click(function() {
		initStudentForm();
		$("#single-student-form input").attr("disabled", "disabled");
		$("#single-student-form select").attr("disabled", "disabled");
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		isEmail($("#single-student-form-email"));
		
	    if (if_phone_right == true){
		  isEmail($("#single-student-form-email"));
	    }
		return false;
	});
    /*
	isEmail($("#single-student-form-email"));
		
	    if (if_phone_right == true){
		  isEmail($("#single-student-form-email"));
	    }*/
		
	$("#single-student-form-grade").change(function() {
		changeClassOption();
	});
	
	
});

function initStudentForm() {
//	var	span ="<div class='page-header'><div class='page-header-title'>学生信息</div></div>";
//	$("#single-student-form").prepend(span);
	var form_width = $("#single-teacher-form").width();
	$(".page-header").width(form_width);

	var student_data = data.data[0],
		direction_data = data.direction,
		grade_class_data = data.grade_class,
		direction_data_length = direction_data.length,
		grade_class_data_length = grade_class_data[student_data.s_grade].length,
		temp = 0;

	$("#single-student-form-name").val(student_data.s_name);
	$("#single-student-form-id").text(student_data.s_id);
	$("#single-student-form-phone").val(student_data.s_phone);
	$("#single-student-form-email").val(student_data.s_email);
	$("#single-student-form-select-credit").text(student_data.s_select_credit);
	$("#single-student-form-pass-credit").text(student_data.s_pass_credit);

	if(student_data.s_sex == "男") {
		$("#single-student-form-sex").get(0).selectedIndex = 0;
	} else {
		$("#single-student-form-sex").get(0).selectedIndex = 1;
	}

	if(student_data.s_good == '1') {
		$("#single-student-form-good").get(0).selectedIndex = 0;
	} else {
		$("#single-student-form-good").get(0).selectedIndex = 1;
	}

	if(student_data.s_lib == '1') {
		$("#single-student-form-lib").get(0).selectedIndex = 0;
	} else {
		$("#single-student-form-lib").get(0).selectedIndex = 1;
	}

	$("#single-student-form-direction").append("<option value=''>无方向</option>");
	for(var i = 0; i < direction_data_length; i++) {
		$("#single-student-form-direction").append("<option value='" + direction_data[i].d_id + "'>" + direction_data[i].d_name + "</option>");

		if(student_data.s_direction == direction_data[i].d_id) {
			$("#single-student-form-direction").get(0).selectedIndex = i + 1;
		}
	}

	for(grade in grade_class_data) {
		$("#single-student-form-grade").append("<option value='" + grade + "'>" + grade + "</option>");
		if(student_data.s_grade == grade) {
			$("#single-student-form-grade").get(0).selectedIndex = temp;
		}
		temp++;
	}

	for(var i = 0; i < grade_class_data_length; i++) {
		$("#single-student-form-class").append("<option value='" + grade_class_data[student_data.s_grade][i][0] + "'>" + grade_class_data[student_data.s_grade][i][1] + "</option>");

		if(student_data.s_class == grade_class_data[student_data.s_grade][i][0]) {
			$("#single-student-form-class").get(0).selectedIndex = i;
		}
	}
}

function changeClassOption() {
	var grade = $("#single-student-form-grade").val(),
		class_array = data.grade_class[grade],
		class_array_length = class_array.length;

	$("#single-student-form-class").empty();

	for(var i = 0; i < class_array_length; i++) {
		$("#single-student-form-class").append("<option value='" + class_array[i][0] + "'>" + class_array[i][1] + "</option>");
	}
}