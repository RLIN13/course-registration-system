$(window).resize(function(){
   var form_width = $("#single-teacher-form").width();
	$(".page-header").width(form_width);
});

$(document).ready(function() {
	//initStudentForm();
	initTeacherForm();
	/*
	isEmail($("#single-teacher-form-email"));
	
    var if_phone_right =isPhone($("#single-teacher-form-phone"));
	
	if (if_phone_right == true){
		isEmail($("#single-teacher-form-email"));
	}else{
		isPhone($("#single-teacher-form-phone"));
	}*/

	$("#modify-btn").click(function() {
		$("#single-teacher-form-name").removeAttr("disabled");
		$("#single-teacher-form-sex").removeAttr("disabled");
		$("#single-teacher-form-phone").removeAttr("disabled");
		$("#single-teacher-form-email").removeAttr("disabled");
		$("#single-teacher-form fieldset").removeAttr("disabled");
	    $("textarea").removeAttr("disabled");
		$("textarea").css("height","80px");
		$("#single-teacher-form-id").attr("disabled", "disabled");
        $(".before-modify-wrapper").css("display", "block");
		$(".modify-wrapper").css("display", "none");
		$(".save-wrapper").css("display", "block");
		$("#error").css("display","none");
		return false;
	});
	
    
          
	
	$("#reset-btn").click(function() {
		initTeacherForm();
		return false;
	});
	
	$("#cancel-btn").click(function() {
		initTeacherForm();
		$("#single-teacher-form fieldset").attr("disabled", "disabled");
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("textarea").css("height","30px")
		/*
		isEmail($("#single-teacher-form-email"));
		
	    if (if_phone_right == true){
		  isEmail($("#single-teacher-form-email"));
	    }*/
		  return false;
	    });
	
	$("#save-btn").click(function() {
		
		
		$.ajax({
			url: ROOT + "/index.php/AdminData/admin_update_teacher_a",
			data: {
				t_id: data.data[0].t_id,
				t_name: $("#single-teacher-form-name").val(),
				t_sex: $("#single-teacher-form-sex").val(),
				t_phone: $("#single-teacher-form-phone").val(),
				t_email: $("#single-teacher-form-email").val(),
				t_introduction: $("textarea").val(),
			},
			type: "POST",
			dataType: "json",
			success: function(data) {
			
				location.reload();
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(XMLHttpRequest.status);
				console.log(XMLHttpRequest.readyState);
				console.log(textStatus);
			}
		});
		
		
		
		$("#single-teacher-form fieldset").attr("disabled", "disabled");
		$(".modify-wrapper").css("display", "block");
		$(".save-wrapper").css("display", "none");
		$("textarea").css("height","30px")
		$("#single-teacher-form-name").attr("disabled", "disabled");
		$("#single-teacher-form-sex").attr("disabled", "disabled");
		$("#single-teacher-form-phone").attr("disabled", "disabled");
		$("#single-teacher-form-email").attr("disabled", "disabled");
		
		
		return false;
	});

	
});

    $("#single-teacher-form-email").blur(function(){
		isEmail($("#single-teacher-form-email"));
	})
	
	$("#single-teacher-form-phone").blur(function(){
		isPhone($("#single-teacher-form-phone"));
	})
	
function initTeacherForm(){
//	var	span ="<div class='page-header'><div class='page-header-title'>教师信息</div></div>";
//	$("#single-teacher-form").prepend(span);
	var form_width = $("#single-teacher-form").width();
	$(".page-header").width(form_width);
	
	var teacher_data = data.data[0];
	/*初始化姓名、电话、邮箱、简介*/
	$("#single-teacher-form-id").val(teacher_data.t_id);
	$("#single-teacher-form-name").val(teacher_data.t_name);
	$("#single-teacher-form-phone").val(teacher_data.t_phone);
	$("#single-teacher-form-email").val(teacher_data.t_email);
	$("#single-teacher-form-intro").val(teacher_data.t_introduction);
	/*初始化性别*/
	if(teacher_data.t_sex == "男") {
		$("#single-teacher-form-sex").get(0).selectedIndex = 0;
	} else {
		$("#single-teacher-form-sex").get(0).selectedIndex = 1;
	}
	
	
}

