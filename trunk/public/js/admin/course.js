$(document).ready(function() {
	console.log(data);
	initCourseSemester();
	initCourseGrade();
	initCourseTable();
	initCourseSearchForm();
	setNextpageUrl($(".page-nav-prev"),$(".page-nav-next"),$(".page-current"));
	gotoPage($(".page-current"),$(".page-nav-goto"));
	initAjaxFileUpload("import-course-submit", "import-course-btn", ROOT + "/index.php/AdminData/admin_upload_course_data_a");
});

function initCourseSemester(){
	semester = data.semester;
	for(var i = 0; i < semester.length; i++) {
		$("#course-form-semester").append("<option value='" + semester[i] + "'>" + semester[i] + "</option>");
	}
}

function initCourseGrade() {
	grade = data.grade;
	for(var i = 0; i < grade.length; i++) {
		$("#course-form-grade").append("<option value='" + grade[i] + "'>" + grade[i] + "</option>");
	}
}

function initCourseSearchForm() {
	var grade = data.grade,
		grade_length = grade.length,
		semester_length = semester.length,
		url_param = getUrlParam();

	for(var i = 0; i < grade_length; i++) {
		if(url_param.search_param.grade == grade[i]) {
			$("#course-form-grade").get(0).selectedIndex = i+1;
		}
	}

	for(var i = 0; i < semester_length; i++) {
		if(url_param.search_param.semester == semester[i]) {
			$("#course-form-semester").get(0).selectedIndex = i+1;
		}
	}
	$("#course-form-keyword").val(url_param.search_param.keyword);
}

function initCourseTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>课程管理</div></div>";
	$("#course-table-wrapper").append(span);
	var row_names = ["课程名字", "年级", "学期", "所属方向","是否是前导课", "最大容量", "已选人数", "学分", "上课时间"],
		course_data = [],
		target = "course-manage-table";

	for(var i = 0; i < data.data.length; i++) {
		course_data.push({
			name: "<a class='u-table-link' href='./admin_single_course_manage/co_id/" + data.data[i].co_id + "' target='_blank' title='查看" + data.data[i].co_name + "课程信息'>" + data.data[i].co_name + "</a>",
			grade: data.data[i].co_grade,
			semester: data.data[i].co_semester,
			direction: data.data[i].co_direction,
			leading_course: data.data[i].co_leading_course,
			max_num: data.data[i].co_max_num,
			selected: data.data[i].co_selected,
			credit: data.data[i].co_credit,
			time: data.data[i].time
		});
	}
	createTable(row_names, course_data, target, $("#course-table-wrapper"));
}

function changeCourseSemesterOption() {
	var semester = $("#course-form-semester").val(),
		semester_array = data.semester,
		semester_length = semester_array.length;

	$("#course-form-semester").empty();

	for(var i = 0; i < semester_length; i++) {
		$("#course-form-semester").append("<option value='" + semester_array[i] + "'>" + semester_array[i] + "</option>");
	}
}