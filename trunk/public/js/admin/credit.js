$(document).ready(function() {
	initCreditSearchForm();
	initCreditTable();
	setNextpageUrl($(".page-nav-prev"),$(".page-nav-next"),$(".page-current"));
	gotoPage($(".page-current"),$(".page-nav-goto"));
    initAjaxFileUpload("import-grade-submit", "import-grade-btn", ROOT + "/index.php/CreditData/upload_score_a");
});

function initCreditSearchForm() {
	var grade = data.grade,
		grade_length = grade.length,
		url_param = getUrlParam();

	for(var i = 0; i < grade_length; i++) {
		$("#credit-form-grade").append("<option value='" + grade[i] + "'>" + grade[i] + "</option>");

		if(url_param.search_param.grade == grade[i]) {
			$("#credit-form-grade").get(0).selectedIndex = i+1;
		}
	}

	if(url_param.search_param.select_credit && url_param.search_param.select_credit == '1') {
		$("#credit-form-credit-type").get(0).selectedIndex = 1;
	} else if(url_param.search_param.select_credit && url_param.search_param.select_credit == '2') {
		$("#credit-form-credit-type").get(0).selectedIndex = 2;
	} else if(url_param.search_param.select_credit && url_param.search_param.select_credit == '3') {
		$("#credit-form-credit-type").get(0).selectedIndex = 3;
	} else if(url_param.search_param.select_credit && url_param.search_param.select_credit == '4') {
		$("#credit-form-credit-type").get(0).selectedIndex = 4;
	} else {
		$("#credit-form-credit-type").get(0).selectedIndex = 0;
	}

	if(url_param.search_param.keyword) {
		$("#credit-form-keyword").val(decodeURI(url_param.search_param.keyword));
	}
	$("#credit-form-credit-floor").val(url_param.search_param.credit_bottom);
	$("#credit-form-credit-upper").val(url_param.search_param.credit_top);
}

function initCreditTable() {
	var	span ="<div class='page-header'><div class='page-header-title'>学分管理</div></div>";
	$("#credit-table-wrapper").prepend(span);
	var row_names = ["名称", "年级", "学号", "已选学分", "已修学分"],
		credit_data = [],
		target = "credit-manage-table",
		credit_array = data.data,
		credit_array_length = credit_array.length;

	for(var i = 0; i < credit_array_length; i++) {
		credit_data.push({
			name: "<a class='u-table-link' href='./admin_single_student_manage/id/" + data.data[i].s_id + "' target='_blank' title='查看" + data.data[i].s_name + "个人信息'>" + data.data[i].s_name + "</a>",
			grade: credit_array[i].s_grade,
			id: credit_array[i].s_id,
			credit_selected: credit_array[i].s_select_credit,
			credit_passed:credit_array[i].s_pass_credit
		});
	}

	createTable(row_names, credit_data, target, $("#credit-table-wrapper"));
}

